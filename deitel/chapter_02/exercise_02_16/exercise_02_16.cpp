#include <iostream>

int
main()
{
    int number1;
    std::cout << "Input first integer: "; 
    std::cin >> number1; 
    int number2;
    std::cout << "Input second integer: "; 
    std::cin >> number2; 
    if (0 == number2) {
        std::cerr << "Error 1: A number can't be divided to 0." << std::endl;
        return 1;
    }
    std::cout << "Sum is " << number1 + number2 << std::endl;
    std::cout << "Differrnce is " << number1 - number2 << std::endl;
    std::cout << "Product is " << number1 * number2 << std::endl;
    std::cout << "Divison is " << number1 / number2 << std::endl;
    std::cout << "Quotient is " << number1 % number2 << std::endl;
    return 0;
}

