#include <iostream>

int
main()
{
    int number1, number2;
    std::cout << "Input two integers: ";
    std::cin >> number1 >> number2;
    if (0 == number2) {
        std::cout << "Error1: number can not by 0" << std::endl;
        return 1;
    }
    if (number1 % number2 != 0) {
        std::cout << "Not multiple." << std::endl;
        return 0;
    }
    std::cout << "Number 1 is multiple of number 2." << std::endl; 
    return 0;   
}

