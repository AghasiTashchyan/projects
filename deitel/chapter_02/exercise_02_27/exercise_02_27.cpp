#include <iostream>

int 
main()
{
    std::cout << static_cast<int>('A') << "\n";
    std::cout << static_cast<int>('B') << "\n";
    std::cout << static_cast<int>('C') << "\n";
    std::cout << static_cast<int>('a') << "\n";
    std::cout << static_cast<int>('b') << "\n";
    std::cout << static_cast<int>('c') << "\n";
    std::cout << static_cast<int>('1') << "\n";
    std::cout << static_cast<int>('2') << "\n";
    std::cout << static_cast<int>('3') << "\n";
    std::cout << static_cast<int>('$') << "\n";
    std::cout << static_cast<int>('*') << "\n";
    std::cout << static_cast<int>('+') << "\n";
    std::cout << static_cast<int>('/') << "\n";
    std::cout << static_cast<int>(' ') << "\n";   
    std::cout << static_cast<int>('\t') << "\n";   
    std::cout << static_cast<int>('\n') << "\n";   
    return 0;
}

