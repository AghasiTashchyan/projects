#include <iostream>

int 
main()
{
    int number;
    std::cin >> number;
    if (number > 99999){
        std::cout << "Error 1: Number must be fiv-digit." << std::endl;
        return 1;
    }
    if (number < 10000){
        std::cout << "Error 1: Number must be fiv-digit." << std::endl;
        return 1;
    }
    std::cout << number / 10000 << "   ";
    number = number % 10000;
    std::cout << number /  1000 << "   ";
    number = number % 1000;
    std::cout << number / 100 << "   ";
    number = number % 100;
    std::cout << number / 10 << "   "; 
    number = number % 10 ;
    std::cout << number; 
    return 0;
}

