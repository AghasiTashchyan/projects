#include <iostream>

int 
main()
{    
    std::cout << "Input first number: ";
    int number1;
    std::cin >> number1;
    std::cout << "Input second number: ";
    int number2;
    std::cin >> number2;
    if (number1 > number2) {
        std::cout << number1 << " greater then " << number2 << std::endl;
        return 0;
    }
    if (number1 < number2) { 
        std::cout << number2 << " greater then " << number1 << std::endl;
        return 0;
    }   
    std::cout << "These numbers are equal." << std::endl;
    return 0; 
}

