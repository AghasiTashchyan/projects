#include <iostream>
#include <unistd.h>

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input 10 numbers: ";
    }
    const int SIZE_ARRAY = 10;
    int array[SIZE_ARRAY];
    for (int i = 0; i < SIZE_ARRAY; ++i) {
        std::cin >> array[i];
    }
    int step = 0;
    for (int i = 0; i < SIZE_ARRAY; ++i) {
        int bubble;
        for (int j = 0; j < SIZE_ARRAY - 1; ++j) {
            if (array[j] > array[j + 1]) {
                bubble = array [j];
                array[j] = array[j + 1];
                array[j + 1] = bubble;
                ++step;
            }
        }
    }
    std::cout << step;
    return 0;
}

