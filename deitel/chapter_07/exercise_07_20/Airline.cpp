#include "Airline.hpp"
#include <iostream>
#include <unistd.h>
#include <iomanip>

void 
Airline::printMenu()
{
    if (::isatty(STDIN_FILENO)) { 
        std::cout << " 1 --->  First class.\n"
                  << " 2 --->  Economy class.\n" 
                  << " 0 --->  To Exit.\n";
    }
}

void
Airline::printFreeTicketsOfEconomyClass(int* array, const int size)
{
    const int midle = size / 2;
    for (int i = midle; i < size; ++i) {
        if (0 == array[i]) {
            std::cout << "Input --->  " << i + 1 << " to reserve\n";
        }
    }
}

bool
Airline::checkEconomyTickets (int* array, const int size)
{
    const int midle = size / 2;
    for (int i = midle; i < size; ++i) {
        if (0 == array[i]) {
            return true;
        }
    }
    return false;
}

int 
Airline::bookingTicketsForFirstClass(const int answer, int* array, const int size)
{
    if (0 > answer && 5 < answer) {
        std::cout << "Error 1 : Invalid value\n";
    }
    for (int i = 0; i < size / 2; ++i) {
        if (0 != array[answer - 1]) {
            std::cout << "The selected ticket is busy!" << std::endl;
            return 0;
        }
    }
    array[answer - 1] = 1;
    std::cout << "You have booked a first class ticket\n";
    std::cout << "Ticket: " << answer << " is reserved\n" << std::endl;
    return 0;
}

bool
Airline::checkFirrstClassTickets (int* array, const int size)
{
    const int midle = size / 2;
    for (int i = 0; i < midle; ++i) {
        if (0 == array[i]) {
            return true;
        }
    }
    return false;
}

int 
Airline::bookingTicketsForEconomyClass(const int answer, int* array, const int size)
{
    if (answer < 6 && answer > 10) {
        std::cout << "Error 2: Invalid value\n";
    }
    const int midle = size / 2;
    for (int i = midle; i < size; ++i) {
        if (0 != array[answer - 1]) {
            std::cout << "The selected ticket is busy!" << std::endl;
            return 0;
        }
    }
    array[answer - 1] = 2;
    std::cout << "You have booked a economy class ticket\n";
    std::cout << "Ticket: " << answer << " is reserved\n" << std::endl;
    return 0;
}

int 
Airline::inputValue()
{
    int value;
    std::cin >> value;
    return value;
}

void
Airline::printFreeTicketsOfFirstClass(int* array, const int size)
{
    const int midle = size / 2;
    for (int i = 0; i < midle; ++i) {
        if (0 == array[i]) {
            std::cout << "Input --->  " << i + 1 << " to reserve\n";
        }
    }
}

void
Airline::printAllTickets(int* array, const int size)
{
    for (int i = 0; i < size; ++i) {
        std::cout << array[i] << std::endl;
    }
}

bool
Airline::checkingAllTickets(int* array, const int size)
{
    for (int i = 0; i < size; ++i) {
        if (0 == array[i]) {
            return true;
        }
    }
    return false;
}

int 
Airline::run()
{
    const int TICKET_COUNT = 10;
    int tickets [TICKET_COUNT] = {0};
    while (checkingAllTickets(tickets, TICKET_COUNT)) {
        printMenu();
        const int answer = inputValue();
        if (0 == answer) {
            return 0;
        }
        if (1 == answer) {
            if (!checkFirrstClassTickets(tickets, TICKET_COUNT)) {
                std::cout << "First class air tickets are all booked !\n";
                std::cout << "Select an economy class ticket or input 0 to exit.\n";
                continue;
            }
            printFreeTicketsOfFirstClass(tickets, TICKET_COUNT);
            const int firstClassTicket = inputValue();
            bookingTicketsForFirstClass(firstClassTicket, tickets, TICKET_COUNT);
        }

        if (2 == answer) {
            if (!checkEconomyTickets(tickets, TICKET_COUNT)) {
                std::cout << "Economy class air tickets are all booked !\n";
                std::cout << "Select an first class ticket or input 0 to exit.\n";
                continue;
            }
            printFreeTicketsOfEconomyClass(tickets, TICKET_COUNT);
            const int economyTicket = inputValue();
            bookingTicketsForEconomyClass(economyTicket, tickets, TICKET_COUNT);
        }
    }
    std::cout << "No tickets available. Next flight leaves in 3 hours\n";
    return 0;
}

