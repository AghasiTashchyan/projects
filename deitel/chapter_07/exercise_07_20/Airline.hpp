class Airline
{
public:
    void printMenu();
    void printFreeTicketsOfEconomyClass(int* array, const int size);
    bool checkEconomyTickets (int* array, const int size);
    int bookingTicketsForFirstClass(const int answer, int* array, const int size);
    bool checkFirrstClassTickets (int* array, const int size);
    int bookingTicketsForEconomyClass(const int answer, int* array, const int size);
    int inputValue();
    void printFreeTicketsOfFirstClass(int* array, const int size);
    void printAllTickets(int* array, const int size);
    bool checkingAllTickets(int* array, const int size);
    int run();
};

