#include <iostream>
#include <unistd.h>
#include <cstdlib>

int
inputValue(const std::string prompt)
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << prompt;
    }
    int value;
    std::cin >> value;
    return value;
}

int
setArray(int* array, const int size)
{
    for (int i = 0; i < size; ++i) {
        array[i] = inputValue("Input value: ");
        int j = i;
        while(j > 0) {
            if (array [i] == array[j - 1]) {
                array[i] = inputValue("Input new value: ");
                --i;
                break;
            }
            --j;
        }
    }
    return 0;
}
void
printArray(int* array, const int size)
{
    std::cout << "Unique values: ";
    for (int i = 0; i < size; ++i) {
        std::cout << array[i] << " ";
    }
}

int
main()
{
    const int sizeOfArray = inputValue("Input size: ");
    int array[sizeOfArray];
    setArray(array, sizeOfArray);
    printArray(array, sizeOfArray);
    return 0;
}

