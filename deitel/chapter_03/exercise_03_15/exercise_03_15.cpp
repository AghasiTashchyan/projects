#include "Date.hpp"
#include <iostream>

int
main()
{
    int day;
    std::cout << "Input day: " << std::endl;
    std::cin >> day;
    int month;
    std::cout << "Input month: " << std::endl;
    std::cin >> month;
    int year;
    std::cout << "Input year: " << std::endl;
    std::cin >> year;
    Date myDate(day, month, year);
    myDate.printDate();
    return 0;
}
