class Date 
{
public:
    Date(int day, int month, int year);
    void setDay(int day);
    int getDay();
    void setMonth(int month);
    int getMonth();
    void setYear(int year);
    int getYear();
    void printDate();
private:
    int day_;
    int month_;
    int year_;
};

