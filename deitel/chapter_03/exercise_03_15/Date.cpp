#include "Date.hpp"
#include <iostream>

Date::Date(int day, int month, int year)
{
    setYear(year);
    setMonth(month);
    setDay(day);
}

void 
Date::setDay(int day) 
{        
    if (day < 1) {
        day_ = 1;
        std::cout << "Warning 1: Invalid day value. Day becomes 1 " << std::endl;
        return;
    }
    if (day > 31) {
        day_ = 1; 
        std::cout << "Warning 2: Invalid day value. Day becomes 1 " << std::endl;
        return;
    }
    day_ = day;
}

int 
Date::getDay()
{
    return day_;
}

void 
Date::setMonth(int month)
{
    if (month < 1) {
        month_ = 1;
        std::cout << "Warning 3: Invalid mounth. Month becomes 1 " << std::endl;
        return;
    }
    if (month > 12) {
        month_ = 1 ;
        std::cout << "Warning 4: Invalid mounth. Month becomes 1" << std::endl;
        return;
    }
    month_ = month;
}

int 
Date::getMonth()
{
    return month_;
}

void
Date::setYear(int year)
{    
    if (year < 0) {
        year_ = 0;
        std::cout << "Warning 5: Invalid year. Year becomes 0 " << std::endl;
        return;
    }
    year_ = year;
}

int 
Date::getYear()
{
    return year_;
}

void
Date::printDate()
{
    std::cout << getDay() << "/" << getMonth() << "/" << getYear() << std::endl;
}

