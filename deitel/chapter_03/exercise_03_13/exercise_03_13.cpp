#include "Invoice.hpp"
#include <iostream>
#include <string>

int
main()
{
    std::string partNumber;
    std::cout << "Input part number: ";
    std::cin >> partNumber;
    std::string discript;
    std::cout << "Input discription: ";
    std::cin >> discript;
    int purches;
    std::cout << "Input amount: ";
    std::cin >> purches;
    if (purches < 0) {
        std::cout << "Error 1: Invalid value." << std::endl;
        return 1;
    }
    int price;
    std::cout << "Input price of amount: ";
    std::cin >> price;
    if (price < 0) {
        std::cout << "Error 2: Invalid value." << std::endl;
        return 2;
    }
    Invoice invoice(partNumber, discript, purches, price);
    std::cout << "Part number is " << invoice.getPartNumber() << std::endl; 
    std::cout << "Discription is " << invoice.getDiscript() << std::endl;
    std::cout << "Amount is " << invoice.getPurches() << std::endl; 
    std::cout << "Price is " << invoice.getPrice() << std::endl;
    std::cout << "Total amount is " << invoice.getInvoiceAmount() << std::endl;
    return 0;
}

