#include <iostream>
#include <string>

class Invoice
{
public:
    Invoice (std::string numberPart , std::string discription, int purchas, int price);
    std::string getPartNumber();
    void setPartNumber(std::string numberPart);
    void printdiscription(std::string partDiscription);
    std::string getDiscript();
    void setDiscript(std::string discription);
    int getPurches();
    void setPurchased(int purchas);
    int getPrice();
    void setPrice(int price);
    int getInvoiceAmount();
private:
    std::string partNumber_;
    std::string partDiscription_;
    int purchased_;
    int price_;
};

