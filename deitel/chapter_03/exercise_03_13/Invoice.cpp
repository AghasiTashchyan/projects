#include "Invoice.hpp"
#include <iostream>
#include <string>

Invoice::Invoice (std::string numberPart, std::string discription, int purchas, int price)
{
    setPartNumber(numberPart);
    setDiscript(discription);
    setPurchased(purchas);
    setPrice(price);
}

std::string
Invoice::getPartNumber()
{
    return partNumber_;
}

void
Invoice::setPartNumber(std::string numberPart)
{
    partNumber_ = numberPart;
}

std::string
Invoice::getDiscript()
{
    return partDiscription_ ;
}

void 
Invoice::setDiscript(std::string discription)
{
    partDiscription_ = discription;
}
    
int 
Invoice::getPurches()
{        
    return purchased_;
}

void 
Invoice::setPurchased(int purchas)
{
    if (purchas <= 0) {
        purchas_ = 0;
        std::cout << "Warning 1: value cant no be negativ, purchase become 0. " << std::endl;
        return;
    }
    purchased_ = purchas;
}

int
Invoice::getPrice()
{        
    return price_;
}
    
void 
Invoice::setPrice(int price) 
{
    if (price_ <= 0) {
        price_ = 0;
        std::cout << "Warning 2: value cant no be negativ, price become 0. " << std::endl;
        return;
    }
    price_ = price;
}

int 
Invoice::getInvoiceAmount()
{       
    return purchased_ * price_;
}

