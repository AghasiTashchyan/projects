#include "Employee.hpp"
#include <string>
#include <iostream>

Employee::Employee(std::string name, std::string lastName, int salary)
{
    setLastName(lastName);
    setName(name);
    setSalary(salary);
}

void 
Employee::setName(std::string name)
{
    name_ = name;
}

std::string
Employee::getName()
{
    return name_;
}

void 
Employee::setLastName(std::string lastName)
{
    lastName_ = lastName;
}

std::string
Employee::getLastName()
{
    return lastName_;
}

void
Employee::setSalary(int salary)
{
    if (salary < 0) {
        std::cout << "Warning 1: Invalide salary. Resetting to 0 " << std::endl;
        monthlySalary_ = 0;
        return;
    }
    monthlySalary_ = salary;
}

int 
Employee::getSalary()
{
    return monthlySalary_;
}

int 
Employee::getYearSalary()
{
    return monthlySalary_ * 12;
}

void
Employee::displayMessage()
{
    std::cout << "name: " 
              << getName() <<"\n" 
              << "lastname: " 
              << getLastName() << "\n"
              << "salary: "
              << getSalary() << "\n" 
              <<"year salary: " 
              << getYearSalary() << std::endl;
}

