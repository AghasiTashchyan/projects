#include "Employee.hpp"
#include <iostream>
#include <string>

int 
main()
{        
    std::cout << "Input firstname: ";
    std::string firstName;
    std::cin >> firstName;
    std::cout << "Input last name: ";
    std::string lastName;
    std::cin >> lastName;
    std::cout << "Input salary: ";
    int salary;
    std::cin >> salary;
    Employee myEmployee(firstName, lastName, salary);
    myEmployee.setSalary(myEmployee.getSalary() * 1.1);
    std::cout << "salary add 10%: " << std::endl;
    myEmployee.displayMessage();

    std::cout << "Input firstname: ";
    std::cin >> firstName;
    std::cout << "Input last name: ";
    std::cin >> lastName;
    std::cout << "Input salary: ";
    std::cin >> salary;
    Employee myEmployee2(firstName, lastName, salary);
    myEmployee2.setSalary( myEmployee2.getSalary() * 1.1);
    std::cout << "salary add 10%: " << std::endl;
    myEmployee2.displayMessage();
    return 0;
}

