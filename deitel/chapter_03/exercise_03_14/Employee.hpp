#include <string>
class Employee
{
public:
    Employee(std::string name, std::string lastName, int salary);
    void setName(std::string name);
    std::string getName();
    void setLastName(std::string lastName);
    std::string getLastName();
    void setSalary(int salary);
    int getSalary();
    int getYearSalary();
    void displayMessage();
private:
    std::string name_;
    std::string lastName_;
    int monthlySalary_;
};

