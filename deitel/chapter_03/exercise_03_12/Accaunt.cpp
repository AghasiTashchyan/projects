#include "Accaunt.hpp"

#include <iostream>

Accaunt::Accaunt()
{
    balance_ = 0 ;
}

void 
Accaunt::credit(int amount)
{
    if (amount < 0) {
        std::cerr << "Warning 1: Invalid credit. Balance is not chenged.\n";
        return;
    } 
    balance_ += amount;
}

void 
Accaunt::debit(int debit)
{
    if (debit < 0) {
        std::cerr << "Warning 2: Invalid debit. Balance is not changed\n";
        return;
    } 
    if (debit > balance_) {
        std::cerr << "Warning 3: Debit is more\n";
        return;
    } 
    balance_ -= debit;
}

int 
Accaunt::getBalance()
{
    return balance_;
}

