#include"GradeBook.hpp"
#include <iostream>
#include <string>

GradeBook::GradeBook(std::string name, std::string nameInstructor, int countOfStudent)
{
    setInstructorName(nameInstructor);
    setCurseName(name);
    setCountStudent(countOfStudent);
}

void
GradeBook::displayMessage()
{

    std::cout << "Welcome to the Grade Book: " << getCurseName() << std::endl;
    std::cout << "Instructor name: " << getInstructorName() << std::endl;
    std::cout << "Count Student in Class is " << getCountOfStudent() << std::endl;

}

std::string 
GradeBook::getCurseName()
{
    return curseName_;
}

void
GradeBook::setCurseName(std::string name)
{
    curseName_ = name;    
}

void
GradeBook::setInstructorName(std::string nameInstructor)
{
    instructorName_ = nameInstructor;
}

std::string
GradeBook::getInstructorName()
{
    return instructorName_;
}

void
GradeBook::setCountStudent(int countOfStudent)
{
    countOfStudent_ = countOfStudent;
}

int
GradeBook::getCountOfStudent()
{
    return countOfStudent_;
}

