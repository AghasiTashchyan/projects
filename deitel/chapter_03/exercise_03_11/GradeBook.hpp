#include <string>

class GradeBook                                                     
{   
public:    
    GradeBook(std::string name, std::string nameInstructor, int countOfStudent);
    void displayMessage();    
    std::string getCurseName();
    void setCurseName(std::string name);
    void setInstructorName(std::string nameInstructor );
    void setCountStudent(int countOfStudent);
    std::string getInstructorName();
    int getCountOfStudent();
private:
    std::string instructorName_;
    std::string curseName_;
    int countOfStudent_;    
};

