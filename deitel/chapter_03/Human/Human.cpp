#include <iostream>
#include <string>

class Human
{

public:
    Human(){};
    void setName     (std::string name)     {         name_ = name;        }
    void setLastName (std::string lastName) {     lastName_ = lastName;    }
    void setAge (int age)                   {          age_ = age;         }
    void printPerson() 
{
    std::cout << "Your name is " << getName() << std::endl;
    std::cout << "Your last name is " << getLastName() << std::endl;
    std::cout << "Your Age is " << getAge() << std::endl;
}


    std::string getName()     { return name_;     }
    std::string getLastName() { return lastName_; }
    int getAge()              { return age_;      }
private:
    std::string name_;
    std::string lastName_;
    int age_;

};

int
main()
{

    Human person1;
    std::cout << "Input Your Name: ";
    std::string name, lastname;
    std::cin >> name;
    std::cout << "Input Your Last Name: ";
    std::cin >> lastname;
    int age;
    std::cout << "Input Age: ";
    std::cin >> age;
    person1.setName(name);
    person1.setLastName(lastname);
    person1.setAge(age);
    person1.printPerson();
    Human person2;
    std::cout << "Input Your Name: ";
    std::string name2, lastname2;
    std::cin >> name2;
    std::cout << "Input Your Last Name: ";
    std::cin >> lastname2;
    int age2;
    std::cout << "Input Age: ";
    std::cin >> age2;
    person2.setName(name2);
    person2.setLastName(lastname2);
    person2.setAge(age2);
    person2.printPerson();

    return 0;
}
