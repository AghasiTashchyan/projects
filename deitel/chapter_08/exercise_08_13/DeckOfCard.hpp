
static const int PLAYER_CARD_COUNT = 5;
class DeckOfCard
{
public:
    DeckOfCard();
    void shuffle();
    void deal();
    void handOfPlayer(int* array, const int size);
    void printHandOfPlayer(int* array, const int size);
    
    void printDeckNumbers();
    int getSuit(const int card);
    int getFace(const int card);
    bool isPair(int* array, const int size);
    bool isTrio(int* array, const int size);
    bool isCare(int* array, const int size);
    bool isFhlash(int* array, const int size);
    bool isStraight(int* array, const int size);
    int isKicker(int* array);
    void printSuits(int* array, const int size);
    void printCombinations(int* array, const int size);
    void sethandOfPlayer(int* array, const int& size);
    void printPlayersHands(int* array, const int size);
    void printWiner(int* array, const int size);
    int Winer(const int player1, const int player2);

private:
    int handOfPlayer1_[5];
    int handOfPlayer2_[5];
    int handOfPlayer3_[5];
    int deck_ [4][13];
};

