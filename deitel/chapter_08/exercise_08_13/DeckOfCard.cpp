#include "DeckOfCard.hpp"
#include <iostream>
#include <iomanip>
#include <stdlib.h>

static const char *suit[4] = {"Hearts", "Diamonds", "Clubs", "Spades"};
static const char *face[13] = {"Ace", "Deuce", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", 
                               "Jack", "Queen", "King"};

DeckOfCard::DeckOfCard() 
{
    for (int row = 0; row <= 3; ++row) {
        for (int column = 0; column <= 12; ++ column) {
            deck_[row][column] = 0;
        }
    }
    std::srand(::time(0));
}

void
DeckOfCard::shuffle() {
    int row;
    int column;
    for (int card = 1; card <= 52; ++card) {
        do {
            row = ::rand() % 4;
            column = ::rand () % 13;
        } while (0 != deck_[row][column]);
        deck_[row][column] = card;
    }
}

void
DeckOfCard::deal()
{
    for (int card = 1; card <= 52; card++) {
        for (int row = 0; row <= 3; row++) {
            for (int column = 0; column <= 12; column++) {
                if (deck_[row][column] == card) {
                    std::cout << std::setw(5) << std::right << face[column]
                    << " of " << std::setw(8) << std::left << suit[row]
                    << (card % 2 == 0 ? '\n' : '\t');
                }
            }
        }
    }
}

void
DeckOfCard::sethandOfPlayer(int* array, const int& size)
{
    int handSize = 0;
    for (int i = 0; i < 4; ++i) {
        for (int j = 0; j < 13; ++j) {
            if (handSize == size) {
                return;
            } 
            while(deck_[i][j] != 0) {
                array[handSize] = deck_[i][j];
                deck_[i][j] = 0;
                ++handSize;
                break;
            }
        }
    }
}

void
DeckOfCard::handOfPlayer(int* array, const int size)
{
    sethandOfPlayer(array, size);
}


void
DeckOfCard::printPlayersHands(int* array, const int size)
{
    for (int i = 0; i < size; ++i) {
        const int card = array[i];
        const int row = getSuit(card);
        const int column = getFace(card);
        std::cout << face[column] << " of " << suit[row] << std::endl;
    }
}

void
DeckOfCard::printHandOfPlayer(int* array, const int size)
{

    printPlayersHands(int* array, const int size);
    std::cout << std::endl;
    const int win = isKicker(int* array);
    if (isPair(int* array, const int size)) {
        std::cout << "You have Pair\n"; win += 1000;
    }
    if (isTrio(int* array, const int size)) {
        std::cout << "You have Trio\n"; win += 2000;
    }
    if (isCare(int* array, const int size)) {
        std::cout << "You have Care\n"; win += 3000;
    }
    if (isStraight(int* array, const int size)) {
        std::cout << "You have Straight\n"; win += 4000;
    }
    if (isFhlash(int* array, const int size)) {
        std::cout << "You have Flash\n"; win += 5000;
    } 
    std::cout << std::endl;
    std::cout << "Kicker is " << win << std::endl;
    printCombinations(int* array, const int size);
}

int
DeckOfCard::Winer(const int player1, const int player2)
{
    if (player1 > player2) {
        return 1;
    }
    if (player1< player2) {
        return 2;
    }
    return 0;
} 

void 
DeckOfCard::printWiner() 
{
    const int winer = Winer(winPlayer1_, winPlayer2_);
    if (1 == winer) {
        std::cout << "Player 1 WIN !\n";
        return;
    }
    if (2 == winer) {
        std::cout << "Player 2 WIN ! \n";
        return;
    }
    std::cout << "Draw !";

}

int
DeckOfCard::getSuit(const int card)
{
    return (card - 1) / 13;
}

int
DeckOfCard::getFace(const int card)
{
    return (card - 1) % 13;
}

int
DeckOfCard::isKicker(int* array) 
{
    int faces[13] = {0};
    for (int myhand = 0; myhand < 5; ++myhand) {
        const int card = array[myhand];
        const int face = getFace(card);
        ++faces[face];
    }
    int maxCount = 0;
    int kicker;
    for (int i = 12; i >= 0; --i) {
        if (faces[i] > maxCount) {
            kicker = i;
            break;
        }
    }
    return kicker;
}

bool  
DeckOfCard::isPair(int* array, const int size)
{
    int faces[13] = {0};
    for (int myhand = 0; myhand < size; ++myhand) {
        const int card = array[myhand];
        const int face = getFace(card);
        ++faces[face];
    }
    for (int i = 0; i < 13; ++i) {
        if (2 == faces[i]) {
            return true;
        }
    }
    return false;
}

bool
DeckOfCard::isTrio(int* array, const int size)
{
    int faces[13] = {0};
    for (int myhand = 0; myhand < size; ++myhand) {
        const int card = array[myhand];
        const int face = getFace(card);
        ++faces[face];
    }
    for (int i = 0; i < 13; ++i) {
        if (3 == faces[i]) {
            return true;
        }
    }
    return false;
}

bool
DeckOfCard::isCare(int* array, const int size)
{
    int faces[13] = {0};
    for (int myhand = 0; myhand < size; ++myhand) {
        const int card = array[myhand];
        const int face = getFace(card);
        ++faces[face];
    }
    for (int i = 0; i < 13; ++i) {
        if (4 == faces[i]) {
            return true;
        }
    }
    return false;
}

bool
DeckOfCard::isStraight(int* array, const int size)
{
    int faces[13] = {0};
    for (int myhand = 0; myhand < size; ++myhand) {
        const int card = array[myhand];
        const int face = getFace(card);
        ++faces[face];
    }
    for (int i = 0; i < 11; ++i) {
        if (2 == faces[i]) {
            return false;
        }
        if (1 == faces [i]) {
            if (faces[i] == 1 && faces[i + 1]  == 1 && faces [i + 2] == 1 && faces [i + 3] == 1 && faces [i + 4] == 1) {
                return true; 
            }
        }
    }
    return false;
}

bool
DeckOfCard::isFhlash(int* array, const int size)
{
    int suits[4] = {0};
    for (int myhand = 0; myhand < size; ++myhand) {
        const int card = array[myhand];
        const int suit = getSuit(card);
        ++suits[suit];
    }
    for (int i = 0; i < 5; ++i) {
        if (5 == suits[i]) {
            return true;
        }
    }
    return false;
}

void
DeckOfCard::printCombinations(int* array, const int size)
{
    int faces[13] = {0};
    for (int myhand = 0; myhand < size; ++myhand) {
        const int card = array[myhand];
        const int face = getFace(card);
        ++faces[face];
    }
    for (int i = 0; i < 13; ++i) {
        std::cout << faces[i] << " ";
    }
}


/*void
DeckOfCard::printSuits()
{
    int suits [4] = {0};
    for (int myhand = 0; myhand < 5; ++myhand) {
        const int card = handOfPlayer1_[myhand];
        const int suit = getSuit(card);
        ++suits[suit];  
    }
    for (int i = 0; i < 4; ++i) {
        std::cout << suits[i] << " ";      
    }
}
*/
