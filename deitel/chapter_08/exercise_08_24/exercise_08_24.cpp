#include <iostream>

void
sortingRight(int* array, const int size)
{
    int valueOfSmalest;
    int indexOfSmalest;
    bool flag = true;
    for (int i = size; i >= 0; --i) {
        if (array[i] == 37) {
            int temp = array[i];
            array[i] = valueOfSmalest;
            array[indexOfSmalest] = temp;     
            break;
        }
        for (int k = size; flag; --k) {
            if (array[k] < 37) {
                valueOfSmalest = array[k];
                indexOfSmalest = k;
                flag = false;
            }
        }
    }
}

void 
printArray(int* array, const int size)
{
    for (int i = 0; i < size; ++i) {
        std::cout << array[i] << " ";
    }
}

int
main()
{
    const int SIZE = 10;
    int array[SIZE] = {37, 2, 6, 4, 89, 8, 10, 12, 68, 45};
    printArray(array, SIZE);
    std::cout << std::endl;
    sortingRight(array, SIZE);
    printArray(array, SIZE);
}

