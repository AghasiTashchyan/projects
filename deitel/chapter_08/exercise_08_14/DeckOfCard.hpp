
class DeckOfCard
{
public:
    DeckOfCard();
    void shuffle();
    void deal();
    void handOfDiller();
    void handOfPlayer2();
    void printHandOfPlayer2();
    void printHandOfPlayer();
    void printDeckNumbers();
    int getSuit(const int card);
    int getFace(const int card);
    bool isPair(int* array, const int size);
    bool isTrio(int* array, const int size);
    bool isCare(int* array, const int size);
    bool isFhlash(int* array, const int size);
    bool isStraight(int* array, const int size);
    int isKicker(int* array);
    void printSuits(int* array, const int size);
    void printCombinations(int* array, const int size);
    void sethandOfPlayer(int* array, const int& size, int numberPlayer);
    void printPlayersHands(int* array, const int size);
    void printHandOfDiller(int* array, const int size);
    void printWiner();
    void cardInterface();
    int Winer(const int player1, const int player2);

private:
    const int PLAYER_CARD_COUNT = 5;
    int winPlayer1_ = 0;
    int winPlayer2_ = 0;
    int handOfPlayer1_[5];
    int handOfPlayer2_[5];
    int deck_ [4][13];
};

