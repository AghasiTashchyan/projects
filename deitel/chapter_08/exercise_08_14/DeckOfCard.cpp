#include "DeckOfCard.hpp"
#include <iostream>
#include <iomanip>
#include <stdlib.h>

static const char *suit[4] = {"Hearts", "Diamonds", "Clubs", "Spades"};
static const char *face[13] = {"Ace", "Deuce", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", 
                               "Jack", "Queen", "King"};

DeckOfCard::DeckOfCard() 
{
    for (int row = 0; row <= 3; ++row) {
        for (int column = 0; column <= 12; ++ column) {
            deck_[row][column] = 0;
        }
    }
    std::srand(::time(0));
}

void
DeckOfCard::shuffle() {
    int row;
    int column;
    for (int card = 1; card <= 52; ++card) {
        do {
            row = ::rand() % 4;
            column = ::rand () % 13;
        } while (0 != deck_[row][column]);
        deck_[row][column] = card;
    }
}

void
DeckOfCard::deal()
{
    for (int card = 1; card <= 52; card++) {
        for (int row = 0; row <= 3; row++) {
            for (int column = 0; column <= 12; column++) {
                if (deck_[row][column] == card) {
                    std::cout << std::setw(5) << std::right << face[column]
                    << " of " << std::setw(8) << std::left << suit[row]
                    << (card % 2 == 0 ? '\n' : '\t');
                }
            }
        }
    }
}

void
DeckOfCard::sethandOfPlayer(int* array, const int& size, int numberPlayer) {
    int k = 0;
    int j = 0;
    if (2 == numberPlayer) {
        j = 5;
    } 
    for (int i = 0; i < size; ++i) {
        j += i;
        array[i] = deck_[k][j];
    }
}
void
DeckOfCard::handOfDiller()
{
    sethandOfPlayer(handOfPlayer1_, PLAYER_CARD_COUNT, 1);
}
void
DeckOfCard::printHandOfDiller(int* array, const int size)
{
    for (int i = 0; i < size; ++i) {
        const int card = array[i];
        const int row = getSuit(card);
        const int column = getFace(card);
        
        ///std::cout << face[column] << " of " << suit[row] << std::endl;
        cardInterface();
    }
}

void
DeckOfCard::handOfPlayer2()
{
    sethandOfPlayer(handOfPlayer2_, PLAYER_CARD_COUNT, 2); 
}

void
DeckOfCard::printPlayersHands(int* array, const int size)
{
    for (int i = 0; i < size; ++i) {
        const int card = array[i];
        const int row = getSuit(card);
        const int column = getFace(card);
        std::cout << face[column] << " of " << suit[row] << std::endl;
    }
}

void
DeckOfCard::printHandOfPlayer()
{
    printHandOfDiller(handOfPlayer1_,PLAYER_CARD_COUNT); 
    ///printPlayersHands(handOfPlayer1_, PLAYER_CARD_COUNT);
    std::cout << std::endl;
    winPlayer1_= isKicker(handOfPlayer1_);
    if (isPair(handOfPlayer1_, PLAYER_CARD_COUNT)) {
        std::cout << "You have Pair\n"; winPlayer1_ += 1000;
    }
    if (isTrio(handOfPlayer1_, PLAYER_CARD_COUNT)) {
        std::cout << "You have Trio\n"; winPlayer1_ += 2000;
    }
    if (isCare(handOfPlayer1_, PLAYER_CARD_COUNT)) {
        std::cout << "You have Care\n"; winPlayer1_ += 3000;
    }
    if (isStraight(handOfPlayer1_, PLAYER_CARD_COUNT)) {
        std::cout << "You have Straight\n"; winPlayer1_ += 4000;
    }
    if (isFhlash(handOfPlayer1_, PLAYER_CARD_COUNT)) {
        std::cout << "You have Flash\n"; winPlayer1_ += 5000;
    } 
    std::cout << std::endl;
    std::cout << "Kicker is " << winPlayer1_ << std::endl;
    printCombinations(handOfPlayer1_, PLAYER_CARD_COUNT);
}

void
DeckOfCard::printHandOfPlayer2()
{
    printPlayersHands(handOfPlayer2_, PLAYER_CARD_COUNT);
    std::cout << std::endl;
    winPlayer2_= isKicker(handOfPlayer2_);
    if (isPair(handOfPlayer2_, PLAYER_CARD_COUNT)) {
        std::cout << "You have Pair\n"; winPlayer2_ += 1000;
    }
    if (isTrio(handOfPlayer2_, PLAYER_CARD_COUNT)) {
        std::cout << "You have Trio\n"; winPlayer2_ += 2000;
    }
    if (isCare(handOfPlayer2_, PLAYER_CARD_COUNT)) {
        std::cout << "You have Care\n"; winPlayer2_ += 3000;
    }
    if (isStraight(handOfPlayer2_, PLAYER_CARD_COUNT)) {
        std::cout << "You have Straight\n"; winPlayer2_ += 4000;
    }
    if (isFhlash(handOfPlayer2_, PLAYER_CARD_COUNT)) {
        std::cout << "You have Flash\n"; winPlayer2_ += 5000;
    }
    std::cout << std::endl;
    std::cout << " Kicker is " <<  winPlayer2_ << std::endl;
    printCombinations(handOfPlayer2_, PLAYER_CARD_COUNT);
}

int
DeckOfCard::Winer(const int player1, const int player2)
{
    if (player1 > player2) {
        return 1;
    }
    if (player1< player2) {
        return 2;
    }
    return 0;
} 

void 
DeckOfCard::printWiner() 
{
    const int winer = Winer(winPlayer1_, winPlayer2_);
    if ( 1 == winer) {
        std::cout << "Player 1 WIN !\n";
        return;
    }
    if ( 2 == winer) {
        std::cout << "Player 2 WIN ! \n";
        return;
    }
    std::cout << "Draw !";

}

int
DeckOfCard::getSuit(const int card)
{
    return (card - 1) / 13;
}

int
DeckOfCard::getFace(const int card)
{
    return (card - 1) % 13;
}

int
DeckOfCard::isKicker(int* array) 
{
    int faces[13] = {0};
    for (int myhand = 0; myhand < 5; ++myhand) {
        const int card = array[myhand];
        const int face = getFace(card);
        ++faces[face];
    }
    int maxCount = 0;
    int kicker;
    for (int i = 12; i >= 0; --i) {
        if (faces[i] > maxCount) {
            kicker = i;
            break;
        }
    }
    return kicker;
}

bool  
DeckOfCard::isPair(int* array, const int size)
{
    int faces[13] = {0};
    for (int myhand = 0; myhand < size; ++myhand) {
        const int card = array[myhand];
        const int face = getFace(card);
        ++faces[face];
    }
    for (int i = 0; i < 13; ++i) {
        if (2 == faces[i]) {
            return true;
        }
    }
    return false;
}

bool
DeckOfCard::isTrio(int* array, const int size)
{
    int faces[13] = {0};
    for (int myhand = 0; myhand < size; ++myhand) {
        const int card = array[myhand];
        const int face = getFace(card);
        ++faces[face];
    }
    for (int i = 0; i < 13; ++i) {
        if (3 == faces[i]) {
            return true;
        }
    }
    return false;
}

bool
DeckOfCard::isCare(int* array, const int size)
{
    int faces[13] = {0};
    for (int myhand = 0; myhand < size; ++myhand) {
        const int card = array[myhand];
        const int face = getFace(card);
        ++faces[face];
    }
    for (int i = 0; i < 13; ++i) {
        if (4 == faces[i]) {
            return true;
        }
    }
    return false;
}

bool
DeckOfCard::isStraight(int* array, const int size)
{
    int faces[13] = {0};
    for (int myhand = 0; myhand < size; ++myhand) {
        const int card = array[myhand];
        const int face = getFace(card);
        ++faces[face];
    }
    for (int i = 0; i < 11; ++i) {
        if (2 == faces[i]) {
            return false;
        }
        if (1 == faces [i]) {
            if (faces[i] == 1 && faces[i + 1]  == 1 && faces [i + 2] == 1 && faces [i + 3] == 1 && faces [i + 4] == 1) {
                return true; 
            }
        }
    }
    return false;
}

bool
DeckOfCard::isFhlash(int* array, const int size)
{
    int suits[4] = {0};
    for (int myhand = 0; myhand < size; ++myhand) {
        const int card = array[myhand];
        const int suit = getSuit(card);
        ++suits[suit];
    }
    for (int i = 0; i < 5; ++i) {
        if (5 == suits[i]) {
            return true;
        }
    }
    return false;
}

void
DeckOfCard::printCombinations(int* array, const int size)
{
    int faces[13] = {0};
    for (int myhand = 0; myhand < size; ++myhand) {
        const int card = array[myhand];
        const int face = getFace(card);
        ++faces[face];
    }
    for (int i = 0; i < 13; ++i) {
        std::cout << faces[i] << " ";
    }
}

void
DeckOfCard::cardInterface() 
{
    for (int i = 0; i < 1; ++i) {
        for (int j = 0; j <= 3; ++j) {  
            std::cout << "# ";
        }
        std::cout << "\t";
    } 
}

/*void
DeckOfCard::printSuits()
{
    int suits [4] = {0};
    for (int myhand = 0; myhand < 5; ++myhand) {
        const int card = handOfPlayer1_[myhand];
        const int suit = getSuit(card);
        ++suits[suit];  
    }
    for (int i = 0; i < 4; ++i) {
        std::cout << suits[i] << " ";      
    }
}
*/
