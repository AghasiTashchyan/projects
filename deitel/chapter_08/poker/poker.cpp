#include "DeckOfCard.hpp"
#include <iostream>
#include <cstdlib>
#include <iomanip>
#include <ctime>

int 
main()
{
    DeckOfCard poker;
    poker.shuffle();
    poker.deal();
    return 0;
}

