#include "DeckOfCard.hpp"
#include <iostream>
#include <iomanip>

DeckOfCard::DeckOfCard() 
{
    for (int row = 0; row <= 3; ++row) {
        for (int column = 0; column <= 12; ++ column) {
            deck_[row][column] = 0;
        }
    }
    std::srand(::time(0));
}
void
DeckOfCard::shuffle() {
    int row;
    int column;
    for (int card = 1; card <= 52; ++card) {
        do {
            row = ::rand() % 4;
            column = ::rand () % 13;
        } while (0 != deck_[row][column]);
            deck_[row][column] = card;
    }
}



void DeckOfCard::deal()
{
    static const char *suit[ 4 ] = { "Hearts", "Diamonds", "Clubs", "Spades" };
    static const char *face[ 13 ] =
    { "Ace", "Deuce", "Three", "Four", "Five", "Six", "Seven",
      "Eight", "Nine", "Ten", "Jack", "Queen", "King" };
     for (int card = 1; card <= 52; card++) {
        for (int row = 0; row <= 3; row++) {
           for (int column = 0; column <= 12; column++) {
                if (deck_[row][column] == card) {
                    std::cout << std::setw(5) << std::right << face[column]
                    << " of " << std::setw(8) << std::left << suit[row]
                    << (card % 2 == 0 ? '\n' : '\t');
                }
            }
        }
    }
}
