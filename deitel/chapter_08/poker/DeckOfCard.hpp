
class DeckOfCard
{
public:
    DeckOfCard();
    void shuffle();
    void deal();

private:
    int deck_ [4][13];
};
