

class Player 
{
public:

    Player();
    bool isPair();
    bool isTrio();
    bool isCare();
    bool isFhlash();
    bool isStraight();
    int getSize() const;
    void setCard(const int index, const int cardNumber);
    void printHandOfPlayer() 
    int getSuit(const int card);
    int getFace(const int card);
private:
    static const int SIZE = 5;
    int hand_[SIZE];
};

