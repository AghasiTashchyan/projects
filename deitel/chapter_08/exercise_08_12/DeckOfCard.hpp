
class DeckOfCard
{
public:
    DeckOfCard();
    void shuffle();
    void deal();
    void handOfPlayer(int* hand);
    void printHandOfPlayer(int* hand);
    void cardFaices(int* hand);
    void cardSuits(int* hand);
private:
    int deck_ [4][13];
};

