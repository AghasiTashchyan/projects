#include "DeckOfCard.hpp"
#include "Player.hpp"
#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <unistd.h>

static const char *suit[4] = {"Hearts", "Diamonds", "Clubs", "Spades"};
static const char *face[13] = {"Ace", "Deuce", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", 
                               "Jack", "Queen", "King"};


DeckOfCard::DeckOfCard() 
{
    for (int row = 0; row <= 3; ++row) {
        for (int column = 0; column <= 12; ++ column) {
            deck_[row][column] = 0;
        }
    }
    if (::isatty(STDIN_FILENO)) {
        std::srand(::time(0));
    }
}

void
DeckOfCard::shuffle() {
    int row;
    int column;
    for (int card = 1; card <= 52; ++card) {
        do {
            row = ::rand() % 4;
            column = ::rand () % 13;
        } while (0 != deck_[row][column]);
        deck_[row][column] = card;
    }
}

void
DeckOfCard::deal()
{
    for (int card = 1; card <= 52; card++) {
        for (int row = 0; row <= 3; row++) {
            for (int column = 0; column <= 12; column++) {
                if (deck_[row][column] == card) {
                    std::cout << std::setw(5) << std::right << face[column]
                    << " of " << std::setw(8) << std::left << suit[row]
                    << (card % 2 == 0 ? '\n' : '\t');
                }
            }
        }
    }
}

void
DeckOfCard::handOfPlayer(Player& p1)
{
    int card = 0;
    for (int i = 0; i < 4; ++i) {
        for (int j = 0; j < 13; ++j) {
            if (card == 5) {
                return;
            }
            while(deck_[i][j] != 0) {
                p1.setCard(car, deck_[i][j]);
                deck_[i][j] = 0;
                ++card;
                break;
            }
        }
    }
}

int
DeckOfCard::getSuit(const int card)
{
    return (card - 1) / 13;
}

int
DeckOfCard::getFace(const int card)
{
    return (card - 1) % 13;
}

