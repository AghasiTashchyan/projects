#include "Player.hpp"
#include <cassert>

int faces[13] = {0};
int suits[4] = {0};

Player::Player()
{
    hand_[SIZE] = {0};
}

void
Player::setCard(const int index, const int cardNumber)
{
        hand_[index] = cardNumber;
}

int
Player::getSize() const
{

    return SIZE;
}

void
Player::cardFaices() 
{
    int faces[13] = {0};
    for (int i = 0; i < 5; ++i) {
        const int card = hand_[i];
        const int face = getFace(card);
        ++faces[face];
    }
}

void
Player::cardSuits()
{
    int suits[4] = {0};
    for (int i = 0; i < 5; ++i) {
        const int card = hand_[i];
        const int suit = getSuit(card);
        ++suits[suit];
    }
}

bool
Player::isTrio()
{
    for (int i = 0; i < 13; ++i) {
        if (3 == faces[i]) {
            return true;
        }
    }
    return false;
}

bool
Player::isCare()
{
    for (int i = 0; i < 13; ++i) {
        if (4 == faces[i]) {
            return true;
        }
    }
    return false;
}

bool
Player::isStraight()
{
    for (int i = 0; i < 11; ++i) {
        if (2 == faces[i]) {
            return false;
        }
        if (1 == faces [i]) {
            if (faces[i] == 1 && faces[i + 1]  == 1 && faces [i + 2] == 1 && faces [i + 3] == 1 && faces [i + 4] == 1) {
                return true; 
            }
        }
    }
    return false;
}

bool
Player::isFhlash()
{
    for (int i = 0; i < 5; ++i) {
        if (5 == suits[i]) {
            return true;
        }
    }
    return false;
}

bool
Player::isPair()
{
    for (int i = 0; i < 13; ++i) {
        if (2 == faces[i]) {
            return true;
        }
    }
    return false;
}



void
Player::printHandOfPlayer()
{
    for (int i = 0; i < 5; ++i) {
        const int card = hand_[i];
        const int row = getSuit(card);
        const int column = getFace(card);
        std::cout << face[column] << " of " << suit[row] << std::endl;
    }
}
