#include <iostream>
#include <unistd.h>
#include <climits>

int 
main() 
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input statement: ";
    }
    int statemant = 0;
    std::cin >> statemant;
    int smallest = INT_MAX;
    for (int i = 0; i < statemant; ++i) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Input number: ";
        }
        int number = 0;
        std::cin >> number;
        if (number < smallest) {
            smallest = number;
        }
    }
    std::cout << "Smallest is " << smallest << std::endl;
    return 0;
}

