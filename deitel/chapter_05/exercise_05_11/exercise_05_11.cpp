#include <iostream>
#include <iomanip>

int
main()
{
    double principal = 1000.0; 
    std::cout << std::fixed << std::setprecision(2);
    for (int rate = 5; rate <= 10; ++rate) {
        double amount = principal;
        double oneAndRate = static_cast<double>(100 + rate) / 100;
        std::cout << "Rate: " << rate << std::endl;
        std::cout << "Year " << std::setw(24) << "Amount " << std::endl;
        for (int year = 1; year <= 10; ++year) {                                                            
            amount *= oneAndRate;             
            std::cout << std::setw(4) << year << std::setw(24) << amount << std::endl;
        }
    }
    return 0;
}

