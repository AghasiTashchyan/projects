#include <iostream>

int
main()
{
    int x = 3, y = 9, a = 2, b = 2, g = 5, i = 3, j = 7;

    bool original = !(x < 5) && !(y >= 7);
    bool equivalent = !((x < 5) || (y >= 7));
    std::cout <<((original == equivalent)? "Equivalent\n": "Not equivalent \n");
    
    original = !(a == b) || !(g != 5);
    equivalent = !((a == b) && (g != 5));
    std::cout <<((original == equivalent)? "Equivalent\n": "Not equivalent \n");

    original = !((x <= 8) && (y > 4));
    equivalent = !(x <= 8) || !(y > 4);
    std::cout <<((original == equivalent)? "Equivalent\n": "Not equivalent \n");
    
    original = !((i > 4) || (j <= 6));
    equivalent = !(i > 4) && !(j <= 6);
    std::cout <<((original == equivalent)? "Equivalent\n": "Not equivalent \n");
    return 0;
}

