#include <iostream>
#include <unistd.h>
#include <iomanip>

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input start year: ";
    } 
    int startYear;
    std::cin >> startYear;
    if (0 > startYear) {
        std::cout << "Error 1: Invalid start year" << std::endl;
    }

    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input finesh year: ";
    } 
    int finishYear;
    std::cin >> finishYear;
    if (finishYear < startYear) {
        std::cout << "Error 2: finish year can not be lees start years" << std::endl;
    }
       
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input deposit: ";
    }    
    double deposit;
    std::cin >> deposit;
    if (0 > deposit) {
        std::cout << "Error 3: Invalid value of sum " << std::endl;
    }

    int year = finishYear - startYear ;
    std::cout << "Procent" <<std::setw(9) <<  "Year" <<std::setw(4)  <<"   Deposit balane" << std::endl; 
    for (int i = 5; i <= 10; ++i) {
        long double eachProcent = deposit;
        for (int j = startYear; j < finishYear; ++j) {
            eachProcent *= static_cast<long double>(i + 100) / 100;
        }
        std::cout << i << std::setw(14) << year << std::setw(19) << eachProcent << std::endl;
    }
    return 0;
}

