#include <iostream>
#include <unistd.h>

int 
main()
{
    int product = 1;
    for (int i = 3; i <= 15; i += 2) {
        product *= i;
    }
    std::cout << "Product is " << product << std::endl;
    return 0;
}

