#include<iostream>
#include <cmath>

int 
main () 
{
    int size = 9;
    int halfSize = size / 2;
    for (int y = -halfSize; y <= halfSize; ++y) { 
        for (int x = -halfSize; x <= halfSize; ++x) {
            std::cout << ((::abs(x) + ::abs(y) <= halfSize) ? "*" : " ");   
        }
        std::cout << std::endl;
    }     
    return 0;
}

