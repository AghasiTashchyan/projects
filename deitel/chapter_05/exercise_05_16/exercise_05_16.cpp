#include <iostream>
#include <iomanip>
#include <cmath>      

int
main()
{
    int principal = 1000;
    std::cout << "Year " << std::setw(20) << "Amount on deposit " << std::endl;
    std::cout << std::fixed << std::setprecision(2);
    principal *= 100;
    for (int year = 1; year <= 10; ++year) {                                                            
        principal = principal * 105 / 100;
        int dollar = principal / 100;
        int cent = principal % 100;
        std::cout << std::setw(2) << year << std::setw(12) << "Dolar: " << dollar << "  " << "Cent: " << cent << std::endl;
    }
    return 0;
}

