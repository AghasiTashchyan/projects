#include <iostream>
#include <string>
#include <cstdio>
#include <unistd.h>

class GradeBook 
{
public:
    GradeBook (const std::string name);
    void setcoursename(const std::string name);
    std::string getcoursename();
    void displayMessage();
    void inputGrades();
    void displayGradeReport();
private:
    std::string coursename_;
    int acount_; 
    int bcount_;
    int ccount_;
    int dcount_;
    int fcount_;
    int totalscore_;
    int average_;
};

