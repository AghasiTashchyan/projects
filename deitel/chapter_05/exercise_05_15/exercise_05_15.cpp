#include "GradeBook.hpp"
#include <iostream>
#include <string>
#include <cstdio>
#include <unistd.h>
#include <stdio.h>

int
main()
{
    GradeBook  myGradeBook("CS101 C++ programing. ");
    myGradeBook.displayMessage();
    myGradeBook.inputGrades();
    myGradeBook.displayGradeReport();
    return 0;
}

