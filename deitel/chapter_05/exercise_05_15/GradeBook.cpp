#include "GradeBook.hpp"
#include <iostream>
#include <string>
#include <cstdio>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

GradeBook::GradeBook (const std::string name) 
{
    setcoursename(name);
    acount_ = 0; 
    bcount_ = 0;
    ccount_ = 0;
    dcount_ = 0;
    fcount_ = 0;
}

void
GradeBook::setcoursename(const std::string name)
{
    if (name.length() <= 25) {
        coursename_ = name;
        return;
    }
    coursename_ = name.substr(0, 25);
    if (::isatty(STDIN_FILENO)) {
        std::cout << "name \"" << name << "\" exceeds maximum length (25).\n"
                  << "limiting coursename to first 25 characters.\n" << std::endl;
    }
}

std::string
GradeBook::getcoursename() 
{
    return coursename_;
}

void
GradeBook::displayMessage() 
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "welcome to the grade book for \n" << getcoursename() << "!\n" << std::endl;
    }
}

void
GradeBook::inputGrades() 
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "input the letter grades. (input the eof character to end input ( ctrl+d )): " << std::endl;
    }
    int grade = 0;
    while ((grade = std::cin.get()) != EOF) {
        if (::isatty(STDIN_FILENO)){
            std::cout << "grade : " << grade << std::endl;
        }
        switch (grade) {                                                   
        case 'a': ++acount_; totalscore_ += 4; break;
        case 'A': ++acount_; totalscore_ += 4; break;
        case 'b': ++bcount_; totalscore_ += 3; break; 
        case 'B': ++bcount_; totalscore_ += 3; break;                                                             
        case 'c': ++ccount_; totalscore_ += 2; break; 
        case 'C': ++ccount_; totalscore_ += 2; break;                                              
        case 'd': ++dcount_; totalscore_ += 1; break; 
        case 'D': ++dcount_; totalscore_ += 1; break;                    
        case 'f': ++fcount_; break;
        case 'F': ++fcount_; break;
        case '\t': break; 
        case ' ': break;
        default: std::cout << "incorrect letter grade entered. "     
                           << "input a new grade. " << std::endl;
        break; 
        }
        int countpoints = acount_ + bcount_ + ccount_ + dcount_ + fcount_;
        if (0 == countpoints) {
            std::cout << "Error 1: Invalid value" << std::endl;
            ::exit(1);
        }
        average_ = totalscore_ / countpoints;
    }
}

void
GradeBook::displayGradeReport() 
{
    std::cout << "\n\nnumber of students who received each letter grade:"
              << "\na: " << acount_  << "\nb: " << bcount_
              << "\nc: " << ccount_  << "\nd: " << dcount_ 
              << "\nf: " << fcount_  << std::endl;
    std::cout << "average is " << average_ << std::endl;
}
