#include <iostream>
#include <unistd.h>
#include <iomanip>

const bool terminal = ::isatty(STDIN_FILENO);
    
int
main()
{
    for (int i = 0; i < 5; ++i) {
        if (terminal) {
            std::cout << "Input" << std::setw(20) << "number:";
        }
        int number;
        std::cin >> number;
        if (number <= 0) {
            std::cout << "Error 1: Invalid number." << std::endl;
            return 1;
        }
        for (int j = number; j > 0; --j) {
            std::cout << "*";
        }
        std::cout << std::endl;
    }
    return 0;
}

