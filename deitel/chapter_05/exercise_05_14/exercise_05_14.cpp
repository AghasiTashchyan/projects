#include <iostream>
#include <unistd.h>

int
main()
{
    double sum = 0;
    while (true) {
        if(::isatty(STDIN_FILENO)) {
            std::cout << "Input product number (-1 to quit): ";
        }
        int productNumber;
        std::cin >> productNumber;
        if (-1 == productNumber) {
            break;
        }
        if (0 > productNumber) {
            std::cout << "Error 1: Invalid product number." << std::endl;
            return 1;
        }
        if (5 < productNumber) {
            std::cout << "Error 2: Undefined product number." << std::endl;
            return 3;
        }
        if(::isatty(STDIN_FILENO)) {
            std::cout << "Input quantity sold: ";
        }
        double quantity;
        std::cin >> quantity;
        if (0 > quantity) {
            std::cout <<"Error 3: Invalid quantity value." << std::endl;
            return 2;
        }
        switch (productNumber) {
        case 1: sum += 2.98 * quantity; break;
        case 2: sum += 4.50 * quantity; break;
        case 3: sum += 9.98 * quantity; break;
        case 4: sum += 4.49 * quantity; break;
        case 5: sum += 6.87 * quantity; break;
        }
    }
    std::cout << "Total is " << sum <<  std::endl;
    return 0;
}

