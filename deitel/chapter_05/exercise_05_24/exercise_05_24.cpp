#include <iostream>
#include <unistd.h>
#include <cmath>

int 
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input odd number 1 - 19";
    }    
    int oddNumber;
    std::cin >> oddNumber;
    if (oddNumber % 2 == 0) {
        std::cout << "Error 1: Number is not odd.";
        return 1;
    }
    if (oddNumber < 0 || oddNumber > 19) {
        std::cout << "Error 2: Invalid value.";
        return 2;
    }
    int halfNumber = oddNumber / 2;
    for (int y = -halfNumber; y <= halfNumber; ++y) {
        for (int x = -halfNumber; x <= halfNumber; ++x) {
            std::cout << ((::abs(x) + ::abs(y) <= halfNumber) ? "*" : " ");   
        }
        std::cout << std::endl;
    }
    return 0;
}

