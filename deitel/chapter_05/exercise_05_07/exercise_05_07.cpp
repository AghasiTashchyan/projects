#include <iostream>
#include <unistd.h>

int
main()
{
    int x; 
    int y; 
    if (::isatty(STDIN_FILENO)){
        std::cout << "Input two integers in the range 1-20: ";
    }
    std::cin >> x >> y;  
    for (int i = 1; i <= y; ++i) {
       for (int j = 1; j <= x; ++j) { 
           std::cout << '@';
       }
       std::cout << std::endl; 
    } 
    return 0; 
}

