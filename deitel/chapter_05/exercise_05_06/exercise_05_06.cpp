#include <iostream>
#include <unistd.h>

int
main()
{   
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input count of numbers: ";
    }
    int count;
    std::cin >> count;
    if (0 > count) {
        std::cout << "Error 1: Invalid count " << std::endl;
        return 1;
    }
    int sum = 0;
    for (int i = 0; i < count; ++i) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Input number: ";
        }
        int number;
        std::cin >> number;
        sum += number;
    }
    float average = static_cast<float> (sum / count);
    std::cout << "Average is " << average << std::endl;
    return 0;
}

