#include <iostream>

int
main()
{
    int i = 1;
    for ( ; i <= 10; ++i) {
        if (i != 5) {
            std::cout << i << " ";
        }
    }
    std::cout <<"\nUsed continue to skip printing 5 " << std::endl;
    return 0;
}

