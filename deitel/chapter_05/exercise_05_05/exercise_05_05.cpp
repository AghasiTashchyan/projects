#include <iostream>
#include <unistd.h>

int 
main()
{     
    if (::isatty(STDIN_FILENO)) { 
        std::cout << "Input numbers count: ";
    }
    int count;
    std::cin >> count;
    if (count <= 0) {
        std::cout << "Error 1: Invalid count. " << std::endl;
        return 1;
    }
    int sum = 0;
    for (int i = 0; i < count; ++i) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Input number: ";
        }
        int number;
        std::cin >> number;
        sum += number;
    }
    std::cout << "Sum is " << sum << std::endl;   
    return 0;
}

