#include <iostream>
#include <string>
class Rational
{
public:
    Rational();
    Rational(int numinator, int denuminator);
    Rational plus(const Rational& object);
    Rational divide(const Rational& object);
    void printFloatingFormat();
    void gcd();
    void setNumerator(const int numinator);
    void setDenominator(const int denuminator);
    void printRationalNumber();
    int getNumerator()const;
    int getDenominator() const;
    int getIntegerValue(const std::string& prompt);
private:
    int numerator_;
    int denominator_;
};

