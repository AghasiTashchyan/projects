#include "headers/Rational.hpp"
#include <iostream>

int
main()
{
    Rational a, b;
    const int numerator = a.getIntegerValue("Input numerator: ");
    const int denominator = a.getIntegerValue("input denominator: "); 
    if (0 == denominator) {
        std::cout << "Error 1: Invalude denomerator:";
        return 1;
    }
    const int numerator2 = b.getIntegerValue("Input numerator: ");
    const int denominator2 = b.getIntegerValue("Input denominator: ");
    if (0 == denominator2) {
        std::cout << "Error 1: Invalude denomerator:";
        return 1;
    }
    a.setNumerator(numerator);
    a.setDenominator(denominator);
    b.setNumerator(numerator2);
    b.setDenominator(denominator2);
    std::cout << "Summarization of rational numbers:\n";
    Rational c = a.plus(b);
    c.printRationalNumber();
    c.printFloatingFormat();
    std::cout << "Divison of rational numbers:\n";
    Rational c1 = a.divide(b);
    c1.printRationalNumber();
    c1.printFloatingFormat();
    return 0;
}

