#include "headers/Rational.hpp"
#include <string>
#include <unistd.h>
#include <cassert>
/// bacasakan tver@ stugel algorutm@ kashxti hanum u bazmapatkum@

Rational::Rational()
{
    numerator_ = 0;
    denominator_ = 1;
}


Rational::Rational(const int nomerator, const int denominator)
{
    setNumerator(nomerator);
    setDenominator(denominator);
    gcd();
}

Rational
Rational::plus(const Rational& rhv)
{
    const int nomerator = (numerator_ * rhv.getDenominator()) + (denominator_ * rhv.getNumerator());
    const int denominator = denominator_ * rhv.getDenominator();
    return Rational(nomerator, denominator);
}

Rational
Rational::divide(const Rational& rhv)
{
    const int nomerator = numerator_ * rhv.getDenominator();
    const int denominator = denominator_ * rhv.getNumerator();
    return Rational(nomerator, denominator);


}

void
Rational::printFloatingFormat()
{
    const double result = static_cast<double>(getNumerator()) / getDenominator();
    std::cout << "Floating Point printing: " << result << std::endl;
}

void
Rational::gcd()
{
    int num = numerator_;
    int denum = denominator_;
    if (denum == 0) {
        return;
    }
    while (num % denum != 0) {
        int temp = denum;
        denum = num % denum;
        num = temp;
    
    }
    if (denum != 0) {
        numerator_ =  numerator_ / denum;
        denominator_ = denominator_ / denum;
    }
}

void
Rational::setNumerator(const int nomerator)
{
    assert(nomerator > 0);
    numerator_ = nomerator;
}

void
Rational::setDenominator(const int denominator)
{
    denominator_ = denominator;
}

void
Rational::printRationalNumber()
{
    std::cout << getNumerator() << " / " << getDenominator() << std::endl;;
}

int
Rational::getNumerator() const
{
    return numerator_;
}

int 
Rational::getDenominator() const
{
   return denominator_;
}

int 
Rational::getIntegerValue(const std::string& prompt)
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << prompt;
    }
    int result;
    std::cin >> result;
    return result;
}

