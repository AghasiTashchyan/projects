#ifndef __COMPLEX_HPP__
#define __COMPLEX_HPP__

#include <string>

class Complex 
{
public:
    Complex();
    Complex(const double realPart, const double imagenaryPart);
    void printComplexNumber() const;
    void setRealPart(const double realPart);
    void setImagenaryPart(const double imagenaryPart);
    double getRealPart() const;
    double getImagenaryPart() const;
    double getValue(const std::string& prompt) const;
    Complex adding(const Complex& rhv) const;
    Complex subtracting(const Complex& rhv) const;
private:
    double realPart_; 
    double imagenaryPart_;
};

#endif /// __COMPLEX_HPP__

