#include "headers/Complex.hpp"
#include <iostream>

int
main()
{
    Complex complex1, complex2;
    const double realA = complex1.getValue("Input real part: ");
    const double imagenA  = complex1.getValue("Input imagenary part: ");
    const double realB =  complex2.getValue("Input real part: "); ;
    const double imagenB  = complex2.getValue("Input imagenary part: ");
    complex1.setRealPart(realA);
    complex1.setImagenaryPart(imagenA);
    complex2.setRealPart(realB);
    complex2.setImagenaryPart(imagenB);

    std::cout << "Adding: ";
    Complex complex3 = complex1.adding(complex2);
    complex3.printComplexNumber();

    std::cout << "Subtracting: ";
    complex3 = complex1.subtracting(complex2);
    complex3.printComplexNumber();
    return 0;
}

