#include "headers/Complex.hpp"
#include <iostream>
#include <string>
#include <unistd.h>

Complex::Complex()
{
    realPart_ = 0;
    imagenaryPart_ = 0;
}

Complex::Complex(const double realPart, const double imagenaryPart)
{
    setRealPart(realPart);
    setImagenaryPart(imagenaryPart);
}

Complex
Complex::adding(const Complex& rhv) const
{
    const double realPart = getRealPart() + rhv.getRealPart();
    const double imagenaryPart = getImagenaryPart() + rhv.getImagenaryPart();
    return Complex(realPart, imagenaryPart);
}

Complex 
Complex::subtracting(const Complex& rhv) const
{
    const double realPart = realPart_ - rhv.realPart_;
    const double imagenaryPart = imagenaryPart_ - rhv.imagenaryPart_;
    return Complex(realPart, imagenaryPart);
}

void 
Complex::setRealPart(const double realPart)
{
    realPart_ = realPart;
}

void 
Complex::setImagenaryPart(const double imagenaryPart)
{
    imagenaryPart_ = imagenaryPart;
}

double
Complex::getRealPart() const
{
    return realPart_;
}

double
Complex::getImagenaryPart() const
{
    return imagenaryPart_;
}    

void
Complex::printComplexNumber() const
{
    if (-1 == getImagenaryPart() || 1 == getImagenaryPart()) {
        std::cout << getRealPart() << " " << "i" << std::endl; 
        return;
    }
    std::cout << getRealPart() << " " << getImagenaryPart()
              << "i" << std::endl; 
}

double
Complex::getValue(const std::string& prompt) const
{
    if(::isatty(STDIN_FILENO)) {
        std::cout << prompt;
    }
    double result;
    std::cin >> result;
    return result;
}
