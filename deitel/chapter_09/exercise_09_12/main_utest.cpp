#include "headers/Rectangel.hpp"
#include "headers/Point.hpp"
#include <gtest/gtest.h>

TEST(Rectangel, isRectangel)
{
    Point point[4] = {Point(3,1), Point(1,3), Point(4,6), Point(6,4)};
    Rectangel rectangel(point, 4);
    const bool isRectangel = rectangel.isRectangel();
    EXPECT_TRUE(isRectangel);
}

TEST(Rectangel, isSquare) 
{
    Point point[4] = {Point(1,1), Point(1,4), Point(4,4), Point(4,1)};
    Rectangel rectangel(point, 4);
    const bool isSquare = rectangel.isSquare();
    EXPECT_TRUE(isSquare);
}

TEST(Rectangel, getArea) 
{
    Point point[4] = {Point(1,1), Point(1,4), Point(4,4), Point(4,1)};
    const Rectangel rectangel(point, 4);
    const double area = rectangel.getArea();
    EXPECT_EQ(area, 9);
}

TEST(Rectangel, getPerimeter) 
{
    Point point[4] = {Point(1,1), Point(1,4), Point(4,4), Point(4,1)};
    const Rectangel rectangel(point, 4);
    const double perimeter = rectangel.getPerimeter();
    EXPECT_EQ(perimeter, 12);
}

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
