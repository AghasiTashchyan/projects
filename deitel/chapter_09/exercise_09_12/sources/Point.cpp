#include "headers/Point.hpp"

Point::Point()
    : x_(0)
    , y_(0)
{
}

Point::Point(const double x, const double y)
    : x_(x)
    , y_(y)
{
}

double
Point::getDistanceSquare(const Point& rhv) const
{      
    const double diferencSquareX = (x_ - rhv.x_) * (x_ - rhv.x_);
    const double diferencSquareY = (y_ - rhv.y_) * (y_ - rhv.y_);
    return (diferencSquareX + diferencSquareY); 
}

