#include <headers/Rectangel.hpp>
#include <iostream>
#include <cmath>
#include <cfloat>

Rectangel::Rectangel(Point* points, const int size)
{
   for (int i = 0; i < size; ++i) {
        points_[i].x_ = points[i].x_;
        points_[i].y_ = points[i].y_;
   } 
}

bool
Rectangel::isRectangel()
{
    return (isDiagonalEqual() && isAngleRight());
}

bool
Rectangel::isDiagonalEqual()
{
    const double diagonal1 = points_[0].getDistanceSquare(points_[2]);
    const double diagonal2 = points_[1].getDistanceSquare(points_[3]);
    return (::fabs(diagonal1 - diagonal2) < DBL_EPSILON);
}

bool
Rectangel::isAngleRight()
{
    const double squareAB = points_[0].getDistanceSquare(points_[1]);
    const double squareAD = points_[0].getDistanceSquare(points_[3]);
    const double diagonalSquare = points_[1].getDistanceSquare(points_[3]);
    const double sideSquareSum = squareAB + squareAD;
    return (::fabs(sideSquareSum - diagonalSquare) < DBL_EPSILON); 
}

bool
Rectangel::isSquare()
{
    return (isRectangel() && (::fabs(getLength() - getWidth()) < DBL_EPSILON));
}

double 
Rectangel::getLength() const
{
    const double length = points_[0].getDistanceSquare(points_[1]);
    const double witdh = points_[0].getDistanceSquare(points_[3]);
    return ::sqrt(((length > witdh) ? length : witdh));
}

double 
Rectangel::getWidth() const
{
    const double length = points_[0].getDistanceSquare(points_[1]);
    const double witdh = points_[0].getDistanceSquare(points_[3]);
    return ::sqrt((length < witdh) ? length : witdh);
}

double
Rectangel::getArea() const
{
   return getWidth() * getLength(); 
}

double
Rectangel::getPerimeter() const
{
    return 2 * (getLength() + getWidth());
}

