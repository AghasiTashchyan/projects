#ifndef __RECTANGEL_HPP__
#define __RECTANGEL_HPP__

#include "Point.hpp"

class Rectangel
{
public:
    Rectangel(Point* points, const int size);
    double getLength() const;
    double getWidth() const;
    double getArea() const;
    double getPerimeter() const;
    bool isRectangel();
    bool isDiagonalEqual();
    bool isAngleRight();
    bool isSquare();
private:
    static const int SIZE = 4;
    Point points_[SIZE];
};

#endif /// __RECTANGEL_HPP__
