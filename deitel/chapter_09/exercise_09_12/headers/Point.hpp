#ifndef __POINT_HPP__
#define __POINT_HPP__

struct Point
{
    Point();
    Point(const double x, const double y);
    double getDistanceSquare(const Point& rhv) const;
    double x_;
    double y_;
};

#endif /// __POINT_HPP__
