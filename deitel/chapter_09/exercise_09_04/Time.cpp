#include "Time.hpp"
#include <iostream>
#include <ctime>
#include <cassert>
#include <iomanip>

Time::Time()
{
    const int time = ::time(0) + (3600 * 4);
    const int hours = time % (24 * 60 * 60 ) / (60 * 60);
    const int minutes = time % 3600 / 60;
    const int seconds = time % 60;
    setHours(hours);
    setMinutes(minutes);
    setSeconds(seconds);    
    
}
Time::Time(const int hours, const int minutes, const int seconds)
{
    setHours(hours);
    setMinutes(minutes);
    setSeconds(seconds);
}    

void
Time::setHours(const int hours)
{
    assert(hours >= 0 && hours < 24);
    hours_ = hours;
}

void
Time::setMinutes(const int minutes)
{       
    assert(minutes >=0 && minutes < 60);
    minutes_ = minutes;
}

void
Time::setSeconds(const  int seconds)
{
    assert(seconds >= 0 && seconds < 60);
    seconds_ = seconds;
}

int 
Time::getHours()
{
    return hours_;
}

int 
Time::getMinutes()
{
    return minutes_;
}

int 
Time::getSeconds()
{
    return seconds_;
}

void
Time::printTime()
{
    std::cout << "Time is " << getHours() <<  ":"  << std::setw(2) << std::setfill('0') << getMinutes() << ":" << getSeconds() << std::endl;
}

