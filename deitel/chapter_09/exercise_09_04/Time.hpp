class Time 
{
public:
    Time();
    Time(const int hours, const int minutes, const int seconds);
    void setHours(const int hours);
    void setMinutes(const int minutes);
    void setSeconds(const int seconds);
    void printTime();
    int getHours();
    int getMinutes();
    int getSeconds();
private:
    int hours_;
    int minutes_;
    int seconds_;
};
