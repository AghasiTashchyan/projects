#include "Time.hpp"
#include <iostream>
#include <iomanip>

Time::Time()
{
    hour_ = 0;
    minute_ = 0;
    second_ = 0;
}

Time::Time(int hour, int minute, int second)                   
{                                                        
    setTime(hour, minute, second);    
}

void
Time::setTime(int hour, int minute, int second)
{
    setHour(hour);
    setMinute(minute);
    setSecond(second);
}

void
Time::setHour(int hour)
{
    hour_ = (hour >= 0 && hour < 24) ? hour : 0;
}

void 
Time::setMinute(int minute)
{
    minute_ = (minute >= 0 && minute < 60) ? minute : 0;
}

void
Time::setSecond(int second)
{
    second_ = (second >= 0 && second < 60) ? second : 0;
}

int
Time::getHour()
{
    return hour_;
}

int 
Time::getMinute()
{
    return minute_;
} 

int 
Time::getSecond()
{
    return second_;
}
void
Time::secondIncrement()
{
    ++second_;
    if (second_ == 60) {
        second_ = 0;
        ++minute_;
        if (minute_ == 60) {
            minute_ = 0;
            ++hour_;
            if (hour_ == 24) {
                hour_ = 0;
            }
        }
    }
}

void
Time::printUniversal()
{
    std::cout << std::setfill('0') << std::setw(2) << getHour() << ":"
    << std::setw(2) << getMinute() << ":" << std::setw(2) << getSecond();
}

void
Time::printStandard()
{
    std::cout << ((getHour() == 0 || getHour() == 12) ? 12 : getHour() % 12)
              << ":" << std::setfill('0') << std::setw(2) << getMinute()
              << ":" << std::setw(2) << getSecond() << (hour_ < 12 ? " AM" : " PM");
} 

