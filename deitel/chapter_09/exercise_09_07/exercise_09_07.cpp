#include "Time.hpp"
#include <iostream>
#include <unistd.h>

int 
main()
{
    Time time();
    time.printStandard();
    Time time1(22, 24, 10);
    time1.printStandard();
    std::cout << std::endl;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Incrementing into the next minute.\t";
    }
    for (int i = 0; i < 60; ++i) { 
        time1.secondIncrement();
    }
    time1.printStandard();
    std::cout << std::endl;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Incrementing into the next hour.\t";
    }
    for (int i = 0; i < 3600; ++i) {
        time1.secondIncrement();
    }
    time1.printStandard();
    std::cout << std::endl;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Incrementing into the next day\t";
    }
    for (int i = 0; i < 10800; ++i) {
        time1.secondIncrement();
    }
    time1.printStandard();
    std::cout << std::endl;
    return 0;
}

