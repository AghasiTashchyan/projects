#ifndef TIME_H
#define TIME_H

class Time
{
public:
    Time();
    Time( int = 0, int = 0, int = 0 ); 
    void setTime( int, int, int );
    void setHour( int );
    void setMinute( int ); 
    void setSecond( int );
    void secondIncrement();
    int getHour(); 
    int getMinute(); 
    int getSecond();
    void printUniversal(); 
    void printStandard();
private:
    int hour_;
    int minute_;
    int second_;
};

#endif
