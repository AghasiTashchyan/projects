#include "headers/Rational.hpp"
#include <iostream>

int
main()
{
    Rational a, b;
    const int numerator = a.getIntegerValue("Input numerator: ");
    const int denominator = a.getIntegerValue("input denominator: "); 
    const int numerator2 = b.getIntegerValue("Input numerator: ");
    const int denominator2 = b.getIntegerValue("Input denominator: ");
    a.setNumerator(numerator);
    a.setDenomirator(denominator);
    b.setNumerator(numerator2);
    b.setDenomirator(denominator2);
    Rational c(a,'+', b);
    c.printRationalNumber();
    c.printFloatingPoint();
    return 0;
}

