#include "headers/Rational.hpp"
#include <string>
#include <unistd.h>

Rational::Rational()
{
    numerator_ = 0;
    denominator_ = 0;
}

Rational::Rational(Rational a, const char simbul, Rational b) 
{
    if ('+' == simbul) {
        if (a.getDenominator() == b.getDenominator()) {
            numerator_ = a.getNumerator() + b.getNumerator();
            denominator_ = a.getDenominator();
            evklidAlgoritm();
        }
        if (a.getDenominator() != b.getDenominator()) {
            numerator_ = (a.getNumerator() * b.getDenominator()) + (a. getDenominator() * b.getNumerator());
            denominator_ = (a.getDenominator() * b.getDenominator());
            evklidAlgoritm();
        }
    }
}

void
Rational::evklidAlgoritm()
{
    int num = numerator_;
    int denum = denominator_;
    while (denum != 0) {
        int temp = denum;
        denum = num % denum;
        num = temp;
    }
    numerator_ /= num;
    denominator_ /= num;
}

void
Rational::setNumerator(const int numerator)
{
    numerator_ = numerator;
}

void
Rational::setDenomirator(const int denominator)
{
    denominator_ = denominator;
}

void
Rational::printRationalNumber()
{
    std::cout << getNumerator() << " / " << getDenominator() << std::endl;
}

void
Rational::printFloatingPoint()
{
    const double result = static_cast<double>(getNumerator()) / getDenominator();
    std::cout << "Floating format: " << result << std::endl;

}

int
Rational::getNumerator()
{
    return numerator_;
}

int 
Rational::getDenominator()
{
    return denominator_;
}

int 
Rational::getIntegerValue(const std::string& prompt)
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << prompt;
    }
    int result;
    std::cin >> result;
    return result;
}

