#include <iostream>
#include <string>
class Rational
{
public:
    Rational();
    Rational(Rational a, const char simbul, Rational b);
    void evklidAlgoritm();
    void setNumerator(const int numinator);
    void setDenomirator(const int denuminator);
    void printRationalNumber();
    void printFloatingPoint();
    int getNumerator();
    int getDenominator();
    int getIntegerValue(const std::string& prompt);
private:
    int numerator_;
    int denominator_;
};

