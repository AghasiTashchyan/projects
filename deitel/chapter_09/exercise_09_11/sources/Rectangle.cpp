#include "headers/Rectangle.hpp"
#include <iostream>
#include <unistd.h>
#include <string>
#include <cmath>

Rectangle::Rectangle()
{
}


Rectangle::Rectangle(const int Ax, const int Ay, const int Bx, const int By, const int Cx, const int Cy, const int Dx, const int Dy)
    :Ax_ (Ax)
    ,Ay_ (Ay)
    ,Bx_ (Bx)
    ,By_ (By)
    ,Cx_ (Cx)
    ,Cy_ (Cy)
    ,Dx_ (Dx)
    ,Dy_ (Dy)
{

}

int 
Rectangle::getValue(const std::string& prompt)
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << prompt;
    }
    int result;
    std::cin >> result; 
    return result;
}

void
Rectangle::setAx(const int Ax)
{
    Ax_ = (Ax >= 0.0 && Ax <= 20.0) ? Ax : 0;
}

void 
Rectangle::setAy(const int Ay)
{
    Ay_ = (Ay >= 0.0 && Ay <= 20.0) ? Ay : 0;
}

void
Rectangle::setBx(const int Bx)
{
    Bx_ = (Bx >= 0.0 && Bx <= 20.0) ? Bx : 0;
}

void 
Rectangle::setBy(const int By)
{
    By_ = (By >= 0.0 && By <= 20.0) ? By : 0;
}

void
Rectangle::setCx(const int Cx)
{
    Cx_ = (Cx >= 0.0 && Cx <= 20.0) ? Cx : 0;
}

void 
Rectangle::setCy(const int Cy)
{
    Cy_ = (Cy >= 0.0 && Cy <= 20.0) ? Cy : 0;
}

void
Rectangle::setDx(const int Dx)
{
    Dx_ = (Dx >= 0.0 && Dx <= 20.0) ? Dx : 0;
}

void 
Rectangle::setDy(const int Dy)
{
    Dy_ = (Dy >= 0.0 && Dy <= 20.0) ? Dy : 0;
}

int
Rectangle::getAx() const
{
    return Ax_;
}

int
Rectangle::getAy() const
{
    return Ay_;
}

int
Rectangle::getBx() const
{
    return Bx_;
}

int
Rectangle::getBy() const
{
    return By_;
}


int
Rectangle::getCx() const
{
    return Cx_;
}

int
Rectangle::getCy() const
{
    return Cy_;
}

int
Rectangle::getDx() const
{
    return Dx_;
}

int
Rectangle::getDy() const
{
    return Dy_;
}


double
Rectangle::getLength(const double x1, const double x2, const double y1, const double y2) const
{
    const double xSquare = (x1 - x2) * (x1 - x2);
    const double ySquare = (y1 - y2) * (y1 - y2);
    return ::sqrt(xSquare + ySquare);
}

void
Rectangle::printSums()
{
    std::cout << getAx() + getAy();
    std::cout << getBx() + getBy();
    std::cout << getCx() + getCy();
    std::cout << getDx() + getDy();
    
}

bool
Rectangle::isSquare()
{
    const double cordinatA = Ax_ + Ay_;
    const double cordinatB = Bx_ + By_;
    const double cordinatC = Cx_ + Cy_;
    const double cordinatD = Dx_ + Dy_;
    
  if ((cordinatB > cordinatA && cordinatB < cordinatC) && (cordinatD > cordinatA && cordinatD < cordinatC)) {
        const double lengthAB = getLength(getAx(), getBx(), getAy(), getBy());    
        const double lengthBC = getLength(getBx(), getCx(), getBy(), getCy());
        const double lengthDC = getLength(getDx(), getCx(), getDy(), getCy());
        const double lengthDA = getLength(getDx(), getAx(), getDy(), getAy());
        if (::abs(lengthAB - lengthBC) <= EPSILON_ && ::abs(lengthAB - lengthDC) <= EPSILON_ && ::abs(lengthAB - lengthDA) <= EPSILON_) {
            const double lengthAC = getLength(getAx(), getCx(), getAy(), getCy());    
            const double lengthDB = getLength(getDx(), getBx(), getDy(), getBy());
            if (::abs(lengthAC - lengthDB) <= EPSILON_) {
                return true;
            }
        }
    }
    return false;
}

void 
Rectangle::printCordinats() const
{
    std::cout << "Cordinats of " << "Ax = " 
              <<getAx() << " Ay = " << getAy() << std::endl;
    std::cout << "Cordinats of " << "Bx = "
              << getBx() << " By = " << getBy() << std::endl;
    std::cout << "Cordinats of " << "Cx = " 
              << getCx() << " Cy = " << getCy() << std::endl;
    std::cout << "Cordinats of " << "Dx = " 
              << getDx() << " Dy = " << getDy() << std::endl;
}

int
Rectangle::run()
{
    const int Ax = getValue("Input Ax: ");
    const int Ay = getValue("Input Ay: ");
    const int Bx = getValue("Input Bx: ");
    const int By = getValue("Input By: ");
    const int Cx = getValue("Input Cx: ");
    const int Cy = getValue("Input Cy: ");
    const int Dx = getValue("Input Dx: ");
    const int Dy = getValue("Input Dy: ");
    Rectangle a(Ax, Ay, Bx, By, Cx, Cy, Dx, Dy);
    a.printCordinats();
    if (isSquare()) {
        std::cout << "Is Square: ";
    } else {  std::cout << "Not Square";}
    a.printSums();
    return 0;
}

