#ifndef RECTANGLE_H
#define RECTANGLE_H
#include <string>

class Rectangle
{
public:
    Rectangle();
    Rectangle(const int Ax, const int Ay, const int Bx, const int By, const int Cx, const int Cy, const int Dx, const int Dy);
    void setAx(const int Ax);
    void setAy(const int Ay);
    void setBx(const int Bx);
    void setBy(const int By);
    void setCx(const int Cx);
    void setCy(const int Cy);
    void setDx(const int Dx);
    void setDy(const int Dy);
    void printCordinats() const;
    double getLength(const double x1, const double x2, const double y1, const double y2) const;
    bool isSquare();
    void printSums();
    int getAx() const;
    int getAy() const;
    int getBx() const;
    int getBy() const;
    int getCx() const;
    int getCy() const;
    int getDx() const;
    int getDy() const;
    int getValue(const std::string& prompt);
    int run();
private:
    static const int EPSILON_ = 0.0001; 
    int Ax_, Ay_, Bx_, By_, Cx_, Cy_, Dx_, Dy_;

};
 #endif

