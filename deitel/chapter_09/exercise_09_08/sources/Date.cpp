#include "headers/Date.hpp"
#include <iostream>

Date::Date(const int month, const int day, const int year)
{
    month_ = month;
    day_ = day;
    year_ = year;
}

void 
Date::setDay(const int day)
{
    if (day < 0  && day > 31) {
        day_ = 0;
    }
    day_ = day;
}

void 
Date::setMonth(const int month)
{
    if (month < 0 && month > 12) {
        month_ = 0; 
    }
    month_ = month;
}

void 
Date::setYear(const int year)
{
    if (year < 0) {
        year_ = 0;
    }
    year_ = year;
}

int 
Date::getDay() const
{
    return day_;
}

int 
Date::getMonth() const
{
    return month_;
}

int 
Date::getYear() const
{
    return year_;
}

void
Date::nexTDay()
{
    ++day_;
    if (day_ == 31) {
        day_ = 0;
        ++month_;
        if (month_ == 12) {
            month_ = 0;
            ++year_;
        }
    }
}

void 
Date::printDate() const
{
    std::cout << getMonth() << '/' << getDay() << '/' << getYear() << std::endl;
} 

