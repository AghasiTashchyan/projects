#ifndef DATE_H
#define DATE_H

class Date
{
public:
    Date (const int mount, const int day, const int year);
    void printDate() const;
    void setDay(const int day);
    void setMonth(const int mount);
    void setYear(const int year);
    void nexTDay();
    int getDay() const;
    int getMonth() const;
    int getYear() const;
private:
    int day_;
    int month_;
    int year_;
};

 #endif
