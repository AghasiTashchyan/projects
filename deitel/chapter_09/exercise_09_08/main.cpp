#include "headers/Date.hpp"
#include <iostream>

int
main()
{
   Date date1( 1, 29, 2004 );
   std::cout << "Data. ";
   date1.printDate();
   for (int i = 0; i < 31; ++i) {
        date1.nexTDay();
   }
   std::cout << "\nIncrementing into the next month. ";
   date1.printDate();
   for (int i = 0; i < 365; ++i) {
        date1.nexTDay();
   }
   std::cout << "\nIncrementing into the next year. ";
   date1.printDate();
   return 0;
}

