#ifndef __BOARD_HPP__
#define __BOARD_HPP__
#include "Player.hpp"

class Board
{
public:
    Board();
    void setBoard();
    void printBoard();
    void selectChoisInBoard(const Player& simbul, const int index);
    bool checkingBoard();
    bool checkingChoice(const int choice);
    bool isWin();
private:
    char board_ [3][3];
};

#endif /// __BOARD_HPP__
