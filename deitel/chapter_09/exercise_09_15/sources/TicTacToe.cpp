#include "headers/TicTacToe.hpp"
#include "headers/Board.hpp"
#include "headers/Player.hpp"
#include <iostream>

TicTacToe::TicTacToe(Board& board, Player& player1, Player& player2)
    : board_(board)
    , player1_(player1)
    , player2_(player2)
{
}

int
TicTacToe::run()
{
    printManue();
    const int answerManue = getValue();
    for (int i = 0; 1 == answerManue; ++i) {
        board_.printBoard();
        const int playerNumber = i % 2 + 1;
        std::cout << "Input your choice (player " << playerNumber << "): ";
        const int playerAnswer = getValue();
        if (!board_.checkingChoice(playerAnswer)) {
            std::cout << "Square is busy. Try agein\n";
            --i;
            continue;
        }
        board_.selectChoisInBoard(1 == playerNumber ? player1_ : player2_, playerAnswer); 
        if (i > 3 && board_.isWin()) {
            board_.printBoard();
            std::cout << "Player " << playerNumber << " Win !\n";
            break; 
        }
    }   
    return 0;
}

void
TicTacToe::printManue() const
{
    std::cout << "Welcome     Tic Tac Toe  !" << std::endl;
    std::cout << "1 : Start\n";
    std::cout << "2 : Exit\n";
}

int
TicTacToe::getValue()
{
    std::cout << "|> ";
    int result;
    std::cin >> result;
    return result;
}
