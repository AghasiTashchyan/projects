#include "headers/TicTacToe.hpp"
#include "headers/Board.hpp"
#include "headers/Player.hpp"
#include <iostream>

int
main()
{
    Board board;
    Player player1('x');
    Player player2('o');
    TicTacToe ticTac(board, player1, player2);
    return ticTac.run();
}

