#include "headers/Time.hpp"
#include <iostream>
#include <iomanip>

Time::Time()
    : hour_ (0)
    , minute_ (0)
    , second_ (0)
{
}

Time::Time(const int hour, const int minute , const int second) 
{
    setHour(hour);
    setMinute(minute);
    setSecond(second);
}

void
Time::setHour(const int hour)
{
    hour_ = (hour >= 0 && hour < 24) ? hour : 0;
}

void 
Time::setMinute(const int minute)
{
    minute_ = (minute >= 0 && minute < 60) ? minute : 0;
}

void
Time::setSecond(const int second)
{
    second_ = (second >= 0 && second < 60) ? second : 0;
}

int
Time::getHour() const
{
    return hour_;
}

int 
Time::getMinute() const
{
    return minute_;
} 

int 
Time::getSecond() const
{
    return second_;
}
void
Time::secondIncrement()
{
    ++second_;
    if (60 == second_) {
        second_ = 0;
        ++minute_;
        if (60 == minute_) {
            minute_ = 0;
            ++hour_;
            if (24 == hour_) {
                hour_ = 0;
            }
        }
    }
}

void
Time::printUniversal() const
{
    const char oldFill = std::cout.fill('0');
    std::cout << std::setfill('0') << std::setw(2) << getHour() << ":"
    << std::setw(2) << getMinute() << ":" << std::setw(2) << getSecond() << std::endl;
    std::cout.fill(oldFill);
}

void
Time::printStandard() const
{
    const char oldFill = std::cout.fill('0');
    std::cout << ((getHour() == 0 || getHour() == 12) ? 12 : getHour() % 12)
              << ":" << std::setfill('0') << std::setw(2) << getMinute()
              << ":" << std::setw(2) << getSecond() << (hour_ < 12 ? " AM" : " PM") << std::endl;
    std::cout.fill(oldFill);
} 

