#include "headers/DateAndTime.hpp"
#include <iostream>

int
main()
{
   DateAndTime dt(0, 10, 10, 1, 12, 2023);
   dt.printDataAndTime();
   dt.printDataAndTimeUniversal();
   for (int i = 0; i < 90000; ++i) {
       dt.secondIncrement();
   }
   std::cout << std::endl;
   dt.printDataAndTime();
   dt.printDataAndTimeUniversal();
   return 0;
}

