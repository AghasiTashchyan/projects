#include "headers/DateAndTime.hpp"
#include <iostream>
#include <iomanip>

DateAndTime::DateAndTime()
    : second_ (0)
    , minute_ (0)
    , hour_  (0)
    , month_ (0)
    , day_ (0)
    , year_ (0) {
    }

DateAndTime::DateAndTime(const int second, const int minute, const int hour, const int month, const int day, const int year)
    : second_ (second)
    , minute_ (minute)
    , hour_ (hour)
    , month_ (month)
    , day_ (day)
    , year_ (year)  
{

}

void 
DateAndTime::setDay(const int day)
{
    if (day < 0  && day > 31) {
        day_ = 0;
    }
    day_ = day;
}

void 
DateAndTime::setMonth(const int month)
{
    if (month < 0 && month > 12) {
        month_ = 0; 
    }
    month_ = month;
}

void 
DateAndTime::setYear(const int year)
{
    if (year < 0) {
        year_ = 0;
    }
    year_ = year;
}

int 
DateAndTime::getDay() const
{
    return day_;
}

int 
DateAndTime::getMonth() const
{
    return month_;
}

int 
DateAndTime::getYear() const
{
    return year_;
}

void 
DateAndTime::secondIncrement()
{
    ++second_;
    if (second_ == 60) {
        second_ = 0;
        ++minute_;
        if (minute_ == 60) {
            minute_ = 0;
            ++hour_;
            if (hour_ == 24) {
                hour_ = 0;
                nextDay();
            }
        }
    }

}

void
DateAndTime::nextDay()
{
    ++day_;
    if (day_ == 31) {
        day_ = 0;
        ++month_;
        if (month_ == 12) {
            month_ = 0;
            ++year_;
        }
    }
}

void 
DateAndTime::setHour(const int hour)
{
    hour_ = hour;
}

void
DateAndTime::setMinute(const int minute)
{
    minute_ = minute;
}

void 
DateAndTime::setSecond(const int second)
{
    second_ = second;
}


int
DateAndTime::getHour() const
{
    return hour_;
}

int 
DateAndTime::getMinute() const
{
    return minute_;
}

int 
DateAndTime::getSecond() const
{
    return second_;
}

void 
DateAndTime::printDataAndTime() const
{
    const char oldFill = std::cout.fill('0');
    std::cout << ((getHour() == 0 || getHour() == 12) ? 12 : getHour() % 12)
              << ":" << std::setfill('0') << std::setw(2) << getMinute()
              << ":" << std::setw(2) << getSecond() << (hour_ < 12 ? " AM  " : " PM  ")
              << getMonth() << "/" << getDay() << "/" << getYear() << std::endl;
    std::cout.fill(oldFill);
}

void
DateAndTime::printDataAndTimeUniversal() const
{
    const char oldFill = std::cout.fill('0');
    std::cout << ((getHour() == 0 || getHour() == 12) ? 12 : getHour() % 12)
              << ":" << std::setfill('0') << std::setw(2) << getHour() << ":"
              << std::setw(2) << getMinute() << " " << std::setw(0) << getSecond()
              << getMonth() << "/" << getDay() << "/" << getYear() << std::endl;
    std::cout.fill(oldFill);
}


