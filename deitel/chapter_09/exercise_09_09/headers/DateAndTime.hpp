#ifndef __DATEANDTIME_HPP__
#define __DATEANDTIME_HPP__

class DateAndTime
{
public:
    DateAndTime();
    DateAndTime (const int second, const int minute, const int hour, const int mount, const int day, const int year);
    void setDay(const int day);
    void setMonth(const int mount);
    void setYear(const int year);
    void setHour(const int hour);
    void setMinute(const int minute);
    void setSecond(const int second);
    void printDataAndTime() const;
    void printDataAndTimeUniversal()const;
    int getDay() const;
    int getMonth() const;
    int getYear() const;
    int getHour() const;
    int getMinute() const;
    int getSecond() const;
    void secondIncrement();
    void nextDay();
private:
    int second_;
    int minute_;
    int hour_;
    int month_;
    int day_;
    int year_;
};

#endif /// __DATEANDTIME_HPP__
