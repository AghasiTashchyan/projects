#include "headers/HugeInteger.hpp"
#include <iostream>
#include <cassert>

HugeInteger::HugeInteger(char* integers)
{
    setIntegers(integers);
} 

void
HugeInteger::setIntegers(char* integers)
{
    for (int i = getSize(); i > 0 || integers[i] != '\0'; --i) {
        integers_[i] = integers[i];
    }
}

void 
HugeInteger::inputInteger(const int index, const char value)
{
    assert(index >= 0 &&  index < 40);
    assert(value >= 0 && value < 10);
    integers_[index] = value; 
}

char
HugeInteger::outInteger(const int index)
{
    return integers_[index];
}

HugeInteger
HugeInteger::plusing(const HugeInteger& rhv)
{
    const int size = (getSize() > rhv.getSize()) ? getSize() : rhv.getSize();
    char array[size];
    char carry = '0';
    for (int i = size; i > 0; --i) {
        int sum = integers_[i] + rhv.integers_[i] + carry;
        if (sum > 9) {
            sum %= 10;
            array[i] = '0' + sum;
            carry = '1';
        } else {
            carry = '0';
        }
    }
    return HugeInteger(array);
}
/*
HugeInteger
HugeInteger::sub(const HugeInteger& rhv)
{

}




*/


int
HugeInteger::getSize() const
{
    return SIZE;
}
void 
HugeInteger::printIntegers() const
{
    for (int i = 0; i < getSize(); ++i) {
        if (integers_[i] != '\0') {
            std::cout << integers_[i] << " ";
        }
    }
}
