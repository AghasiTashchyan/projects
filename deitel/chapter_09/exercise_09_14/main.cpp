#include "headers/HugeInteger.hpp"
#include <iostream>

int 
main()
{
    const int SIZE = 40;
    char array[SIZE] = "754124854";
    char array2[SIZE] = "752148520";
    HugeInteger myintegers(array);
    HugeInteger myintegers2(array2);
    HugeInteger myintegers3 = myintegers.plusing(myintegers2);
    myintegers.printIntegers();
    std::cout << std::endl; 
    myintegers2.printIntegers();
    std::cout << std::endl;
    myintegers3.printIntegers();
    std::cout << std::endl;
    return 0;

}
