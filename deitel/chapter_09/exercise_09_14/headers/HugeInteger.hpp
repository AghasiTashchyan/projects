
class HugeInteger
{
public:
    HugeInteger(char* integers);
    void setIntegers(char* integers);
    void inputInteger(const int index, const char value);
    char outInteger(const int index);
    HugeInteger plusing(const HugeInteger& rhv);
    ////HugeInteger sub(const HugeInteger& rhv);
    int getSize() const;
    void printIntegers() const;
private:
    static const int SIZE = 40;
    char integers_[SIZE];
};
