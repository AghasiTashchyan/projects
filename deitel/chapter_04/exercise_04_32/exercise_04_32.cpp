#include <iostream>
#include <unistd.h>

int 
main()
{
    while (true) {
        if (::isatty(STDIN_FILENO)) { 
            std::cout << "Input sides of a triangle (-1 to quit): ";
        }
        double side1;
        std::cin >> side1;
        if (-1 == side1) {
            return 0;
        }
        if (side1 < 0) {
            std::cout << "Error 1: Invalid value." << std::endl;
            return 1;
        }

        double side2;
        std::cin >> side2;
        if (-1 == side2) {
            return 0;
        }
        if (side2 < 0) {
            std::cout << "Error 2: Invalid value." << std::endl;
            return 2;
        }

        double side3;
        std::cin >> side3;
        if (-1 == side3) {
            return 0;
        }
        if (side3 < 0) {
            std::cout << "Error 3: Invalid value." << std::endl;
            return 3;
        }

        if (side1 + side2 < side3) {
            std::cout << "Triangle with such parameters does not exist. " << std::endl;
            return 0;
        }
        if (side3 + side2 < side1) {
            std::cout << "Triangle with such parameters does not exist. " << std::endl;
            return 0;
        }
        if (side1 + side3 < side2) {
            std::cout << "Triangle with such parameters does not exist. " << std::endl;
            return 0;
        }
        std::cout << "Triangel with such parameters can exist" << std::endl;
    }
    return 0;
}

