#include <iostream>
#include <unistd.h>

int 
main()
{
    while (true) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Input sales in dollars: " << std::endl;
        }
        double sale;
        std::cin >> sale;
        if (-1 == sale) {
            return 0;
        }
        double salary = 200;
        double commission = salary + (sale * 0.09);
        std::cout << "Salary is: " << commission << std::endl;
    }
    return 0;
}

