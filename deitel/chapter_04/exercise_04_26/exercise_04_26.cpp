#include <iostream>
#include <unistd.h>

int 
main()
{      
    if (::isatty(STDIN_FILENO)) { 
        std::cout << "Input number: ";
    }
    int number;
    std::cin >> number;
    if (number < 10000 || number > 99999) {
        std::cout << "Error 1: Number is not five-digit." << std::endl;
        return 1;
    }
    int counter = 10000;
    while (10 < number) {
        int lastIndex = number % 10;
        int firstIndex = number / counter ;
        if (lastIndex != firstIndex) {
            std::cout << "Number is not palindrome." << std::endl;
            return 0;
        }
        number %= counter;
        number /= 10;
        counter /= 100;
    }
    std::cout << "Number is palindrome." << std::endl;
    return 0;
}

