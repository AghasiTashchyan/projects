#include "Bank.hpp"
#include <unistd.h>
#include <iostream>
#include <cstdlib>

int 
main()
{
    while (true) {
        if (::isatty(STDIN_FILENO)) {                            
            std::cout << "input accaunt number (-1 to quit): " << std::endl;
        }    
        int accauntNumber;
        std::cin >> accauntNumber;
        if (-1 == accauntNumber) {
            std::cout << "Program is Closed" << std::endl;
            return 0;
        }       
        if (::isatty(STDIN_FILENO)) {                            
            std::cout << "input balance: " << std::endl;
        }
        double balance;
        std::cin >> balance;
        if (balance < 0) {
            std::cout << "Error1: Balance can not by less than 0. Balance becomes 0." << std::endl;
            balance = 0;
            return 1;
        }
        if (::isatty(STDIN_FILENO)) {                            
            std::cout << "Input chargers: " << std::endl;
        }

        double charges;
        std::cin >> charges;
        if (charges < 0) {
            std::cout << "Error 2: Charges can not by less than 0." << std::endl;
            return 2;
        }
        if (::isatty(STDIN_FILENO)) {                           
            std::cout << "Input credits: " << std::endl;
        }

        double credits;
        std::cin >> credits;
        if (credits < 0) {
            std::cout << "Error 3: Credit can not by less than 0." << std::endl;
            return 3;
        }
        if (::isatty(STDIN_FILENO)) {                           
            std::cout << "Input credit limit: " << std::endl;
        }
        double creditLimit;
        std::cin >> creditLimit;       

        Bank myBank(accauntNumber, balance, charges, credits, creditLimit);
        myBank.checkAndPrint();
    }    
    return 0;
}

