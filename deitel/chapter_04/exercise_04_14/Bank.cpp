#include "Bank.hpp"
#include <iostream>
#include <cstdlib>

Bank::Bank(int number, double balance, double charges, double credits, double creditLimit)
{
    setAccauntNumber(number);
    setBalance(balance);
    setCharges(charges);
    setCredits(credits);
    setLimitcredit(creditLimit);
    findOutNewBalance();
}

void
Bank::setAccauntNumber(int number)
{
    accauntNumber_= number;
}

void
Bank::setBalance(double balance)
{  
    if (balance < 0) {
        std::cout << "Warning 1: Balance can not be less than 0. balance becomes 0." << std::endl;
balance_ = 0;
    }
    balance_ = balance;
}

void
Bank::findOutNewBalance()
{   
    newBalance_ = balance_ + totalCharges_ - totalCredit_;
}
void
Bank::setCharges(double charges)
{       
    if (charges < 0) {
        std::cout << "Error 1: Charges can not by less than 0." << std::endl;
        ::exit(1);      
    }
    totalCharges_ = charges;
}

void
Bank::setCredits(double credits)
{
    if (credits < 0) {
        std::cout << "Error 2: Credit can not by less than 0." << std::endl;
        ::exit(2);      
    }
    totalCredit_ = credits;
}
void
Bank::setLimitcredit(double creditLimit)
{
    if (creditLimit < 0) {
        std::cout << "Error 3: Limit can not by less than 0." << std::endl;
    }
    creditLimit_ = creditLimit;
}

void
Bank::checkAndPrint() 
{
    if (newBalance_ > creditLimit_) {
        std::cout << "Accaunt number: " << getAccauntNumber() << std::endl;
        std::cout << "New balance is: " << getNewBalance() << std::endl;
        std::cout << "Credit Limit Exeeded." << std::endl;
        return;
    } 
    std::cout << "New Balance: " << getNewBalance() << std::endl;
}

int
Bank::getAccauntNumber() 
{
    return accauntNumber_;
}

double
Bank::getBalance()
{
      return balance_;
}

double
Bank::getCreditLimit() 
{
    return creditLimit_;
}
double
Bank::getNewBalance() 
{
    return newBalance_;
}

