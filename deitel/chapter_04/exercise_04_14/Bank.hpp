#include <iostream>
#include <cstdlib>

class Bank
{
public:
    Bank(int number, double balance, double charges, double credits, double creditLimit);
    void setAccauntNumber(int number);
    void setBalance(double balance);
    void findOutNewBalance();
    void setCharges(double charges);
    void setCredits(double credits);
    void setLimitcredit(double creditLimit);
    void checkAndPrint();
    int getAccauntNumber();
    double getBalance();
    double getCreditLimit();
    double getNewBalance();
private:
    int accauntNumber_;
    double balance_;
    double totalCharges_;
    double totalCredit_;
    double creditLimit_;
    double newBalance_;
};

