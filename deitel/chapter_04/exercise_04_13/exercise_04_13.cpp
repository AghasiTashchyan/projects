#include "Mpg.hpp"
#include <iostream>
#include <cstdlib>
#include <unistd.h>

int 
main()
{
    Mpg myMpg;
    while (true) {
        if (::isatty(STDIN_FILENO)) { 
           std::cout << "Input mile (-1 to quit): ";
        }
        int mile;  
        std::cin >> mile;
        if (-1 == mile) {
            return 0;
        }
        if (mile < 0) {
            std::cout << "Error 1: Invalid mile value." << std::endl;
            return 1;
        }
        myMpg.setMile(mile);
        if (::isatty(STDIN_FILENO)) { 
           std::cout << "Input galon: "; 
        }
        int gallon;
        std::cin >> gallon;
        if (gallon <= 0) {
            std::cout << "Error 2: Invalid gallon value." << std::endl;
            return 2;
        }
        myMpg.setGallon(gallon);
        myMpg.printTotalMileGallon();   
    }
    return 0;

}

