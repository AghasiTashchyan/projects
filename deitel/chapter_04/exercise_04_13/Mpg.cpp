#include "Mpg.hpp"
#include <iostream>
#include <cstdlib>
#include <cassert>

Mpg::Mpg() 
{
    totalMiles_ = 0;
    totalGallons_ = 0;
}

int
Mpg::getGallon() 
{
    return gallon_;
}

int
Mpg::getMile()
{
    return mile_;
}

double
Mpg::getTankful()
{
    return static_cast<double>(mile_) / gallon_;
}

double 
Mpg::getMpg()
{
    assert(0 != totalGallons_);
    return static_cast<double>(totalMiles_) / totalGallons_;
}

void
Mpg::printTotalMileGallon()
{
    std::cout << "Tankful is " << getTankful() <<  std::endl;
    std::cout << "Total is " << getMpg() <<  std::endl;
}

void 
Mpg::setGallon(int gallon)
{
    assert(gallon > 0);
    gallon_ = gallon;
    totalGallons_ += gallon_;
}

void
Mpg::setMile(int mile)
{
    assert( mile > 0);
    mile_ = mile;
    totalMiles_ += mile_;
}

