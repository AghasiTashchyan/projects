class Mpg 
{  
public:
    Mpg(); 
    int getGallon();
    int getMile();
    double getTankful();
    double getMpg();
    void printTotalMileGallon();
    void setGallon(int gallon);
    void setMile(int mile);
private:
    int gallon_;
    int mile_;
    double totalMiles_;
    double totalGallons_;
};

