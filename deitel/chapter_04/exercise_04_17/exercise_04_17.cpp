#include <iostream>
#include <climits>
#include <unistd.h>

int
main()
{   
    int counter = 0;
    int largest = INT_MIN;
    while (counter < 10) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Input number: ";
        }
        int number;
        std::cin >> number ;
        if (number > largest) {
            largest = number;
        } 
        ++counter;
    }
    std::cout << "Largest is " << largest << std::endl;
    return 0;
}

