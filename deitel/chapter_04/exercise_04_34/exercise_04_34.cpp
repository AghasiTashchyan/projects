#include <iostream>
#include <unistd.h>

int main() {
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input encrypted four-digit integer for decryption: ";
    }
    int encryptedValue;
    std::cin >> encryptedValue;
    
    if (encryptedValue < 0 || encryptedValue > 9999) {
        std::cerr << "Error: Please input a four-digit integer." << std::endl;
        return 1;
    }

    int divisor = 1000;
    int originalNumber = 0;
    int position = 0;
    while (divisor > 0) {
        int digit = (encryptedValue / divisor) % 10;
        int decryptedDigit = (digit + 3) % 10;
        0 == position ? originalNumber += decryptedDigit * 10   :
        3 == position ? originalNumber += decryptedDigit * 100  :     
        2 == position ? originalNumber += decryptedDigit * 1000 :
                        originalNumber += decryptedDigit;
        encryptedValue %= divisor;
        divisor /= 10;
        ++position;
    }

    std::cout << "Decrypted value: " << originalNumber << std::endl;
    return 0;
}
