#include <iostream>
#include <cstdlib>
#include <unistd.h>

int
main() 
{
    while (true) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Input hours worked: " << std::endl;
        } 
        double hourlyWage;
        std::cin >> hourlyWage;
        if (hourlyWage <= -1) {
             break;
        }
        if (::isatty(STDIN_FILENO)) {  
            std::cout << "Input hourly rate of the worker: " << std::endl;
        }
        double rate;
        std::cin >> rate;
        double salary = hourlyWage * rate;
        if (hourlyWage > 40) {     
            salary += (hourlyWage - 40) * rate / 2;
        }
        std::cout << "Salary is " << salary << std::endl;

    }
    return 0;
}

