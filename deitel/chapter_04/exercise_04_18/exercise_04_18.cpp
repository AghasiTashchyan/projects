#include <iostream>
#include <unistd.h>

int
main()
{   
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input number: ";
    }
    int number;
    std::cin >> number;
    if (0 == number) {
        std::cout << "Error 1: invalid value." << std::endl;
        return 1;
    }
    std::cout << "N\t" << "10 * N\t" << "100 * N\t"<< "1000 * N" << std::endl;
    int i = 0; 
    while (i < 5) {   
        std::cout << number << "\t" << number * 10 << "\t" << number * 100 << "\t" << number * 1000 << std::endl;
        ++number;
        ++i;
    }
    return 0;
}

