#include <iostream>
#include <unistd.h>

int 
main()
{
    double pi = 3.14159;
    while (true) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Input radius (-1 to quit): ";
        }
        double radius;
        std::cin >> radius;
        if (-1 == radius) {
            return 0;
        }
        double diameter = 2 * radius;
        double circumference = 2 * pi * radius;
        double area = pi * diameter;
        std::cout << "Diameter is " << diameter << std::endl;
        std::cout << "Circumference is " << circumference << std::endl;
        std::cout << "Area is " << area << std::endl;
    }
    return 0;
}

