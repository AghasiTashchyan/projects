#include <iostream>
#include <unistd.h>

int 
main ()
{
    if (::isatty(STDIN_FILENO)) { 
        std::cout << "Input sides of a triangle: ";
    }
    while (true) {
        int a;
        std::cin >> a;
        if (0 >= a) {
            std::cout << "Error 1: Invalid value" << std::endl;
            return 1;
        }
        int b;
        std::cin >> b;
        if (0 >= b) {
            std::cout << "Error 2: Invalid value" << std::endl;
            return 2;
        }
        int c;
        std::cin >> c;
        if (0 >= c) {
            std::cout << "Error 3: Invalid value" << std::endl;
            return 3;
        }
        int squareA = a * a;
        int squareB = b * b;
        int squareC = c * c;
        if (squareA + squareB == squareC) {
            std::cout << "With such parameters a triangle can exist. " << std::endl;
            return 0;
        }
        if (squareC + squareA == squareB) {
            std::cout << "With such parameters a triangle can exist. " << std::endl;
            return 0;
        }
        if (squareB + squareC == squareA) {
            std::cout << "With such parameters a triangle can exist. " << std::endl;
            return 0;
        }
        std::cout << "With such parameters a triangel can not exist." << std::endl;
    }
    return 0;
}

