#include <iostream>
#include <unistd.h>

int
main()
{       
    if (::isatty(STDIN_FILENO)) {
       std::cout << "Input size: ";
    }
    int boxSize;
    std::cin >> boxSize;
    if (boxSize <= 0) {
        std::cout << "Error 1: Invalid value." << std::endl;
        return 1;
    }
    int counter1 = 0;
    while (counter1 < boxSize) {
        int counter2 = 0;
        while (counter2 < boxSize) {
            std::cout << (0 == counter1 || boxSize - 1 == counter1 || 0 == counter2 || boxSize - 1 == counter2 ? "*" : " ");
            ++counter2;
        }
        ++counter1;
        std::cout << std::endl;
    }
    return 0;
}

