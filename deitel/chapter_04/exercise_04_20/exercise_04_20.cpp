#include <iostream>
#include <unistd.h>

class Analysis
{

public:

    void
    processExamResults()
    { 
        int passes = 0;        
        int failures = 0;    
        int studentCounter = 1;
        while (true) {  
            if (::isatty(STDIN_FILENO)) {
                std::cout << "Input result (1 = pass, 2 = fail, -1 == exit): ";
            }
            int result;
            std::cin >> result; 
            if (-1 == result) {
                break;
            }
            if (1 == result) {     
                ++passes;
                ++studentCounter;
            } else if (2 == result) {                   
                ++failures;
                ++studentCounter;
            } else {
               std::cout << "Warning 1: Incorrect value." << std::endl;
            } 
        }
        std::cout << "Passed " << passes << "\nFailed " << failures << std::endl;
        if (passes > 8) {
            std::cout << "Raise tuition " << std::endl;
        }
    } 
};

int 
main()
{
    Analysis aplication;
    aplication.processExamResults();
    return 0; 
}

