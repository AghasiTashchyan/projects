#include <iostream>
#include <unistd.h>

int
main()
{      
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input binary number: ";
    }
    int number;
    std::cin >> number;
    if (number < 0) {
        std::cout << "Error 1: Invalid value." << std::endl;
        return 1;
    }  
    int decimal = 0;
    int powerOf = 2;
    while (number != 0) {  
        int lastNumber = number % 10;
        if (1 < lastNumber) {
            std::cout << "Error 2: Number is not binary." << std::endl;
            return 2;
        }
        decimal += lastNumber * powerOf;
        number = number / 10;
        powerOf *= 2; 
    }
    std::cout << "Decimal is " << decimal << std::endl;
    return 0;
}

