#include <iostream>
#include <climits>
#include <unistd.h>

int
main()
{       
    int firstValue = INT_MIN, nextValue = INT_MIN, counter = 0;
    while (counter < 10) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Input numbers: ";
        }
        int numbers;
        std::cin >> numbers;
        if (numbers > firstValue) {
            nextValue = firstValue;
            firstValue = numbers;
        } else if (numbers > nextValue) {
            nextValue = numbers;
        }
        ++counter;    
    }
    std::cout << "Largest value is " << firstValue << std::endl;
    std::cout << "Next value is " << nextValue << std::endl;
    return 0;
}

