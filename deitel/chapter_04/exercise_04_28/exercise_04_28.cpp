#include <iostream>
#include <unistd.h>

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input board size: ";
    }
    int checkerBox;
    std::cin >> checkerBox;
    if (checkerBox < 1) {
        std::cout << "Error 1: Invalid board size." << std::endl;
        return 1;
    }
    int countY = 0;
    while (countY < checkerBox) {
        int countX = 0;
        if (countY % 2 == 0) {
            std::cout << " ";
        }
        while (countX < checkerBox) {
            std::cout << "* ";
            ++countX;
        }
        std::cout << std::endl;
        ++countY;
    }
    return 0;                     
}

