#ifndef __HUGEINT_HPP__
#define __HUGEINT_HPP__

#include <iostream>

class HugeInt
{
public:
    void operator<<(std::ostream& out);
    HugeInt(const long value = 0);
    HugeInt(const char* string);
    HugeInt operator+(const HugeInt& rhv) const;
    HugeInt operator+(const long vlaue) const;
    HugeInt operator+(const char* string) const;
    HugeInt operator*(const HugeInt& rhv) const;
    HugeInt operator*(const long vlaue) const;
    HugeInt operator*(const char* string) const;

private:
    char integer[30];
};

#endif /// __HUGEINT_HPP__

