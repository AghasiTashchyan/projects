#include "headers/Hugeint.hpp"
#include <gtest/gtest.h>

TEST(HugeintTest, SumingObjeqtHugeint)
{
    HugeInt hugeint1("56784");
    HugeInt hugeint2("25489");
    HugeInt result = hugeint1 + hugeint2;
    std::stringstream out;
    result << out;
    EXPECT_EQ(out.str(), "82273");
}

TEST(HugeintTest, MultipolObjeqtHugeint)
{  
    HugeInt hugeint1("123");
    HugeInt hugeint2("456");
    HugeInt result = hugeint1 * hugeint2;
    std::stringstream out;
    result << out;
    EXPECT_EQ(out.str(), "56088");
}

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();

}
