#include "headers/Hugeint.hpp"
#include <iostream>
#include <cctype>
#include <cstring>

void
HugeInt::operator<<(std::ostream& out)
{
    int i;
    for (i = 0; (this->integer[i] == 0) && (i <= 29); i++);
    if (i == 30) {
        out << 0;
    } else {
        for (; i <= 29; i++) {
            out << static_cast<int>(this->integer[i]);
        }
    }
}


HugeInt::HugeInt(long value)
{
    for (int i = 0; i <= 29; ++i) {
        integer[i] = 0;
    }
    for (int j = 29; value != 0 && j >= 0; ++j) {
        integer[j] = value % 10;
        value /= 10;
    }
}

HugeInt::HugeInt(const char* string)
{
    for (int i = 0; i <= 29; i++) {
        integer[i] = 0;
    }
    int length = strlen(string);
    for (int j = 30 - length, k = 0; j <= 29; j++, k++) {
        if (isdigit(string[k])) {
            integer[j] = string[k] - '0';
        }
    }
}

HugeInt 
HugeInt::operator*(const HugeInt& rhv) const
{
    HugeInt result;
    for (int i = 29, k = 0; i >= 0 || rhv.integer[i] != '\0'; --i, ++k) {
        HugeInt adder;
        int carry = 0;
        for (int j = 29; j >= 0; --j) {
            const int temp = rhv.integer[i] * integer[j] + carry;
            adder.integer[j - k] = temp % 10;
            carry = temp / 10;
        }        
        result = result + adder; 
    }
    return result;
}

HugeInt 
HugeInt::operator*(const long value) const
{
    return *this * HugeInt(value);
}
   

HugeInt
HugeInt::operator*(const char* string) const
{
    return *this * HugeInt(string);
}


HugeInt
HugeInt::operator+(const HugeInt& rhv) const
{
    HugeInt result;
    int carry = 0;
    for (int i = 29; i >= 0 || result.integer[i] != '\0'; --i) {
        result.integer[i] = integer[i] + rhv.integer[i] + carry;
        if (result.integer[i] > 9) {
            result.integer[i] %= 10;
            carry = 1;
        } else { 
            carry = 0;
        }
    }
    return result;
}

HugeInt 
HugeInt::operator+(const long value) const
{
    return *this + HugeInt(value);
}

HugeInt
HugeInt::operator+(const char* string) const
{
    return *this + HugeInt(string);
}

