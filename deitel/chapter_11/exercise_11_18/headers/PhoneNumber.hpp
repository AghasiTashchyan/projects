#ifndef __PHONENUMBER_HPP__
#define __PHONENUMBER_HPP__

#include <iostream>
#include <string>

class PhoneNumber
{
public:
    void operator<<(std::ostream& out);
    void operator>>(std::istream& in);      
private:
    std::string areaCode_;
    std::string exchange_;
    std::string line_;
};

#endif /// __PHONENUMBER_HPP__
