#include "headers/PhoneNumber.hpp"
#include <gtest/gtest.h>
#include <iostream>

TEST(PhoneNumber, InputOutputOperators)
{
    PhoneNumber phone;
    std::stringstream in("+37477181697");
    phone >> in;
    std::stringstream out;
    phone << out;
    EXPECT_EQ(out.str(), "(+374) 77-181697");

}

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();

}
