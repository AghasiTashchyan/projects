#include "headers/PhoneNumber.hpp"
#include <iomanip>
                            
void
PhoneNumber::operator<<(std::ostream& out)
{                                                                
    out << "(" << areaCode_ << ") "                      
       << exchange_ << "-" << line_;                                
}                                    

void
PhoneNumber::operator>>(std::istream& in)  
{                                                                                   
    in >> std::setw(4) >> areaCode_;
    in >> std::setw(2) >> exchange_;   
    in >> std::setw(6) >> line_;                      
}

