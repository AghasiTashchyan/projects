#include "headers/Array.hpp"
#include <iostream>

using std::cerr;
using std::cout;
using std::cin;
using std::endl;

#include <iomanip>
using std::setw;

#include <cstdlib>
using std::exit;


Array::Array(int arraySize)
{
    size_ = (arraySize > 0 ? arraySize : 10);
    ptr_ = new int[size_];

    for (int i = 0; i < size_; i++)
        ptr_[i] = 0;
}

Array::Array(const Array &arrayToCopy)
    : size_(arrayToCopy.size_)
{
    ptr_ = new int[size_];

    for (int i = 0; i < size_; i++)
        ptr_[i] = arrayToCopy.ptr_[i];
}

Array::~Array()
{
    delete[] ptr_;
}

int Array::getSize() const
{
    return size_;
}

const Array &Array::operator=(const Array &right)
{
    if (&right != this)
    {
        if (size_ != right.size_)
        {
            delete[] ptr_;
            size_ = right.size_;
            ptr_ = new int[size_];
        }

        for (int i = 0; i < size_; i++)
            ptr_[i] = right.ptr_[i];
    }

    return *this;
}

bool Array::operator==(const Array &right) const
{
    if (size_ != right.size_)
        return false;

    for (int i = 0; i < size_; i++)
        if (ptr_[i] != right.ptr_[i])
            return false;

    return true;
}

int &Array::operator[](int subscript)
{
    if (subscript < 0 || subscript >= size_)
    {
        cerr << "\nError: Subscript " << subscript
             << " out of range" << endl;
        exit(1);
    }

    return ptr_[subscript];
}

int Array::operator[](int subscript) const
{
    if (subscript < 0 || subscript >= size_)
    {
        cerr << "\nError: Subscript " << subscript
             << " out of range" << endl;
        exit(1);
    }

    return ptr_[subscript];
}

istream &operator>>(istream &input, Array &a)
{
    for (int i = 0; i < a.size_; i++)
        input >> a.ptr_[i];

    return input;
}

ostream &operator<<(ostream &output, const Array &a)
{
    int i;

    for (i = 0; i < a.size_; i++)
    {
        output << setw(12) << a.ptr_[i];

        if ((i + 1) % 4 == 0)
            output << endl;
    }

    if (i % 4 != 0)
        output << endl;

    return output;
}

