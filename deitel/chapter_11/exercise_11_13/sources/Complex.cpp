#include "headers/Complex.hpp"
#include <iostream>

std::istream&
operator>>(std::istream& input, Complex& rhv) 
{
    input >> rhv.real_ >> rhv.imaginary_;
    return input;
}

std::ostream&
operator<<(std::ostream& out, const Complex& rhv) 
{
    out << "(" << rhv.real_ << ", " << rhv.imaginary_ << ")";
    return out;
}

bool
operator==(const Complex& lhv, const Complex& rhv)
{
    return (lhv.real_ == rhv.real_ && lhv.imaginary_ == rhv.imaginary_);
}

bool
operator!=(const Complex& lhv, const Complex& rhv)
{
    return (lhv.real_ != rhv.real_ && lhv.imaginary_ != rhv.imaginary_);
}

Complex::Complex(const double realPart, const double imaginaryPart)
    : real_(realPart)
    ,imaginary_(imaginaryPart)
{

}

Complex 
Complex::operator+(const Complex& rhv) const
{
    return Complex(real_ + rhv.real_, imaginary_ + rhv.imaginary_);
}

Complex
Complex::operator-(const Complex& rhv) const
{
    return Complex(real_ - rhv.real_, imaginary_ - rhv.imaginary_);
}

Complex
Complex::operator*(const Complex& rhv) const
{
    return Complex((real_ * rhv.real_) - (imaginary_ * rhv.imaginary_), (real_ * rhv.imaginary_) + (imaginary_ * rhv.real_));
}

