#include <headers/Complex.hpp>
#include <gtest/gtest.h>

TEST(ComplexTest, StreamInsertionExtraction)
{
    Complex complex1;
    std::stringstream in("4.3 8.2");
    in >> complex1;
    std::stringstream out;
    out << complex1;
    EXPECT_EQ(out.str(), "(4.3, 8.2)");
}

TEST(ComplexTest, OperatorMultipl)
{
    Complex complex1, complex2;
    std::stringstream in("4 2 3 2");
    in >> complex1 >> complex2;
    const Complex complex3 = complex1 * complex2;
    std::stringstream out;
    out << complex3;
    EXPECT_EQ(out.str(), "(8, 14)");
}

TEST(ComplexTest, OperatorЕquality)
{
    Complex complex1, complex2;
    std::stringstream in("4 4 4 4");
    in >> complex1 >> complex2;
    bool result = (complex1 == complex2);
    EXPECT_TRUE(result);
}

TEST(ComplexTest, OperatorNotЕquality)
{
    Complex complex1, complex2;
    std::stringstream in("1 1 4 4");
    in >> complex1 >> complex2;
    bool result = (complex1 == complex2);
    EXPECT_FALSE(result);
}

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

