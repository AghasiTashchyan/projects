#ifndef __COMPLEX_HPP__
#define __COMPLEX_HPP__

#include <iostream>
#include <string>

class Complex
{
private:
    friend std::istream& operator>>(std::istream& input, Complex& rhv);
    friend std::ostream& operator<<(std::ostream& input, const Complex& rhv);
    friend bool operator==(const Complex& lhv, const Complex& rhv);
    friend bool operator!=(const Complex& lhv, const Complex& rhv);
public:
    Complex(const double real = 0.0, const double imaginary = 0.0);
    Complex operator+(const Complex& rhv) const;
    Complex operator-(const Complex& rhv) const;
    Complex operator*(const Complex& rhv) const;
private:
    double real_;
    double imaginary_; 
};

#endif /// __COMPLEX_HPP__
