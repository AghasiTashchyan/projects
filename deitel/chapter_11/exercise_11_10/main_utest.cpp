#include "headers/Array.hpp"
#include <gtest/gtest.h>

TEST(ArrayTest, ReturningIndex)
{
    Array array1;
    const int result = array1(1, 6);
    EXPECT_EQ(result, 39);
}



int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();

}
