#include <iostream>

class Array
{
public:
    int operator()(int index, int index2);
    Array();
    Array(const int size);
    void setBoard(const int size);
    int getSize() const;

private:
    static const int SIZE = 8;
    int board_ [SIZE][SIZE];
};

