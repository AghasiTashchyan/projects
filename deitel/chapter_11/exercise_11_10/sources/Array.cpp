#include "headers/Array.hpp"
#include <iostream>
#include <cstdlib>

int
Array::operator()(int index, int index2)
{
    return board_[index][index2];
}

Array::Array()
{
    for (int row = 0; row < getSize(); row++) {
        for (int column = 0; column < getSize(); column++) {
            board_[row][column] = 1 + ::rand() % 65;
        }
    }
}

Array::Array(const int size)
{
    setBoard(size);
}

void
Array::setBoard(const int size)
{
    for (int i = 0; i < size; ++i) {
        for (int y = 0; y < size; ++y) {
            board_[i][y] = i;  
        }
    }
}

int
Array::getSize() const
{
    return SIZE;
}
