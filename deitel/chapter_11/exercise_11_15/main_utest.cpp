#include "headers/Rational.hpp"
#include <gtest/gtest.h>
#include <iostream>

TEST(RationalTest, AddingRationalNumbers)
{
    Rational rational1, rational2;
    std::stringstream in("2 3 1 2");
    in >> rational1 >> rational2;
    Rational result = rational1 + rational2;
    std::stringstream out;
    out << result;
    EXPECT_EQ(out.str(), "7 6");
}

TEST(RationalTest, SubstractingRationalNumbers)
{
    Rational rational1, rational2;
    std::stringstream in("2 3 1 2");
    in >> rational1 >> rational2;
    Rational result = rational1 - rational2;
    std::stringstream out;
    out << result;
    EXPECT_EQ(out.str(), "1 6");
}

TEST(RationalTest, MultiplRationalNumbers)
{
    Rational rational1, rational2;
    std::stringstream in("2 3 1 2");
    in >> rational1 >> rational2;
    Rational result = rational1 * rational2;
    std::stringstream out;
    out << result;
    EXPECT_EQ(out.str(), "1 3");
}

TEST(RationalTest, DivisionRationalNumbers)
{
    Rational rational1, rational2;
    std::stringstream in("2 3 1 2");
    in >> rational1 >> rational2;
    Rational result = rational1 / rational2;
    std::stringstream out;
    out << result;
    EXPECT_EQ(out.str(), "4 3");
}

TEST(RationalTest, EqualityRationalNumbers)
{
    Rational rational1, rational2;
    std::stringstream in("1 2 1 2");
    in >> rational1 >> rational2;
    const bool result = (rational1 == rational2);
    EXPECT_TRUE(result);
}

TEST(RationalTest, NotEqualityRationalNumbers)
{
    Rational rational1, rational2;
    std::stringstream in("1 2 2 2");
    in >> rational1 >> rational2;
    const bool result = (rational1 != rational2);
    EXPECT_TRUE(result);
}

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();

}
