#include "headers/Rational.hpp"
#include <string>
#include <unistd.h>
#include <cassert>
#include <cmath>
#include <iostream>

std::istream&
operator>>(std::istream& input, Rational& rhv) 
{
    input >> rhv.numerator_ >> rhv.denominator_;
    return input;
}

std::ostream&
operator<<(std::ostream& out, const Rational& rhv)
{
    out << rhv.numerator_ << " " << rhv.denominator_;
    return out;
}

Rational::Rational()
{
    numerator_ = 0;
    denominator_ = 1;
}

Rational::Rational(const int numerator, const int denominator)
{
    setNumerator(numerator);
    setDenominator(denominator);
    gcd();
}

Rational
Rational::operator+(const Rational& rhv) const
{
    const int numerator = (numerator_ * rhv.getDenominator()) + (denominator_ * rhv.getNumerator());
    const int denominator = denominator_ * rhv.getDenominator();
    return Rational(numerator, denominator);
}

Rational
Rational::operator-(const Rational& rhv) const
{
    const int numerator = (numerator_ * rhv.getDenominator()) - (denominator_ * rhv.getNumerator());
    const int denominator = denominator_ * rhv.getDenominator();
    return Rational(numerator, denominator);
}

Rational
Rational::operator/(const Rational& rhv) const
{
    const int numerator = numerator_ * rhv.getDenominator();
    const int denominator = denominator_ * rhv.getNumerator();
    return Rational(numerator, denominator);
}

Rational 
Rational::operator*(const Rational& rhv) const
{
    const int numerator = numerator_ * rhv.getNumerator();
    const int denominator = denominator_ * rhv.getDenominator();
    return Rational(numerator, denominator);
}

bool
operator==(const Rational& lhv, const Rational& rhv)
{
    return (lhv.numerator_ == rhv.numerator_ && lhv.denominator_ == rhv.denominator_);
}

bool
operator!=(const Rational& lhv, const Rational& rhv)
{
    return (lhv.numerator_ != rhv.numerator_ || lhv.denominator_ != rhv.denominator_);
}

void
Rational::printFloatingFormat() const
{
    const double result = static_cast<double>(getNumerator()) / getDenominator();
    std::cout << "Floating Point printing: " << result << std::endl;
}

void
Rational::gcd()
{
    int numerator = numerator_;
    int denominator = denominator_;
    if (0 == denominator) {
        return;
    }
    while (numerator % denominator != 0) {
        int temp = denominator;
        denominator = numerator % denominator;
        numerator = temp;
    }
    if (denominator != 0) {
        numerator_ =  numerator_ / denominator;
        denominator_ = denominator_ / denominator;
    }
}

void
Rational::setNumerator(const int numerator)
{
    numerator_ = numerator;
}

void
Rational::setDenominator(const int denominator)
{
    if (denominator < 0) {
        denominator_ = std::abs(denominator);
        return;
    }
    denominator_ = (0 == denominator) ? 1 : denominator;
}

void
Rational::printRationalNumber() const
{
    std::cout << getNumerator() << " / " << getDenominator() << std::endl;
}

int
Rational::getNumerator() const
{
    return numerator_;
}

int 
Rational::getDenominator() const
{
   return denominator_;
}

int 
Rational::getIntegerValue(const std::string& prompt)
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << prompt;
    }
    int result;
    std::cin >> result;
    if (result < 0) {
        result = std::abs(result);
    }
    return result;
}

