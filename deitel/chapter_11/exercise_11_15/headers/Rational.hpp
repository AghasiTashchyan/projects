#ifndef __RATIONAL_HPP__
#define __RATIONAL_HPP__

#include <string>

class Rational
{
private:
    friend std::istream& operator>>(std::istream& input, Rational& rhv);
    friend std::ostream& operator<<(std::ostream& input, const Rational& rhv);
    friend bool operator==(const Rational& lhv, const Rational& rhv);
    friend bool operator!=(const Rational& lhv, const Rational& rhv); 
public:
    Rational();
    Rational(const int numerator, const int denominator);
    Rational operator+(const Rational& rhv) const;
    Rational operator-(const Rational& rhv) const;
    Rational operator/(const Rational& rhv) const; 
    Rational operator*(const Rational& rhv) const;
    void printFloatingFormat() const;
    void gcd();
    void setNumerator(const int numerator);
    void setDenominator(const int denuminator);
    void printRationalNumber() const;
    int getNumerator() const;
    int getDenominator() const;
    int getIntegerValue(const std::string& prompt);
private:
    int numerator_;
    int denominator_;
};

#endif /// __RATIONAL_HPP__
