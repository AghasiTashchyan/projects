#include "headers/String.hpp"
#include <cstring>

std::istream&
operator>>(std::istream& input, String& rhv)
{
    for (int i = 0; i < rhv.getLenght(); ++i) {
        input >> rhv.string_[i];
    }
    return input;
}

std::ostream&
operator<<(std::ostream& out, const String& rhv)
{
    for (int i = 0; i < rhv.lenght_; ++i) {
        out << rhv.string_[i];
    }
    return out;
}

String 
String::operator=(const String& rhv)
{
     if (&rhv != this) {
        delete[] string_;
        lenght_ = rhv.lenght_;
        setString(rhv.string_);
        return *this;
     }
     return *this;
}

String
String::operator+(const String& rhv)
{
    const int size = lenght_ + rhv.lenght_;
    char* concatenation = new char[size + 1];
    ::strcpy(concatenation, string_);
    ::strcpy(concatenation + lenght_, rhv.string_);
    const String result(concatenation);
    delete[] concatenation;
    return result;
}

String::String(const char* string) 
    : lenght_ (0)
    , string_ (NULL)
{
    setString(string);
}

String::String(const String& rhv)
    : lenght_ (rhv.lenght_)
    , string_ (rhv.string_)
{
    setString(rhv.string_);
}

String::~String()
{
    delete [] string_;
    string_ = NULL;
}

void
String::setString(const char* string)
{
    lenght_ = (string != NULL) ? ::strlen(string) : 0;
    string_ = new char[lenght_ + 1];
    if (string != NULL) {
        ::strcpy(string_, string);
    } else {
        string_[0] = '\0';
    }
}

int
String::getLenght() const
{
    return lenght_;
}

