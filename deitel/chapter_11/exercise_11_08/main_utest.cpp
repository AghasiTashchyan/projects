#include <headers/String.hpp>
#include <gtest/gtest.h>
#include <iostream>

TEST(StringTest, Concatination)
{
    String string1("Momento");
    String string2(" Mori");
    String string3 = string1 + string2;
    std::stringstream out;
    out << string3;
    EXPECT_EQ(out.str(), "Momento Mori");
}

int
main(int argc, char** argv) 
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
