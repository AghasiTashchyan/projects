#include <cstddef>
#include <iostream>

class String
{
private:
    friend std::istream& operator>>(std::istream& input, String& rhv);
    friend std::ostream& operator<<(std::ostream& out, const String& rhv);  
public:
    String operator=(const String& rhv);
    String operator+(const String& rhv);
    String(const char* string);
    String(const String& rhv);
    ~String();
    void setString(const char* string);
    int getLenght() const;
private:
    int lenght_;
    char* string_;
};

