#include "headers/Account.hpp"
#include "headers/SavingAccount.hpp"
#include <cassert>

SavingAccount::SavingAccount(const double balance, const double rate)
    : Account(balance)
    , rate_(rate)
{
}

void
SavingAccount::setRate(const double rate)
{
    assert(rate > 0);
    rate_ = rate;
}

double 
SavingAccount::calculateInterest()
{
    Account::balance_ += (Account::balance_ * rate_ )/ 100;
    return  Account::balance_;
}

double
SavingAccount::getBalance() const
{
    return Account::getBalance();
}

double
SavingAccount::getRate() const
{
    return rate_;
}
