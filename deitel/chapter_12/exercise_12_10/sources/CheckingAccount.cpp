#include "headers/Account.hpp"
#include "headers/CheckingAccount.hpp"

CheckingAccount::CheckingAccount(const double balance ,const double feeCharged)
    : Account(balance)
    , feeCharged_(feeCharged)
{
}

bool
CheckingAccount::tranzactionSuccessfully(const double debit)
{
    const double commission = debit * feeCharged_;
    return getBalance() - commission >= 0; 
}

void
CheckingAccount::setDebit(const double debit)
{
    const double commission =  debit + (debit * feeCharged_);
    Account::setDebit(commission);

}
