#include "headers/Account.hpp"
#include <cassert>


Account::Account()
    : balance_(0)
{
}

Account::Account(const double balance)
    : balance_(balance)
{
}

void
Account::setBalance(const double balance)
{       
    assert(balance > 0);
    balance_ = balance;
}

void 
Account::setCredit(const double credit)
{
    assert(credit > 0);
    balance_ += credit;
}

void
Account::setDebit(const double debit)
{
    assert(debit > 0 && balance_ - debit >= 0);
    balance_ -= debit;
}


double
Account::getBalance() const
{
    return balance_;
}

