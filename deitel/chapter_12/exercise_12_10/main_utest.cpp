#include "headers/Account.hpp"
#include "headers/SavingAccount.hpp"
#include "headers/CheckingAccount.hpp"
#include <gtest/gtest.h>


TEST(SavingAccount, showeBalance)
{
    const double rate = 0.2;    
    SavingAccount myAccount(2000.00, rate);
    const double balance = myAccount.getBalance();  
    EXPECT_EQ(balance, 2000.00);
}

TEST(SavingAccount, calculateInterest)
{
    const double rate = 2;    
    SavingAccount myAccount(2000.00, rate);
    myAccount.calculateInterest(); 
    EXPECT_EQ(myAccount.getBalance(), 2040.00);  
}

TEST(CheckingAccount, validationCommission)
{
    CheckingAccount account(2000, 0.1);
    const double debite = 150;
    if (account.tranzactionSuccessfully(debite)) {
        account.setDebit(debite);
    }
    EXPECT_EQ(account.getBalance(), 1835);
}

TEST(CheckingAccount, invalidDebit)
{
    CheckingAccount account(2000, 0.10);
    const double debite = 1500;
    const bool negativRespons = (account.tranzactionSuccessfully(debite));
    EXPECT_TRUE(negativRespons);
}


int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
