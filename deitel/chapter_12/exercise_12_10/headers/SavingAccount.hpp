#ifndef __SAVING_ACCOUNT_HPP__
#define __SAVING_ACCOUNT_HPP__

#include "Account.hpp"

class SavingAccount : Account
{
public:
    SavingAccount(const double balance, const double rate);
    void setRate(const double rate);
    double calculateInterest();
    double getBalance() const;
    double getRate() const;
private:
    double rate_;
};

#endif /// __SAVING_ACCOUNT_HPP__
