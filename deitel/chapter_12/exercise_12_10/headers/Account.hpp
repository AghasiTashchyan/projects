#ifndef __ACCOUNT_HPP__
#define __ACCOUNT_HPP__

class Account
{
public:
    Account();
    Account(const double balance);
    void setBalance(const double balance);
    void setCredit(const double credit);
    void setDebit(const double debit);
    double getBalance() const;
protected:
    double balance_;
};

#endif /// __ACCOUNT_HPP__
