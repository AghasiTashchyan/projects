#ifndef __CHECKING_ACCOUNT_HPP__
#define __CHECKING_ACCOUNT_HPP__

class CheckingAccount : public Account
{
public:
    CheckingAccount(const double balance, const double feeCharged);
    bool tranzactionSuccessfully(const double debit);
    void setDebit(const double debite);
private:
    const double feeCharged_;

};

#endif /// __CHECKING_ACCOUNT_HPP__
