#include <iostream>
#include <unistd.h>

double
getDoublePrecisionValue(const std::string& message)
{
        std::cout << message;
    double value;
    std::cin >> value;
    return value;
}
double
smallest (const double number1, const double number2, const double number3)
{
    double smallest = number1;
    if (number2 < smallest) {
        smallest = number2;
    }
    if (number3 < smallest) {
        smallest = number3;
    }
    return smallest;
}

int
main()
{
    const double number1 = getDoublePrecisionValue("Input first number: ");
    const double number2 = getDoublePrecisionValue("Input next number: ");
    const double number3 = getDoublePrecisionValue("Input last number: ");
    std::cout << "Smallest is " << smallest(number1, number2, number3);
    return 0;
}
