#include <iostream>
#include <cassert>
#include <iomanip>
#include <unistd.h>
#include <cmath>

double
hypotenuse(const double side1, const double side2)
{
    assert(side1 > 0 && side2 > 0);
    const double hypotenuse = ::sqrt((side1 * side1) + (side2 * side2));
    return hypotenuse;
}
double
getSide(const std::string &message)
{
    if(::isatty(STDIN_FILENO)){
        std::cout << message;
    }
    double side;
    std::cin >> side;
    return side;
}

int
main()
{
    std::cout << "Triangle " << std::setw(5) << "side 1 " << std::setw(5) << "side 2 " << std::setw(5) << "Hypotenuse" << std::endl;
    int count = 0; 
    while (true) {
        const double side1 = getSide("Input side 1 (-1 to quit). ");
        if (-1 == side1) {
            break;
        }
        const double side2 = getSide("Input side 2: ");
        const double value = hypotenuse(side1, side2);
        ++count; 
        std::cout << std::setw(4) << count << std::setw(8)
              << std::setw(8) << side1
              << std::setw(6) << side2 
              << std::setw(8) << std::setprecision(4) 
              << value << std::endl;
    }
    return 0;
}

