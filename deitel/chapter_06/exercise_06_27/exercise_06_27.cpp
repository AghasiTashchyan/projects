#include <iostream>
#include <unistd.h>
#include <iomanip>

int
celsius(const int celsius) 
{
    return celsius * 9 / 5 + 32;
}

int
fahrenheit(const int fahrenheit) 
{
    return (fahrenheit - 32) * 5 / 9;
}
int
getIntegerValue(const std::string& message)
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << message;
    }
    int value;
    std::cin >> value;
    return value;
}


int 
main()
{
    const int equivalentOfCelsius = getIntegerValue("Input Celsius: ");
    std::cout << "Celsius : " << equivalentOfCelsius << "\n" 
            << "Fahrenheit: " << celsius(equivalentOfCelsius) << std::endl;

    const int equivalentOfFahrenheit = getIntegerValue("Input Fahrenheit: ");
    std::cout << "Fahrenheit: " << equivalentOfFahrenheit << "\n" 
            << "Celsius: " << fahrenheit(equivalentOfFahrenheit) << std::endl;

    std::cout << "Celsius     Fahrenheit" << std::endl;
    for (int i = 0; i <= 100; ++i) {
        std::cout << std::setw(7) << std:: left << i 
                  << std::setw(14) << std::right 
                  << celsius(i) << std::endl;
    }

    std::cout << "Fahrenheit     Celsius" << std::endl;
    for (int i = 32; i <= 212; ++i) {
        std::cout << std::setw(7) << std::left << i 
                  << std::setw(14) << std::right 
                  << fahrenheit(i) << std::endl;
    }
    return 0;
}

