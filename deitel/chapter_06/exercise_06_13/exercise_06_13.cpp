#include <iostream>
#include <cmath>
#include <unistd.h>

double
inputNumber()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input number: ";
    }
    double number;
    std::cin >> number;
    return number;
}

int
roundUp(double number) 
{
    return ::floor(number + 0.5);
}

int 
main()
{
    const int integerNumber = roundUp(inputNumber());
    std::cout << "Integer number is " <<  integerNumber << std::endl;
    return 0;
}

