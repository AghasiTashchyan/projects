#include <iostream>
#include <unistd.h>
#include <cassert>

int
getIntegerValue(const std::string& message)
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << message;
    }
    int number;
    std::cin >> number;
    return number;
}

int 
mirroringNumber(const int number)
{
    assert(0 < number || 9999 > number);
    int num = number;
    int mirrorNumber = 0;
    int digit = 1000;
    while (0 != num) {
        int digitValue  =  num % 10;
        num /= 10;
        mirrorNumber += digitValue * digit;
        digit /= 10; 
    }
    return mirrorNumber;
}

int 
main()
{
    const int number = getIntegerValue("Input number: ");
    if (0 > number) {
        std::cout << "Error 1: Invalid value " << std::endl;
        return 1;
    }
    const int mirrorNumber = mirroringNumber(number);
    std::cout << mirrorNumber;
    return 0;
}

