#include <iostream>
#include <unistd.h>

bool 
even(const int number)
{
    return (0 == number % 2);
}

int 
main()
{
    while (true) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Input number (-1 to quit). ";
        }
        int number;
        std::cin >> number;
        if (-1 == number) {
            break;
        }
        std::cout << (even(number)? "Number is even.\n" : "Number is not even.\n");
    }
    return 0;
}

