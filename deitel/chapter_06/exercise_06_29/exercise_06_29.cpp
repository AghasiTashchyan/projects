#include <iostream>

bool
perfectNumber(const int perfectNumber)
{
    int sum = 0;
    for (int i = 1; i <= perfectNumber / 2; ++i) {
        if (0 == perfectNumber % i) {
            sum += i;
        }
    }
    return sum == perfectNumber;
}

int
main()
{
    for (int i = 1; i <= 1000 / 2 ; ++i) {
        if (perfectNumber(i)) {
            std::cout << "Perfect number: " << i << " =" << " 1";
            for (int j = 2; j <= i / 2; ++j) {
                if (0 == i % j) {
                    std::cout << "+" << j;
                }
            }
            std::cout << std::endl;
        }
    }
    return 0;
}

