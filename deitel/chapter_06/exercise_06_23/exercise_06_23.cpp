#include <iostream>
#include <cassert>
#include <unistd.h>
#include <ctype.h>

void 
solidSquare(const int side, const char character) 
{
    assert(side > 0 && std::isprint(character));
    for (int y = side; y > 0; --y) {
        for (int x = 0; x < side; ++x) {
            std::cout << character;
        }
        std::cout << std::endl; 
    }
}

int 
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input side: ";
    }
    int side;
    std::cin >> side;
    if (0 > side) {
        std::cout << "Error 1: Invalid value. " << std::endl;
        return 1;
    }
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input character: ";
    }
    char fillCharacter;
    std::cin >> fillCharacter;
    if (!std::isprint(fillCharacter)) {
        std::cout << "Error 2: Character is not printable." << std::endl;
        return 2;
    }
    solidSquare(side, fillCharacter);
    return 0; 
}

