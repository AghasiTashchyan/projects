#include <iostream>
#include <cassert>
#include <unistd.h>

void 
printSquare(const int side) 
{
    assert(side > 0);
    for (int i = side; i > 0; --i) {
        for (int x = 0; x < side; ++x) {
            std::cout << "*";
        }
        std::cout << std::endl; 
    }
}

int 
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input side of square: ";
    }
    int side;
    std::cin >> side;
    if (0 > side) {
        std::cout << "Error 1: Invalid value." << std::endl;
        return 1;
    }
    printSquare(side);
    return 0;
}

