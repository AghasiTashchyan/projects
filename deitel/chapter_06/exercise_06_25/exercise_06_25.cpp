#include <iostream>
#include <unistd.h>
#include <cassert>

int
quotient(const int number1, const int number2)
{
    assert(0 != number2);
    return number1 / number2;
}

int
remainder(const int number1, const int number2)
{
    assert(0 != number2);
    return number1 % number2;
}

void
printNumberWithSpace(int number)
{
    int division = 10000;
    while (0 != division) {
        const int digit = quotient(number, division);
        const int modulusOfNumber = remainder(number, division);
        if (modulusOfNumber == number) {
            division = quotient(division, 10);
            continue;
        }
        number = remainder(number, division);
        std::cout << digit << "  ";
        division = quotient(division, 10);
    }
    std::cout << std::endl;
}

int 
getIntegerValue(const std::string& message)
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << message;
    }
    int integerValue;
    std::cin >> integerValue;
    return integerValue;
}

int
main()
{
    const int number = getIntegerValue("Input number");
    if (number < 1 || number > 32767) {
        std::cout << "Error 1: Invalid value. " << std::endl;
        return 1;
    }
    printNumberWithSpace(number);
    return 0;
}

