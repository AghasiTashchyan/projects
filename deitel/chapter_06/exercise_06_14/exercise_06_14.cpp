#include <iostream>
#include <cmath>
#include <unistd.h>

double
inputNumber()
{
    if (::isatty(STDIN_FILENO)){
        std::cout << "Input number";
    }
    double number;
    std::cin >> number;
    return number;
    
}


int 
roundToInteger(const int number)
{
    return ::floor(number + 0.5);
}

double 
roundToTenths(const double number) 
{
    return ::floor(number * 10 + 0.5) / 10;
}


double 
roundToHundredths(const double number) 
{
    return ::floor(number * 100 + 0.5) / 100;
}

double 
roundToThousandths(const double number) 
{
    return ::floor(number * 1000 + 0.5) / 1000;
}

int 
main()
{
    double number = inputNumber();
    std::cout << "Number to integer: " << roundToInteger(number) << std::endl;
    std::cout << "Number to tenths: " << roundToTenths(number) << std::endl;
    std::cout << "Number to hundredths: " << roundToHundredths(number) << std::endl;
    std::cout << "Number to thousandths: " << roundToThousandths(number) << std::endl;
    return 0;
}

