#include "headers/IntegerSet.hpp"
#include <cassert>
#include <cstddef>
#include <iostream>


std::ostream& 
operator<<(std::ostream& out, const IntegerSet& rhv)
{
    for (int i = 0; i < rhv.getSize(); ++i) {
        out << rhv.integers_[i] << " ";
    }
    return out;
}

IntegerSet::IntegerSet()
    : integers_(NULL)
{
    emptySet();
}

IntegerSet::IntegerSet(int* array, const int size)
{
    setArray(array, size);
}

IntegerSet::~IntegerSet()
{
    if (integers_ != NULL) {
        delete[] integers_;
        integers_ = NULL;
    }
}
void
IntegerSet::emptySet()
{
    if (integers_ == NULL) {
        integers_ = new int [getSize()];
    }
    for (int i = 0; i < getSize(); ++i) {
        integers_[i] = 0;
    }
}

void
IntegerSet::insertElement(const int index, const int value)
{
    assert(index >= 0 && index <= 100);
    if (0 == value) {
        integers_[index - 1] = 0;

    }
    if (value >= 0) {
        integers_[index - 1] = 1;
    }
}

void
IntegerSet::deletElement(const int index)
{
    assert(index >= 0 && index <= 100);
    integers_[index - 1] = 0;
}

void 
IntegerSet::setArray(int* array, const int size)
{
    integers_ = new int[size];
    for (int i = 0; i < size; ++i) {
        if (array[i] >= 0 && array[i] <= 100) {
            integers_[i] = 1;
        }
        if (array[i] <= 0 && array[i] >= 100) {
            integers_[i] = 0;
        }
    } 
}

IntegerSet
IntegerSet::unionOfSets(const IntegerSet& rhv) const
{
    const int size = (getSize() > rhv.getSize()) ? getSize() : rhv.getSize();
    int array[size] = {0};
    for (int i = 0; i < size; ++i) {
        if (0 == integers_[i] && 0 == rhv.integers_[i]) {
            array[i] = 0;
        }else {
            array[i] = 1;
        }
    }
    IntegerSet result(array, size);
    return result;
}

IntegerSet
IntegerSet::intersectionOfSets(const IntegerSet& rhv) const
{
    const int size = (getSize() > rhv.getSize()) ? getSize() : rhv.getSize();
    int array[size];
    for (int i = 0; i < size; ++i) {
        if (0 == integers_[i] || 0 == rhv.integers_[i]) {
            array[i] = 0;
        }
        if (1 == integers_[i] && 1 == rhv.integers_[i]) {
            array[i] = 1;
        }
    }
    IntegerSet result(array, size);
    return result;
}

bool
IntegerSet::isEqualTo(const IntegerSet& rhv) const
{
    if (this-> getSize() != rhv.getSize()) {
        return false;
    }
    for (int i =0; i < this-> getSize(); ++i) {
        if (this-> integers_[i] != rhv.integers_[i]) {
            return false;
        }
    }
    return true;
}

int 
IntegerSet::getSize() const
{
    return size_;
}

void
IntegerSet::printSet() const 
{
    for (int i = 0; i < getSize(); ++i) {
        if (integers_[i] > 0) {
            std::cout << i << " ";
        }
    }   
}


