#include "headers/IntegerSet.hpp"
#include <gtest/gtest.h>
#include <iostream>

TEST(IntegerSet, ArrayInput)
{
    const int SIZE = 100;
    int array[SIZE] = {1};
    for (int i = 0; i < SIZE; ++i) {
        array[i] = i + 50;
    }
    IntegerSet is(array, SIZE);
    std::stringstream out;
    out << is;
    EXPECT_EQ(out.str(), "1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ");
}

TEST(IntegerSet, UnionSets)
{
    const int SIZE = 100;
    int array1[SIZE];
    for (int i = 0; i < SIZE; ++i) {
        array1[i] = i + 50;
    }
    int array2[SIZE];
    for (int i = 0; i < SIZE; ++i) {
        array2[i] = i + 52;
    }
    IntegerSet myIntegersSet1(array1, SIZE);
    IntegerSet myIntegersSet2(array2, SIZE);
    IntegerSet myIntegersSet3;
    myIntegersSet3 = myIntegersSet1.unionOfSets(myIntegersSet2);
    std::stringstream out;
    out << myIntegersSet3;
    EXPECT_EQ(out.str(), "1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ");
}


TEST(IntegerSet, intersectionOfSets)
{
    const int SIZE = 100;
    int array1[SIZE];
    for (int i = 0; i < SIZE; ++i) {
        array1[i] = i + 80;
    }
    int array2[SIZE];
    for (int i = 0; i < SIZE; ++i) {
        array2[i] = i + 41;
    }
    IntegerSet myIntegersSet1(array1, SIZE);
    IntegerSet myIntegersSet2(array2, SIZE);
    IntegerSet myIntegersSet3;
    myIntegersSet3 = myIntegersSet1.intersectionOfSets(myIntegersSet2);
    std::stringstream out;
    out << myIntegersSet3;
    EXPECT_EQ(out.str(), "1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ");
}

TEST(IntegerSet, equalityIntegers)
{
    const int SIZE = 100;
    int array1[SIZE];
    for (int i = 0; i < SIZE; ++i) {
        array1[i] = i + 80;
    }
    int array2[SIZE];
    for (int i = 0; i < SIZE; ++i) {
        array2[i] = i + 80;
    }
    IntegerSet myIntegersSet1(array1, SIZE);
    IntegerSet myIntegersSet2(array2, SIZE);
    const bool isTrue = myIntegersSet1.isEqualTo(myIntegersSet2);
    EXPECT_TRUE(isTrue);

}

TEST(IntegerSet, insertElement)
{
    const int SIZE = 100;
    int array[SIZE] = {1};
    for (int i = 0; i < SIZE; ++i) {
        array[i] = i + 50;
    }
    IntegerSet myIntegersSet(array, SIZE);
    myIntegersSet.insertElement(100, 1);
    std::stringstream out;
    out << myIntegersSet;
    EXPECT_EQ(out.str(), "1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 ");
}

TEST(IntegerSet, deleteElement)
{
    const int SIZE = 100;
    int array[SIZE] = {1};
    for (int i = 0; i < SIZE; ++i) {
        array[i] = i + 50;
    }
    IntegerSet myIntegersSet(array, SIZE);
    for (int i = 0; i < 10; ++i) {
        myIntegersSet.deletElement(i);
    }
    std::stringstream out;
    out << myIntegersSet;
    EXPECT_EQ(out.str(), "0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ");
}

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();

}
