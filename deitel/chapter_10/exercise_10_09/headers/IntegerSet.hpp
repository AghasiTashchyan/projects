#include <iostream>


class IntegerSet
{
private:
    friend std::ostream& operator<<(std::ostream& out, const IntegerSet& rhv);
public:
    IntegerSet();
    ~IntegerSet();
    IntegerSet(int* array, const int size);
    IntegerSet unionOfSets(const IntegerSet& rhv) const;
    IntegerSet intersectionOfSets(const IntegerSet& rhv) const;
    void emptySet();
    void insertElement(const int index, const int value);
    void deletElement(const int index);
    void setArray(int* array, const int size);
    void printSet() const;
    bool isEqualTo(const IntegerSet& rhv) const;
    int getSize() const;
private:
    int size_;
    int* integers_;
};
