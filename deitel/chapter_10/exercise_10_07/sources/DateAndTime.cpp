#include "headers/DateAndTime.hpp"
#include <iostream>
#include <ctime>
#include <iomanip>

static const std::string months[13] = {"", "January", "February", "March", "April", "May", "June",
                            "July", "August", "September", "October", "November", "December"};
static const std::string week[8] = {"", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};

DateAndTime::DateAndTime()
{
    const int timeNew = ::time(0) + (3600 * 4);
    const int hours = timeNew % (24 * 60 * 60 ) / (60 * 60);
    const int minutes = timeNew % 3600 / 60;
    const int seconds = timeNew % 60;
    std::time_t t = std::time(NULL);
    std::tm* now = std::localtime(&t);
    setHour(hours);
    setMinute(minutes);
    setSecond(seconds);
    setYear(now-> tm_year + 1900);
    setDay(now-> tm_mday);
    setMonth(now-> tm_mon + 1);    

}

DateAndTime::DateAndTime 
(const int second, const int minute, const int hour, const int month, const int day, const int year)
    : second_ (second)
    , minute_ (minute)
    , hour_ (hour) 
    , month_ (month)
    ,day_ (day)
    , year_ (year)
{
}

void 
DateAndTime::setDay(const int day)
{
    day_ = day;
}
void 
DateAndTime::setMonth(const int month)
{
    month_ = month;
}

void
DateAndTime::setYear(const int year)
{
    year_ = year;
}
void 
DateAndTime::setHour(const int hour)
{
    hour_ = hour;
}

void
DateAndTime::setMinute(const int minute)
{
    minute_ = minute;
}
void
DateAndTime::setSecond(const int second)
{
    second_ = second;
}

void 
DateAndTime::printDataAndTime() const
{
    const char oldFill = std::cout.fill('0');
    std::cout << ((0 == getHour() || 12 == getHour()) ? 12 : getHour() % 12)
              << ":" << std::setfill('0') << std::setw(2) << getMinute()
              << ":" << std::setw(2) << getSecond() << (hour_ < 12 ? " AM" : " PM");
    std::cout.fill(oldFill); 
    std::cout << "\t" << months[getMonth()] << "/" << getDay() << "/" << getYear() << std::endl;

    
}
void 
DateAndTime::printDataAndTimeMMDDYY()const
{
    const char oldFill = std::cout.fill('0');
    std::cout << ((0 == getHour() || 12 == getHour()) ? 12 : getHour() % 12)
              << ":" << std::setfill('0') << std::setw(2) << getMinute()
              << ":" << std::setw(2) << getSecond() << (hour_ < 12 ? " AM" : " PM");
    std::cout.fill(oldFill); 
    std::cout << "\t" << getDay() << "   " << week[getDay() % 7 + 1] << "/" << getMonth() << "/" << getYear() % 100 << std::endl;

    
}

int
DateAndTime::getDay() const
{
    return day_;
}

int 
DateAndTime::getMonth() const
{
    return month_;
}

int
DateAndTime::getYear() const
{
    return year_;

}

int
DateAndTime::getHour() const
{
    return hour_;
}

int
DateAndTime::getMinute() const
{
    return minute_;
}

int 
DateAndTime::getSecond() const
{
    return second_;
}

