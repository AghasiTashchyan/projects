#ifndef __DATEANDTIME_HPP__
#define __DATEANDTIME_HPP__

class DateAndTime
{
public:
    DateAndTime();
    DateAndTime(const int second= 0, const int minute =0, const int hour=0, const int mount=0, const int day=0, const int year=0);
    void setDay(const int day);
    void setMonth(const int mount);
    void setYear(const int year);
    void setHour(const int hour);
    void setMinute(const int minute);
    void setSecond(const int second);
    void printDataAndTime() const;
    void printDataAndTimeMMDDYY()const;
    int getDay() const;
    int getMonth() const;
    int getYear() const;
    int getHour() const;
    int getMinute() const;
    int getSecond() const;
    void secondIncrement();
private:
    int second_;
    int minute_;
    int hour_;
    int month_;
    int day_;
    int year_;
};

#endif // __DATEANDTIME_HPP__
