#include "headers/SavingsAccount.hpp"
#include <gtest/gtest.h>
#include <cmath>

TEST(SavingsAccount, setingBalance)
{
    const double balance1 = 2000.00;
    SavingsAccount account1(balance1);
    account1.setRate(2);
    const double balance2 = 3000.00;
    SavingsAccount account2(balance2);
    account2.setRate(1);
    EXPECT_EQ(account1.getSavingBalance(), 2000.00);
    EXPECT_EQ(account2.getSavingBalance(), 3000.00);
}

TEST(SavingsAccount, calculatyonRate)
{
    const double balance1 = 2000.00;
    SavingsAccount account1(balance1);
    account1.setRate(2);
    account1.calculateMonthlyInterest();
    const double EPSILION = 0.00001;
    const bool result = (::abs(account1.calculateMonthlyInterest() - 2006.67) >= EPSILION);
    EXPECT_TRUE(result);  
}

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

