
class SavingsAccount
{
public:
    static void setRate(double rate);
    static double getRate();
    static double rate_;
public:
    SavingsAccount();
    SavingsAccount(const double savingBalance);
    void setSavingBalance(const double setSavingBalance);
    double getSavingBalance() const;
    double calculateMonthlyInterest();
private:
    double savingBalance_;
};

