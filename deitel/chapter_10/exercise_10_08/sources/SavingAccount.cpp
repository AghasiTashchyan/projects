#include "headers/SavingsAccount.hpp"
#include <cassert>

double SavingsAccount::rate_ = 0;

SavingsAccount::SavingsAccount()
    : savingBalance_(0)
{
}

SavingsAccount::SavingsAccount(const double savingBalance)
    :savingBalance_(savingBalance)
{
}

void
SavingsAccount::setSavingBalance(const double savingBalance)
{       
    assert(savingBalance > 0);
    savingBalance_ = savingBalance;
}


void
SavingsAccount::setRate(double rate)
{
    assert(rate > 0);
    rate_ = rate;
}

double
SavingsAccount::getSavingBalance() const
{
    return savingBalance_;
}

double
SavingsAccount::getRate()
{
    return rate_;
}

double
SavingsAccount::calculateMonthlyInterest()
{
    savingBalance_ += savingBalance_ * rate_ / 1200;
    return savingBalance_;
}
