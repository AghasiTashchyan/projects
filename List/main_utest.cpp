#include "headers/List.hpp"
#include <gtest/gtest.h>
#include <iostream>

struct A 
{
    int x;
    A(){ std::cout << "A::A()" << std::endl;}
    A(const A&) {std::cout << "A::A(const A&)" << std::endl;}
    const A& operator=(const A&) { std::cout << "A::operator=(const A&)" << std::endl; return *this; }
    ~A() { std::cout << "A::~A() " << std::endl;}
};

TEST(ListA, DefaultConstructor)
{
    List<int> l;
    l.push_back(7);
    l.push_back(8);
    l.push_back(9);
    List<int> l1;
    l = l1; 
}

TEST(Listint, SizeConstructor)
{
    List<int> l(5);
    EXPECT_EQ(l.size(), 5);
}

TEST(Listint, resizeConstructor)
{
    List<int> l(5, 1);
    std::stringstream out;
    out << l;
    EXPECT_EQ(out.str(), "1 1 1 1 1 ");
}

TEST(ListInt, CopyConstructor)
{
    List<int> l(5, 1);
    List<int> l2(l);
    std::stringstream out;
    out << l2;
    EXPECT_EQ(out.str(), "1 1 1 1 1 ");
}

TEST(ListInt, RangeConstructor)
{
    int arr[] = {1, 2, 3, 4, 5, 6, 7};
    List<int> l(arr, arr + 5);
    std::stringstream out;
    out << l;
    EXPECT_EQ(out.str(), "1 2 3 4 5 ");
}

TEST(ListInt, clear)
{
    List<int> l;
    l.push_back(0);
    l.push_back(1);
    l.push_back(2);
    l.clear();
    std::stringstream out;
    out << l;
    EXPECT_EQ(out.str(), "");
}


TEST(ListInt, pop_push_front)
{
    List<int> l;
    l.push_front(0);
    l.push_front(1);
    l.push_front(2);
    l.pop_front();
    l.pop_front();
    l.push_front(2);
    std::stringstream out;
    out << l;
    EXPECT_EQ(out.str(), "2 0 ");
}

TEST(ListInt, empty)
{
    List<int> l;
    const bool result = l.empty();
    EXPECT_TRUE(result);
}

TEST(ListInt, size)
{
    List<int> l;
    l.push_back(0);
    l.push_back(0);
    size_t size = 2;
    EXPECT_EQ(l.size(), size);
}

TEST(ListInt, push_front)
{
    List<int> l;
    l.push_front(1);
    std::stringstream out;
    out << l;
    EXPECT_EQ(out.str(), "1 ");
}

TEST(ListInt, insert)
{
    List<int> l;
    l.push_front(3);
    List<int>::iterator it = l.begin();
    it = l.insert(it, 9);
    l.insert(it, 8);
    std::stringstream out;
    out << l;
    EXPECT_EQ(out.str(), "8 9 3 ");
}

TEST(ListInt, insertN)
{
    List<int> l;
    l.push_front(3);
    List<int>::iterator it = l.begin();
    it = l.insert(it, 9);
    l.insert(it, 2, 8);
    std::stringstream out;
    out << l;
    EXPECT_EQ(out.str(), "8 8 9 3 ");
}

TEST(ListInt, insertRange)
{
    List<int> l;
    l.push_front(3);
    List<int>::iterator it = l.begin();
    int arr [] = {1, 2, 3, 4, 5, 6, 7};
    l.insert(it, arr, arr + 7);
    std::stringstream out;
    out << l;
    EXPECT_EQ(out.str(), "7 6 5 4 3 2 1 3 ");
}

TEST(ListInt, insert_afterRange)
{
    List<int> l;
    l.push_front(3);
    List<int>::iterator it = l.begin();
    int arr [] = {1, 2, 3, 4, 5, 6, 7};
    l.insert_after(it, arr, arr + 7);
    std::stringstream out;
    out << l;
    EXPECT_EQ(out.str(), "3 1 2 3 4 5 6 7 ");
}

TEST(ListInt, eraseRange)
{
    List<int> l;
    l.push_front(6);
    l.push_front(7);
    l.push_front(8);
    List<int>::iterator it = l.begin();
    List<int>::iterator jt = l.end();
    l.erase(it, jt);
    std::stringstream out;
    out << l;
    EXPECT_EQ(out.str(), "");
    EXPECT_TRUE(l.empty());
}


TEST(ListInt, insert_after)
{
    List<int> l;
    l.push_back(3);
    List<int>::iterator it = l.begin();
    l.insert_after(it, 9);
    std::stringstream out;
    out << l;
    EXPECT_EQ(out.str(), "3 9 ");
}

TEST(ListInt, erasePos)
{
    List<int> l;
    l.push_front(6);
    l.push_front(7);
    l.push_front(8);
    l.push_front(9);
    List<int>::iterator it = l.begin();
    for (int i = 0; i < 2; ++i) {
       it = l.erase(it);
    }
    std::stringstream out;
    out << l;
    EXPECT_EQ(out.str(), "7 6 ");
}

int
main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
