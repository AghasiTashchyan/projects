#ifndef __LIST_HPP__
#define __LIST_HPP__

#include <cstddef>
#include <iostream>

template <typename T>
class List
{
    template <typename U>
    friend std::ostream& operator<<(std::ostream& out, const List<U>& rhv);
    struct Node {
        Node() {}
        Node(const T& d, Node* n = NULL, Node* p = NULL)
            : data_(d), next_(n), prev_(p) {}
        T data_;
        Node* next_;
        Node* prev_;
    };

public:
    typedef T value_type;
    typedef value_type& reference;
    typedef const value_type& const_reference;
    typedef value_type* pointer;
    typedef std::ptrdiff_t difference_type;
    typedef std::size_t size_type;

    class const_iterator {
        friend List<T>;
    public:
        const_iterator();
        const_iterator(const const_iterator& rhv);
        const_iterator(Node* ptr);
        ~const_iterator();
        const const_iterator& operator=(const const_iterator& rhv);
        const value_type& operator*() const;
        const value_type* operator->() const;
        Node* getPtr() const;
        const_iterator operator++();
        const_iterator operator++(int);
        const_iterator operator--();
        const_iterator operator--(int);
        bool operator==(const const_iterator& rhv) const;
        bool operator!=(const const_iterator& rhv) const;
    private:
        Node* ptr_;
    };

    class iterator : public const_iterator {
        friend List<T>;
    public:
        iterator();
        iterator(Node* ptr);
        value_type& operator*() const;
        value_type* operator->() const;
        iterator operator++();
        iterator operator++(int);
        iterator operator--();
        iterator operator--(int);
    };

    class const_reverse_iterator {
        friend List<T>;
    public:
        const_reverse_iterator();
        const_reverse_iterator(const const_reverse_iterator& rhv);
        const_reverse_iterator(Node* ptr);
        ~const_reverse_iterator();
        const const_reverse_iterator& operator=(const const_reverse_iterator& rhv);
        const value_type& operator*() const;
        const value_type* operator->() const;
        Node getPtr() const;
        const_reverse_iterator operator++();
        const_reverse_iterator operator++(int);
        const_reverse_iterator operator--();
        const_reverse_iterator operator--(int);
        bool operator==(const const_reverse_iterator& rhv) const;
        bool operator!=(const const_reverse_iterator& rhv) const;
    private:
        Node* ptr_;
    };

    class reverse_iterator : public const_reverse_iterator {
        friend List<T>;
    public:
        reverse_iterator();
        reverse_iterator(Node* ptr);
        value_type& operator*() const;
        value_type* operator->() const;
        reverse_iterator operator++();
        reverse_iterator operator++(int);
        reverse_iterator operator--();
        reverse_iterator operator--(int);
    };

    List();
    List(const size_type n);
    List(const int n, const T& t);
    List(const List& rhv);
    template <typename InputIterator>
    List(InputIterator f, InputIterator l);
    ~List();
    const List& operator=(const List& rhv);
    bool operator==(const List& rhv) const;
    bool operator!=(const List& rhv) const;
    bool operator<(const List& rhv) const;
    bool operator<=(const List& rhv) const;
    bool operator>(const List& rhv) const;
    bool operator>=(const List& rhv) const;
    void swap(List&);
    bool empty() const;
    void clear();
    void resize(const size_type n, const T& t);
    void resize(const size_type n);
    void push_front(const_reference value);
    void pop_front();
    void push_back(const_reference value);
    void pop_back();
    value_type& front();
    const value_type& front() const;    
    value_type& back();
    const value_type& back() const;
    size_type size() const;
    size_type max_size() const;
    const_iterator begin() const;
    iterator begin();
    const_iterator end() const;
    iterator end();
    const_reverse_iterator rbegin() const;
    const_reverse_iterator rend() const;
    reverse_iterator rbegin();
    reverse_iterator rend();
    
    iterator insert(iterator pos, const T& x);
    iterator insert_after(iterator pos);
    iterator insert_after(iterator pos, const T& x);
    void insert(iterator pos, const int n, const T& x);
    template <typename InputIterator>
    void insert(iterator pos, InputIterator f, InputIterator l);
    void insert_after(iterator pos, int n, const T& x);
    template <typename InputIterator>
    void insert_after(iterator pos, InputIterator f, InputIterator l);
    iterator erase(iterator pos);
    iterator erase(iterator f, iterator l);
private:
    Node* begin_;
    Node* end_;
};

template <typename T>
std::ostream& operator<<(std::ostream& out, const List<T>& rhv)
{
    typename List<T>::Node* temp = rhv.begin_;
    while(temp != NULL) {
        out << temp->data_ << " ";
        temp = temp-> next_;
    }
    return out;
}

#include "templates/List.cpp"

#endif ///__LIST_HPP__

