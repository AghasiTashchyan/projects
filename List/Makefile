progname=SinglList
lib=lib$(progname).a
so=$(progname).so
utest=utest_$(progname)
CXX=g++
CXXFLAGS=-Wall -Wextra -Werror -std=c++03 -I.
BUILDS=builds
BUILD_DIR=$(BUILDS)

ifeq ($(MAKECMDGOALS),)
    BUILD_DIR=$(BUILDS)/debug
else
    BUILD_DIR=$(BUILDS)/$(MAKECMDGOALS)
endif

SOURCES:=main.cpp $(wildcard sources/*.cpp)
PREPROCS:=$(patsubst %.cpp,%.ii,$(SOURCES))
DEPENDS:=$(patsubst %.cpp,%.d,$(SOURCES))
ASSEMBLES:=$(patsubst %.cpp,%.s,$(SOURCES))
OBJS:=$(patsubst %.cpp,%.o,$(SOURCES))

LIB_SOURCES:=$(wildcard sources/*.cpp)
LIB_PREPROCS:=$(patsubst %.cpp,%.ii,$(LIB_SOURCES))
LIB_DEPENDS:=$(patsubst %.cpp,%.d,$(LIB_SOURCES))
LIB_ASSEMBLES:=$(patsubst %.cpp,%.s,$(LIB_SOURCES))
LIB_OBJS:=$(patsubst %.cpp,%.o,$(LIB_SOURCES))

UTEST_SOURCES:=main_utest.cpp $(wildcard sources/*.cpp)
UTEST_PREPROCS:=$(patsubst %.cpp,%.ii,$(UTEST_SOURCES))
UTEST_DEPENDS:=$(patsubst %.cpp,%.d,$(UTEST_SOURCES))
UTEST_ASSEMBLES:=$(patsubst %.cpp,%.s,$(UTEST_SOURCES))
UTEST_OBJS:=$(patsubst %.cpp,%.o,$(UTEST_SOURCES))

TEST_INPUTS:=$(wildcard test*.input)
TESTS:=$(patsubst %.input,%,$(TEST_INPUTS))

debug:   CXXFLAGS+=-g3
release: CXXFLAGS+=-g0 -DNDEBUG -Wno-unused-parameter

debug:   $(BUILD_DIR) utest qa
release: $(BUILD_DIR) utest qa

qa: $(TESTS)

utest: $(BUILD_DIR)/$(utest)
	./$<

test%: $(BUILD_DIR)/$(progname)
	./$< < $@.input > $@.output || echo "Negative test"
	diff $@.output $@.expected > /dev/null && echo "$@ PASSED" || echo "$@ FAILED"

$(BUILD_DIR)/$(lib): $(LIB_OBJS) | .gitignore
	ar -crv $@ $^

$(BUILD_DIR)/$(so): $(LIB_OBJS) | .gitignore
	$(CXX) $(CXXFLAGS) --shared $^ -o $@

$(BUILD_DIR)/$(progname): $(OBJS) | .gitignore
	$(CXX) $(CXXFLAGS) $^ -o $@

$(BUILD_DIR)/$(utest): $(UTEST_OBJS) | .gitignore
	$(CXX) $(CXXFLAGS) $^ -lgtest -lpthread -o $@

%.ii: %.cpp
	$(CXX) -E $(CXXFLAGS) $< -o $@
	$(CXX) $(CXXFLAGS) -MM $< -MT $@ > $(patsubst %.cpp,%.d,$<)

%.s: %.ii
	$(CXX) -S $(CXXFLAGS) $< -o $@

%.o: %.s
	$(CXX) -c $(CXXFLAGS) $< -o $@

$(BUILD_DIR):
	mkdir -p $(BUILD_DIR)

.gitignore:
	echo $(progname) > .gitignore
	echo $(utest)   >> .gitignore

install:
	sudo cp headers/IntVector.hpp /usr/local/include
	sudo cp $(BUILDS)/debug/$(so) /usr/local/lib

clean:
	rm -rf $(BUILDS) *.ii *.d *.s *.o sources/*.ii sources/*.d sources/*.s sources/*.o *.output $(progname) $(utest) .gitignore

.PRECIOUS:  $(PREPROCS) $(ASSEMBLES) $(UTEST_PREPROCS) $(UTEST_ASSEMBLES)
.SECONDARY: $(PREPROCS) $(ASSEMBLES) $(UTEST_PREPROCS) $(UTEST_ASSEMBLES)

sinclude $(DEPENDS)

