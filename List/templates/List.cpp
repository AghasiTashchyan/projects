#include "headers/List.hpp"
#include <cassert>
#include <cstddef>
#include <cmath>

template <typename T>
List<T>::List()
    : begin_(NULL)
    , end_(NULL)
{
}

template <typename T>
List<T>::List(const size_type n)
    : begin_(NULL)
    , end_(NULL)
{
    resize(n, T());
}

template <typename T>
List<T>::List(const List<T>& rhv)
    : begin_(NULL)
    , end_(NULL)
{
    for (const_iterator it = rhv.begin(); it != NULL; ++it) {
        this->push_back(*it);
    }
}

template <typename T>
List<T>::List(const int n, const T& t)
    : begin_(NULL)
    , end_(NULL)
{
    resize(n, t);
}

template <typename T>
const List<T>&
List<T>::operator=(const List<T>& rhv)
{
    if (this == &rhv) {
        return *this;
    }
    resize(rhv.size());
    iterator  it1 = begin();
    for (const_iterator it2 = rhv.begin(); it2 != rhv.end(); ++it1, ++it2) {
        *it1 = *it2;
    }
    return *this;
}

template <typename T>
template <typename InputIterator>
List<T>::List(InputIterator f, InputIterator l)
    : begin_(NULL)
    , end_(NULL)
{
    while(f != l) {
        push_back(*f++);
    }
}

template <typename T>
List<T>::~List()
{
    if (begin_ != NULL) {
        clear();
        delete []begin_;
        begin_ = NULL;
        end_ = NULL;
    }
}

template <typename T>
bool
List<T>::operator==(const List<T>& rhv) const
{
    if (size() != rhv.size()) {
        return false;
    }
    Node* ptr = begin_;
    for (Node* temp = rhv.begin_; temp != NULL; temp = temp->next_) {
        if (ptr->data_ != temp->data_) {
            return false;
        }
        ptr = ptr->next_;
    }
}

template <typename T>
bool
List<T>::operator<(const List<T>& rhv) const
{
    if (size() == rhv.size()) {
        Node* ptr = begin_;
        for (Node* temp = rhv.begin_; temp != NULL; temp = temp->next) {
            if (!(ptr->data_ < temp->data_)) {
                return false;
            }
        }
        return true;
    }
    return size() < rhv.size();
}

template <typename T>
bool
List<T>::operator>(const List<T>& rhv) const
{
    return !(rhv < *this);
}

template <typename T>
bool
List<T>::operator<=(const List<T>& rhv) const
{
    return !(*this > rhv);
}

template <typename T>
bool
List<T>::operator>=(const List<T>& rhv) const
{
    return !(*this < rhv);
}

template <typename T>
bool
List<T>::operator!=(const List<T>& rhv) const
{
    return !(*this == rhv);
}

template <typename T>
typename List<T>::size_type
List<T>::size() const
{
    size_type size = 0;
    for (Node* temp = begin_; temp != NULL; temp = temp->next_) {
        ++size;
    }
    return size;
}

template <typename T>
bool
List<T>::empty() const
{
    return begin_ == NULL;
}

template <typename T>
void
List<T>::push_front(const_reference value)
{
    begin_ = new Node(value, begin_, NULL);
    if (end_ == NULL) {
        end_ = begin_;
        return;
    }
    begin_->next_->prev_ = begin_;
}

template <typename T>
void
List<T>::push_back(const_reference value)
{
    end_ = new Node(value, NULL, end_);
    if (begin_ == NULL) {
        begin_ = end_;
        return;
    }
    end_->prev_->next_ = end_;
}

template <typename T>
void
List<T>::pop_front()
{
    assert(begin_ != NULL);
    Node* temp = begin_->next_;
    delete begin_;
    begin_ = temp;
    (begin_ == NULL ? end_ : begin_->prev_) = NULL;
}

template <typename T>
void
List<T>::pop_back()
{
    assert(end_ != NULL);
    Node* temp = end_->prev_;
    delete end_;
    end_ = temp;
    (end_ == NULL ? begin_ : end_->next_) = NULL;
}

template <typename T>
void
List<T>::resize(const size_type n, const T& t)
{
    while(n > size()) {
        push_back(t);
    }
    while(n < size()) {
        pop_back();
    }
}

template <typename T>
void
List<T>::resize(const size_type n)
{
    while(n > size()) {
        push_back(T());
    }
    while(n < size()) {
        pop_back();
    }
}

template <typename T>
void
List<T>::clear()
{
    erase(begin(), end());
}

template <typename T>
typename List<T>::iterator
List<T>::begin()
{
    return iterator(begin_);
}

template <typename T>
typename List<T>::const_iterator
List<T>::begin() const
{
    return const_iterator(begin_);
}

template <typename T>
typename List<T>::iterator
List<T>::end()
{
    return iterator(NULL);
}

template <typename T>
typename List<T>::const_iterator
List<T>::end() const
{
    return const_iterator(NULL);
}

template <typename T>
typename List<T>::reverse_iterator
List<T>::rbegin()
{
    return reverse_iterator(end_);
}

template <typename T>
typename List<T>::const_reverse_iterator
List<T>::rbegin() const
{
    return const_reverse_iterator(end_);
}

template <typename T>
typename List<T>::reverse_iterator
List<T>::rend()
{
    return reverse_iterator(begin_->prev_);
}

template <typename T>
typename List<T>::const_reverse_iterator
List<T>::rend() const
{
    return const_reverse_iterator(begin_->prev_);
}

template <typename T>
typename List<T>::value_type&
List<T>::front()
{
    return begin_->data_;
}

template <typename T>
const
typename List<T>::value_type&
List<T>::front() const
{
    return begin_->data_;
}

template <typename T>
typename List<T>::value_type&
List<T>::back()
{
    return end_->data_;
}

template <typename T>
const
typename List<T>::value_type&
List<T>::back() const
{
    return end_->data_;
}

template <typename T>
List<T>::const_iterator::const_iterator() : ptr_(NULL)
{
}

template <typename T>
List<T>::const_iterator::const_iterator(const List<T>::const_iterator& rhv) 
    : ptr_(rhv.ptr_)
{
}

template <typename T>
List<T>::const_iterator::~const_iterator()
{
    if (ptr_ != NULL) {
        ptr_ = NULL;
    }
}

template <typename T>
List<T>::const_iterator::const_iterator(List<T>::Node* ptr) 
    :ptr_(ptr)
{
}

template <typename T>
const 
typename List<T>::const_iterator&
List<T>::const_iterator::operator=(const List<T>::const_iterator& rhv)
{
    ptr_ = rhv.ptr_;
    return *this;
}

template <typename T>
typename List<T>::Node*
List<T>::const_iterator::getPtr() const
{
    return ptr_;    
}

template <typename T>
const
typename List<T>::value_type&
List<T>::const_iterator::operator*() const
{
    return ptr_->data_;
}

template <typename T>
const
typename List<T>::value_type*
List<T>::const_iterator::operator->() const
{
    return ptr_->data_;
}

template <typename T>
typename List<T>::const_iterator
List<T>::const_iterator::operator++()
{
    ptr_ = ptr_-> next_;
    return ptr_;
}

template <typename T>
typename List<T>::const_iterator
List<T>::const_iterator::operator++(int)
{
    Node* temp = ptr_;
    ptr_ = ptr_-> next_;
    return temp;
}

template <typename T>
typename List<T>::const_iterator
List<T>::const_iterator::operator--()
{
    ptr_ = ptr_-> prev_;
    return ptr_;
}

template <typename T>
typename List<T>::const_iterator
List<T>::const_iterator::operator--(int)
{
    Node* temp = ptr_;
    ptr_ = ptr_-> prev_;
    return temp;
}

template <typename T>
bool
List<T>::const_iterator::operator==(const List<T>::const_iterator& rhv) const
{
    return (this->ptr_ == rhv.ptr_);
}

template <typename T>
bool
List<T>::const_iterator::operator!=(const List<T>::const_iterator& rhv) const
{
    return !(this->ptr_ == rhv.ptr_);
}

template <typename T>
List<T>::iterator::iterator() : const_iterator()
{
}

template <typename T>
List<T>::iterator::iterator(List<T>::Node* ptr) : const_iterator(ptr)
{
}

template <typename T>
typename List<T>::value_type&
List<T>::iterator::operator*() const
{
    return this-> ptr_-> data_;
}

template <typename T>
typename List<T>::value_type*
List<T>::iterator::operator->() const
{
    return &(this-> ptr_-> data_);
}

template <typename T>
typename List<T>::iterator
List<T>::iterator::operator++()
{
    this->ptr_ = this->ptr_->next_;
    return this->ptr_;
}

template <typename T>
typename List<T>::iterator
List<T>::iterator::operator++(int)
{
    Node* temp = this->getPtr();
    temp = temp-> next_;
    return this->getPtr();
}

template <typename T>
typename List<T>::iterator
List<T>::iterator::operator--()
{
    iterator temp = this->getPtr();
    temp = temp->prev_;
    return temp;
}

template <typename T>
typename List<T>::iterator
List<T>::iterator::operator--(int)
{
    iterator temp = this->getPtr();
    temp = temp-> prev_;
    return this->getPtr();
}

template <typename T>
List<T>::const_reverse_iterator::const_reverse_iterator() : ptr_(NULL)
{
}

template <typename T>
List<T>::const_reverse_iterator::const_reverse_iterator(const List<T>::const_reverse_iterator& rhv) 
    : ptr_(rhv.ptr_)
{
}

template <typename T>
List<T>::const_reverse_iterator::~const_reverse_iterator()
{
    if (ptr_ != NULL) {
        ptr_ = NULL;
    }
}

template <typename T>
List<T>::const_reverse_iterator::const_reverse_iterator(List<T>::Node* ptr) 
    :ptr_(ptr)
{
}

template <typename T>
const 
typename List<T>::const_reverse_iterator&
List<T>::const_reverse_iterator::operator=(const List<T>::const_reverse_iterator& rhv)
{
    ptr_ = rhv.ptr_;
}

template <typename T>
typename List<T>::Node
List<T>::const_reverse_iterator::getPtr() const
{
    return ptr_;    
}

template <typename T>
const
typename List<T>::value_type&
List<T>::const_reverse_iterator::operator*() const
{
    return ptr_->data_;
}

template <typename T>
const
typename List<T>::value_type*
List<T>::const_reverse_iterator::operator->() const
{
    return ptr_->data_;
}

template <typename T>
typename List<T>::const_reverse_iterator
List<T>::const_reverse_iterator::operator++()
{
    ptr_ = ptr_-> prev_;
    return ptr_;
}

template <typename T>
typename List<T>::const_reverse_iterator
List<T>::const_reverse_iterator::operator++(int)
{
    Node* temp = ptr_;
    ptr_ = ptr_-> prev_;
    return temp;
}

template <typename T>
typename List<T>::const_reverse_iterator
List<T>::const_reverse_iterator::operator--()
{
    ptr_ = ptr_-> next_;
    return ptr_;
}

template <typename T>
typename List<T>::const_reverse_iterator
List<T>::const_reverse_iterator::operator--(int)
{
    Node* temp = ptr_;
    ptr_ = ptr_-> next_;
    return temp;
}

template <typename T>
bool
List<T>::const_reverse_iterator::operator==(const List<T>::const_reverse_iterator& rhv) const
{
    return (this->ptr_ == rhv.ptr_);
}

template <typename T>
bool
List<T>::const_reverse_iterator::operator!=(const List<T>::const_reverse_iterator& rhv) const
{
    return !(this->ptr_ == rhv.ptr_);
}

template <typename T>
List<T>::reverse_iterator::reverse_iterator() : const_reverse_iterator()
{
}

template <typename T>
List<T>::reverse_iterator::reverse_iterator(List<T>::Node* ptr) : const_reverse_iterator(ptr)
{
}

template <typename T>
typename List<T>::value_type&
List<T>::reverse_iterator::operator*() const
{
    return this-> ptr_-> data_;
}

template <typename T>
typename List<T>::value_type*
List<T>::reverse_iterator::operator->() const
{
    return &(this-> ptr_-> data_);
}

template <typename T>
typename List<T>::reverse_iterator
List<T>::reverse_iterator::operator++()
{
    Node* ptr = this->getPtr();
    ptr = ptr-> prev_;
    return ptr;
}

template <typename T>
typename List<T>::reverse_iterator
List<T>::reverse_iterator::operator++(int)
{
    Node* temp = this->getPtr();
    temp = temp-> prev_;
    return this->getPtr();
}

template <typename T>
typename List<T>::reverse_iterator
List<T>::reverse_iterator::operator--()
{
    iterator temp = this->getPtr();
    temp = temp->next_;
    return temp;
}

template <typename T>
typename List<T>::reverse_iterator
List<T>::reverse_iterator::operator--(int)
{
    iterator temp = this->getPtr();
    temp = temp-> next_;
    return this->getPtr();
}

template <typename T>
typename List<T>::iterator
List<T>::insert(iterator pos, const T& x)
{
    Node* p = pos.getPtr();
    if (NULL == p || NULL == p->prev_) {
        push_front(x);
        return begin();
    }
    Node* temp = new Node (x, p, p->prev_);
    p->prev_->next_ = temp;
    return iterator(p->prev_);
}

template <typename T>
typename List<T>::iterator
List<T>::insert_after(iterator pos)
{
    Node* temp = pos.getPtr();
    temp->next_ = new Node(T(), temp->next_, temp);
    return ++pos;
}

template <typename T>
typename List<T>::iterator
List<T>::insert_after(iterator pos, const T& x)
{
    Node* temp = pos.getPtr();
    temp->next_ = new Node(x, temp->next_, temp);
    return ++pos;
}

template <typename T>
void
List<T>::insert(iterator pos, const int n, const T& x)
{
    for (int i = 0; i < n; ++i) {
        insert(pos, x);
    }
}

template <typename T>
template <typename InputIterator>
void 
List<T>::insert(iterator pos, InputIterator f, InputIterator l) 
{
    while (f != l) {
        pos = insert(pos, *f++);
    }
}

template <typename T>
void
List<T>::insert_after(iterator pos, const int n, const T& x)
{
    for (int i = 0; i < n; ++i) {
        pos = insert_after(pos, x);
    }
}

template <typename T>
template <typename InputIterator>
void 
List<T>::insert_after(iterator pos, InputIterator f, InputIterator l) 
{
    while (f != l) {
        pos = insert_after(pos, *f++);
    }
}

template <typename T>
typename List<T>::iterator
List<T>::erase(iterator pos)
{
    Node* temp = pos.getPtr();
    assert(temp != NULL);
    Node* prevPos = temp->prev_;
    Node* nextPos = temp->next_;
    (prevPos == NULL ? begin_ : prevPos->next_) = nextPos;
    (end_    == NULL ? begin_ : end_->next_) = NULL;
    (nextPos == NULL ? end_   : nextPos->prev_) = prevPos;
    (begin_  == NULL ? end_   : begin_->prev_) = NULL;
    delete temp;
    return iterator(nextPos);
}

template <typename T>
typename List<T>::iterator
List<T>::erase(iterator f, iterator l)
{
    while (f != l) {
        f = erase(f);
    }
    return l;
}
