#ifndef __T_SINGLE_LIST_HPP__
#define __T_SINGLE_LIST_HPP__

#include <iostream>
#include <cstddef>

template <typename T> 
class SingleList
{
private:
    struct Node {
        Node() {};
        Node(const T& d, Node* next = NULL) : data_(d) , next_(next) {};
        T data_;
        Node* next_;
    };

public:
    template <typename U>
    friend std::ostream& operator<<(std::ostream& out, const SingleList<U>& rhv);
    typedef T value_type;
    typedef value_type& reference;
    typedef const value_type& const_reference;
    typedef value_type* pointer;
    typedef std::ptrdiff_t difference_type;
    typedef std::size_t size_type;

    SingleList();
    SingleList(const int n);
    SingleList(const int n, const T& t);
    SingleList(const size_t n, const T& t);
    SingleList(const SingleList& rhv);
    template <typename InputIterator>
    SingleList(InputIterator f, InputIterator l);
    ~SingleList();
    const SingleList& operator=(const SingleList& rhv);
    size_type size() const;
    bool empty() const;
    bool operator==(const SingleList& rhv) const;
    bool operator!=(const SingleList& rhv) const;
    bool operator<(const SingleList& rhv) const;
    bool operator<=(const SingleList& rhv) const;
    bool operator>(const SingleList& rhv) const;
    bool operator>=(const SingleList& rhv) const;

    void clear();
    void push_front(const T& t = T());
    void push_back(const T& t = T());
    void pop_front();
    void pop_back();
    void resize(const size_t n, const T& t = T()); 

    class const_iterator {
            friend SingleList<T>; 
    public:
            const_iterator();
            const_iterator(const const_iterator& rhv);
            const_iterator(Node* ptr);
            ~const_iterator();
            const const_iterator& operator=(const const_iterator& rhv);
            const value_type& operator*() const;
            const value_type* operator->() const;
            Node* getPtr() const;
            const_iterator operator++();
            const_iterator operator++(int);
            bool operator==(const const_iterator& rhv) const;
            bool operator!=(const const_iterator& rhv) const;
    private:
            Node* ptr_;
    };
    class iterator : public const_iterator { 
        friend SingleList<T>;
    public:
            iterator();
            iterator(Node* ptr);
            value_type& operator*() const;
            value_type* operator->() const;
            iterator operator++();
            iterator operator++(int);
    };

    iterator begin();
    iterator end();
    const_iterator begin() const;
    const_iterator end() const;

    iterator insert(iterator position, const T& x);
    void insert(iterator position, const size_t n, const T& x);
    template <typename InputIterator>
    void insert(iterator position, InputIterator first, InputIterator laslt);
    iterator insert_after(iterator positon);
    iterator insert_after(iterator position, const T& x);
    void insert_after(iterator positon, const int  n, const T& x);
    template <typename InputIterator>
    void insert_after(iterator positon, InputIterator firest, InputIterator last);
    iterator erase_after(iterator pos);
    iterator erase(iterator pos);
    iterator erase(iterator before_first, iterator last);
    iterator erase_after(iterator before_first, iterator last);
private:
    Node* begin_;
    Node* end_;
};

template <typename T>   
std::ostream& operator<<(std::ostream& out, const SingleList<T>& rhv)
{
    typename SingleList<T>::Node* temp = rhv.begin_; 
    while(temp != NULL) {
        out << temp->data_ << " ";
        temp = temp-> next_;
    }
    return out;
};

#include <templates/SingleList.cpp>

#endif /// __T_SINGLE_LIST_HPP__
