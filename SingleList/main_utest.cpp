#include "headers/SingleList.hpp"

#include <iostream>
#include <gtest/gtest.h>

TEST(SingleList, FirstTest)
{
    SingleList<int> single;   
    single.push_front(1);
    single.push_front(1);
    SingleList<int>::iterator it = single.begin();
    single.insert_after(it, 6);
    std::stringstream out;
    out << single;
    EXPECT_EQ(out.str(), "1 6 1 ");
}

TEST(SingleList, insertBefore)
{
    SingleList<int> single;   
    single.push_front(1);
    single.push_front(1);
    SingleList<int>::iterator it = single.begin();
    single.insert(it, 6);
    std::stringstream out;
    out << single;
    EXPECT_EQ(out.str(), "6 1 1 ");
}

TEST(SingleList, insertRange)
{
    SingleList<int> single;  
    int arr[] = {7, 8, 9, 10, 11}; 
    SingleList<int>::iterator it = single.begin();
    single.insert(it, arr, arr + 4);
    std::stringstream out;
    out << single;
    EXPECT_EQ(out.str(), "10 9 8 7 ");
}

TEST(SingleList, insert_after_Range)
{
    SingleList<int> single;  
    int arr[] = {7, 8, 9, 10, 11}; 
    SingleList<int>::iterator it = single.begin();
    single.insert_after(it, arr, arr + 5);
    std::stringstream out;
    out << single;
    EXPECT_EQ(out.str(), "7 8 9 10 11 ");
}

TEST(SingleList, pop_back)
{
    SingleList<int> single;   
    single.push_front(1);
    single.push_front(1);
    single.push_front(1);
    single.pop_back();
    std::stringstream out;
    out << single;
    EXPECT_EQ(out.str(), "1 1 ");
}

TEST(SingleList, empty)
{
    SingleList<int> single;   
    EXPECT_TRUE(single.empty());
}

TEST(SingleList, equalLists)
{
    SingleList<int> single;   
    single.push_front(1);
    single.push_front(1);
    SingleList<int> single2;   
    single2.push_front(1);
    single2.push_front(1);
    EXPECT_TRUE(single2 == single);
}

TEST(SingleList, NOTequalLists)
{
    SingleList<int> single;   
    single.push_front(1);
    single.push_front(1);
    SingleList<int> single2;   
    single2.push_front(1);
    single2.push_front(1);
    EXPECT_FALSE(single2 != single);
}

TEST(SingleList, smallestLists)
{
    SingleList<int> single;   
    single.push_front(1);
    single.push_front(1);
    SingleList<int> single2;   
    single2.push_front(2);
    single2.push_front(2);
    EXPECT_TRUE(single < single2);
}

TEST(SingleList, largestLists)
{
    SingleList<int> single;   
    single.push_front(1);
    single.push_front(1);
    SingleList<int> single2;   
    single2.push_front(2);
    single2.push_front(2);
    EXPECT_TRUE(single2 > single);
}

TEST(SingleList, smallestiOrEqualLists)
{
    SingleList<int> single;   
    single.push_front(1);
    single.push_front(1);
    SingleList<int> single2;   
    single2.push_front(1);
    single2.push_front(2);
    EXPECT_TRUE(single <= single2);
}

TEST(SingleList, largestOrEqualLists)
{
    SingleList<int> single;   
    single.push_front(1);
    single.push_front(1);
    SingleList<int> single2;   
    single2.push_front(1);
    single2.push_front(2);
    EXPECT_TRUE(single2 >= single);
}

TEST(SingleList, sizeConstructor)
{
    const int SIZE = 5;
    SingleList<int> single(SIZE);   
}

TEST(SingleList, sizeAndValueConstructor)
{
    const int SIZE = 5;
    const int value = 1;
    SingleList<int> single(SIZE, value);
    single.clear();
    EXPECT_TRUE(single.empty());   
}

TEST(SingleList, CopyConstructor)
{
    const int SIZE = 5;
    int arr[SIZE] = {1, 2, 3, 4, 5};
    SingleList<int> single(arr, arr + 3);
    SingleList<int> single2(single);
    std::stringstream out1;
    out1 << single;

    std::stringstream out;
    out << single2;
    EXPECT_EQ(out.str(), "1 2 3 "); 
    EXPECT_EQ(single2.size(), 3);  
}

TEST(SingleList, Operator)
{
    int array [] = {1, 2, 3, 4, 5, 6};   
    int* begin = array;
    int* end = array + 4; 
    SingleList<int> single2(begin, end);     
    EXPECT_EQ(single2.size(), 4);  
}

TEST(SingleList, push_backAndPushFront)
{
    SingleList<int> single;   
    single.push_front(1);
    single.push_front(1);
    single.push_back(1);
    single.push_back(3);
    single.push_back(4);
    std::stringstream out;
    out << single;
    EXPECT_EQ(out.str(), "1 1 1 3 4 ");
    EXPECT_EQ(single.size(), 5);
}

TEST(SingleList, push_frontAndBack)
{
    SingleList<int> single;   
    single.push_front(6);
    single.push_front(8);
    single.push_front(9);
    single.push_front(10);
    single.push_front(11);
    std::stringstream out;
    out << single;
    EXPECT_EQ(out.str(), "11 10 9 8 6 ");
}

TEST(SingleList, push_back)
{
    SingleList<int> single;   
    single.push_back(6);
    single.push_back(7);
    single.push_back(8);
    single.push_back(9);
    single.push_back(10);
    std::stringstream out;
    out << single;
    EXPECT_EQ(out.str(), "6 7 8 9 10 ");
}

struct A 
{
    A(){ std::cout << "A::A()\n";};
    A(const A&) { std::cout << "A::A(const A&)\n";};
    ~A() { std::cout <<"A::~A()\n";};
};

TEST(SingleListA, clear)
{
    SingleList<A> single;   
    single.push_front(A());
    single.push_front(A());
    single.push_front(A());
    single.clear();
}

TEST(SingleList, push_back_clear)
{
    SingleList<int> single;   
    single.push_back(6);
    single.push_back(7);
    single.push_back(8);
    single.push_back(9);
    single.push_back(10);
    single.clear();
    EXPECT_EQ(single.size(), 0);
}

TEST(SingleListInt, pop_back_two_elements)
{
    SingleList<int> single;  
    single.push_back(1);
    single.push_back(1);
    single.push_back(1);
    single.pop_back();
    single.pop_back();
    single.pop_back();
    std::stringstream out;
    out << single;
    EXPECT_EQ(single.size(), 0);
    EXPECT_EQ(out.str(), "");
    EXPECT_TRUE(single.empty());
}

TEST(SingleListInt, SizeConstructor)
{
    SingleList<int> single(2);  
    single.push_back(1);
    std::stringstream out;
    out << single;
    EXPECT_EQ(out.str(), "0 0 1 ");
    EXPECT_EQ(single.size(), 3);
}

TEST(SingleList, Veragrum)
{
    SingleList<int> single1, single2;
    single1.push_front(6);
    single2.push_front(7);
    single2 = single1;   
    std::stringstream out;
    out << single2;
    EXPECT_EQ(out.str(), "6 ");
}

TEST(SingleList, erase)
{
    SingleList<int> s;
    s.push_front(6);
    s.push_front(7);
    s.push_front(8);
    s.push_front(9);
    SingleList<int>::iterator it = s.begin(); 
    while (it != s.end()) {
        it = s.erase_after(it);
    } 
    std::stringstream out;
    out << s;
    EXPECT_EQ(out.str(), "9 ");
}

TEST(SingleList, eraseRange)
{
    SingleList<int> s;
    s.push_front(6);
    s.push_front(7);
    s.push_front(8);
    s.push_front(9);
    SingleList<int>::iterator it = s.begin();
    SingleList<int>::iterator jt = s.end();
    s.erase_after(it, jt);
    std::stringstream out;
    out << s;
    EXPECT_EQ(out.str(), "");
}

TEST(SingleList, IteratorBegin)
{
    SingleList<int> single1;
    single1.push_front(6);
    SingleList<int>::iterator it = single1.begin();
    EXPECT_EQ(*it, 6);
}

TEST(SingleList, IteratorEnd)
{
    SingleList<int> single1;
    single1.push_front(6);
    SingleList<int>::iterator it = single1.end();
}

TEST(SingleList, resize)
{
    SingleList<int> list;
    for (int i = 0; i < 10; ++i) {
        list.push_back(i);
    }
    list.resize(5);
    std::stringstream out;
    out << list;
    EXPECT_EQ(out.str(), "0 1 2 3 4 ");
}

TEST(SingleList, resizeLargest) 
{
    SingleList<int> list;
    for (int i = 0; i < 10; ++i) {
        list.push_back(i);
    }
    list.resize(15);
    std::stringstream out;
    out << list;
    EXPECT_EQ(out.str(), "0 1 2 3 4 5 6 7 8 9 0 0 0 0 0 ");
}

TEST(SingleList, erase_befor)
{
    SingleList<int> s;
    s.push_front(6);
    s.push_front(7);
    s.push_front(8);
    s.push_front(9);
    SingleList<int>::iterator it = ++s.begin(); 
    s.erase(it);
    std::stringstream out;
    out << s;
    EXPECT_EQ(out.str(), "9 7 6 ");
}

TEST(SingleList, erase_befor_Range)
{
    SingleList<int> s;
    s.push_front(6);
    s.push_front(7);
    s.push_front(8);
    s.push_front(9);
    SingleList<int>::iterator it = s.begin(); 
    SingleList<int>::iterator jt = s.end(); 
    s.erase(it, jt);
    std::stringstream out;
    out << s;
    EXPECT_EQ(out.str(), "");
    EXPECT_TRUE(s.empty());
    EXPECT_EQ(s.size(), 0);
}

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

