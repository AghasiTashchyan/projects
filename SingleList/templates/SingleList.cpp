#include "headers/SingleList.hpp"

#include <iostream>
#include <cstddef>
#include <cassert>

template <typename T>
bool
SingleList<T>::operator==(const SingleList<T>& rhv) const
{
    Node* checkBegin = begin_;
    for ( Node* temp = rhv.begin_; temp != NULL; temp = temp-> next_) {
        if (checkBegin-> data_ != temp-> data_) {
            return false;
        }
        checkBegin = checkBegin-> next_;
    }
    return true;
}

template <typename T>
bool
SingleList<T>::operator!=(const SingleList<T>& rhv) const
{
    return !(*this == rhv);
}

template <typename T>
bool
SingleList<T>::operator<(const SingleList<T>& rhv) const
{
    Node* checkBegin = begin_;
    for ( Node* temp = rhv.begin_; temp != NULL; temp = temp-> next_) {
        if (checkBegin-> data_ > temp-> data_) {
            return false;
        }
        checkBegin = checkBegin-> next_;
    }
    return true;
}

template <typename T>
bool
SingleList<T>::operator<=(const SingleList<T>& rhv) const
{
    return !(*this > rhv);
}

template <typename T>
bool
SingleList<T>::operator>=(const SingleList<T>& rhv) const
{
    return !(*this < rhv);
}

template <typename T>
bool
SingleList<T>::operator>(const SingleList<T>& rhv) const
{
    return !(*this < rhv);
}

template <typename T>
SingleList<T>::SingleList()
    : begin_(NULL)
    , end_(NULL)
{
}

template <typename T>
SingleList<T>::SingleList(const int n)
    : begin_(NULL)
    , end_(NULL)
{
    resize(n);
}

template <typename T>
SingleList<T>::SingleList(const int n, const T& t)
    : begin_(NULL)
    , end_(NULL)
{
    resize(n, t);
}

template <typename T>
SingleList<T>::SingleList(const size_t n, const T& t)
    : begin_(NULL)
    , end_(NULL)
{
    resize(n, t);
}

template <typename T> 
SingleList<T>::SingleList(const SingleList<T>& rhv)
    : begin_(NULL)
    , end_(NULL)
{
    for (const_iterator it = rhv.begin(); it != rhv.end(); ++it) {
        push_back(*it);
    }
}

template <typename T>
template <typename InputIterator>
SingleList<T>::SingleList(InputIterator first, InputIterator last)
    : begin_(NULL)
    , end_(NULL)
{      
    for (; first != last; ++first) {
        push_back(*(first));
    }
}

template <typename T>
const SingleList<T>&
SingleList<T>::operator=(const SingleList<T>& rhv)
{
    if (!this-> empty()) {
        this-> clear();
    }
    for (const_iterator it = rhv.begin(); it != rhv.end(); ++it) {
        this-> push_back(*it);
    }
    return *this;
}

template <typename T>
SingleList<T>::~SingleList()
{
    Node* temp = begin_;
    while (temp != NULL) {
        Node* next = temp->next_;
        delete temp;
        temp = next;
    }
}

template <typename T>
typename SingleList<T>::size_type
SingleList<T>::size() const
{
    size_t size_ = 0;
    for (const_iterator it = begin(); it != end(); ++it) {
        ++size_;
    }
    return size_;
}

template <typename T>
void
SingleList<T>::push_front(const T& t)
{
    begin_ = new Node(t, begin_);
    if (NULL == end_) {
        end_ = begin_;
    }
}

template <typename T>
void
SingleList<T>::push_back(const T& t)
{
    end_ = new Node(t, end_);
    (NULL == begin_ ? begin_ : end_->next_->next_) = end_;
    end_->next_ = NULL;
}

template <typename T>
void
SingleList<T>::pop_front()
{
    if (begin_ != NULL) {
        Node* temp = begin_;
        begin_ = begin_-> next_;
        delete temp;
    }
}

template <typename T>
void
SingleList<T>::pop_back()
{
    assert(begin_ != NULL);
    Node* temp = begin_;
    while (temp != NULL && temp->next_ != end_) {
        temp = temp->next_;
    }
    delete end_;
    end_ = temp;
    (end_ != NULL ? end_->next_ : begin_) = NULL; 
}

template <typename T>
void
SingleList<T>::resize(const typename SingleList<T>::size_type n, const T& t)
{ 
    while(size() > n) {
        pop_back();
    }
    while(size() < n) {
        push_back(t);
    }
}

template <typename T>
bool
SingleList<T>::empty() const
{
    return begin_ == NULL;
}

template <typename T>
void
SingleList<T>::clear()
{
    while (begin_ != NULL) {
        Node* temp = begin_;
        begin_ = begin_-> next_;
        delete temp;
    }
    begin_ = NULL;
    end_ = NULL;
}

template <typename T>
SingleList<T>::const_iterator::const_iterator() : ptr_(NULL)
{
}

template <typename T>
SingleList<T>::const_iterator::const_iterator(const SingleList<T>::const_iterator& rhv) 
    : ptr_(rhv.ptr_)
{
}

template <typename T>
SingleList<T>::const_iterator::~const_iterator()
{
    if (ptr_ != NULL) {
        ptr_ = NULL;
    }
}

template <typename T>
SingleList<T>::const_iterator::const_iterator(SingleList<T>::Node* ptr) 
    :ptr_(ptr)
{
}

template <typename T>
const 
typename SingleList<T>::const_iterator&
SingleList<T>::const_iterator::operator=(const SingleList<T>::const_iterator& rhv)
{
    ptr_ = rhv.ptr_;
    return *this;
}

template <typename T>
typename SingleList<T>::Node*
SingleList<T>::const_iterator::getPtr() const
{
    return ptr_;    
}

template <typename T>
const
typename SingleList<T>::value_type&
SingleList<T>::const_iterator::operator*() const
{
    return ptr_->data_;
}

template <typename T>
const
typename SingleList<T>::value_type*
SingleList<T>::const_iterator::operator->() const
{
    return ptr_->data_;
}

template <typename T>
typename SingleList<T>::const_iterator
SingleList<T>::const_iterator::operator++()
{
    ptr_ = ptr_-> next_;
    return ptr_;
}

template <typename T>
typename SingleList<T>::const_iterator
SingleList<T>::const_iterator::operator++(int)
{
    Node* temp = ptr_;
    ptr_ = ptr_-> next_;
    return temp;
}

template <typename T>
bool
SingleList<T>::const_iterator::operator==(const SingleList<T>::const_iterator& rhv) const
{
    return (this->ptr_ == rhv.ptr_);
}

template <typename T>
bool
SingleList<T>::const_iterator::operator!=(const SingleList<T>::const_iterator& rhv) const
{
    return !(this->ptr_ == rhv.ptr_);
}

template <typename T>
SingleList<T>::iterator::iterator() : const_iterator()
{
}

template <typename T>
SingleList<T>::iterator::iterator(SingleList<T>::Node* ptr) : const_iterator(ptr)
{
}

template <typename T>
typename SingleList<T>::value_type&
SingleList<T>::iterator::operator*() const
{
    return this-> ptr_-> data_;
}

template <typename T>
typename SingleList<T>::value_type*
SingleList<T>::iterator::operator->() const
{
    return &(this-> ptr_-> data_);
}

template <typename T>
typename SingleList<T>::iterator
SingleList<T>::iterator::operator++()
{
    this->ptr_ = this->ptr_-> next_;
    return *this;
}

template <typename T>
typename SingleList<T>::iterator
SingleList<T>::iterator::operator++(int)
{
    Node* temp = this->getPtr();
    temp = temp-> next_;
    return iterator(temp);
}

template <typename T>
typename SingleList<T>::iterator
SingleList<T>::begin()
{
    return iterator(begin_);
}

template <typename T>
typename SingleList<T>::const_iterator
SingleList<T>::begin() const
{
    return const_iterator(begin_);
}

template <typename T>
typename SingleList<T>::iterator
SingleList<T>::end()
{
    return iterator(NULL);
}

template <typename T>
typename SingleList<T>::const_iterator
SingleList<T>::end() const
{
    return const_iterator(NULL);
}

template <typename T>
typename SingleList<T>::iterator
SingleList<T>::insert(iterator position, const T& x)
{
    insert_after(position, x);
    Node* temp = position.getPtr();
    std::swap(temp->data_, temp->next_->data_);
    return position;
}

template <typename T>
typename SingleList<T>::iterator
SingleList<T>::insert_after(iterator position)
{
    Node* temp = position.getPtr();
    temp->next_ = new Node(T(), temp->next_);
    return position;
}

template <typename T>
typename SingleList<T>::iterator
SingleList<T>::insert_after(iterator position, const T& x)
{
    Node* temp = position.getPtr();
    temp->next_ = new Node(x, temp->next_);
    return position;
}

template <typename T>
void
SingleList<T>::insert(iterator position, const size_t n, const T& x)
{
    for (int i = 0; i < n; ++i) {
        insert(position, x);
    }
}

template <typename T>
template <typename InputIterator>
void
SingleList<T>::insert(iterator position, InputIterator first, InputIterator last)
{
    if (empty()) {
        while (first != last) {
            push_front(*first++);
        }
    }
    while (first != last) {
        insert(position, *first++);
    }
}

template <typename T>
void
SingleList<T>::insert_after(iterator position, const int n, const T& x)
{
    for (int i = 0; i < n; ++i) {
        insert_after(position, x);
    }
}

template <typename T>
template <typename InputIterator>
void
SingleList<T>::insert_after(iterator position, InputIterator first, InputIterator last)
{
    if (empty()) {
        while (first != last) {
            push_back(*first++);
        }
    }
    while (first != last) {
        insert_after(position, *first++);
    }
}

template <typename T>
typename SingleList<T>::iterator
SingleList<T>::erase_after(iterator pos)
{
    Node* temp = pos.getPtr();
    if (temp == NULL || temp->next_ == NULL) {
        return end();
    }
    Node* next = temp->next_;
    temp->next_ = temp->next_->next_;
    delete next;
    return pos;
}

template <typename T>
typename SingleList<T>::iterator
SingleList<T>::erase(iterator pos)
{
    Node* temp = pos.getPtr();
    assert(temp != NULL);
    if (temp == begin_) {
        pop_front(); 
        return begin();
    }
    Node* first = begin_;
    while (first->next_ != temp) {
        first = first->next_;
    }
    first->next_ = temp->next_;
    delete temp;
    return pos;
}

template <typename T>
typename SingleList<T>::iterator
SingleList<T>::erase(iterator f, iterator l) 
{
    while (f != l) {
        f = erase(f);
    }
    return l;
}

template <typename T>
typename SingleList<T>::iterator
SingleList<T>::erase_after(iterator f, iterator l) 
{ 
    Node* first = f.getPtr();
    Node* last = l.getPtr();
    if (first == begin_) {
        begin_ = last;
    } else {
        while (first->next_ != last) {
            Node* temp = first->next_;
            first->next_ = temp->next_;
            delete temp;
        }
    }
    return l;
}

