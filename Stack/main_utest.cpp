#include "headers/Stack.hpp"
#include <iostream>
#include <gtest/gtest.h>

TEST(StackInt, Equal)
{
    Stack<int, std::vector<int> > s1;
    s1.push(1);
    s1.push(2);
    s1.push(3);
    Stack<int, std::vector<int> > s2;
    s2.push(1);
    s2.push(2);
    s2.push(3);
    const bool result = (s1 == s2); 
    EXPECT_TRUE(result);
}

TEST(StackInt, CopyConstructor)
{
    Stack<int, std::vector<int> > s1;
    s1.push(1);
    s1.push(2);
    s1.push(3);
    Stack<int, std::vector<int> > s2(s1);
    while(!s2.empty()) {
        std::cout << s2.top() << " ";
        s2.pop();
    }
}

TEST(StackInt, operatorAssign)
{
    Stack<int, std::vector<int> > s1;
    s1.push(1);
    s1.push(2);
    s1.push(3);
    Stack<int, std::vector<int> > s2;
    s2 = s1;
    while(!s2.empty()) {
        std::cout << s2.top() << " ";
        s2.pop();
    }
}

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

