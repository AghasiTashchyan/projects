#include "headers/Stack.hpp"

template <typename T, typename Sequence>
bool
Stack<T, Sequence>::operator==(const Stack& rhv)
{
    return static_cast<Sequence>(*this) == static_cast<Sequence>(rhv);
}

template <typename T, typename Sequence>
bool
Stack<T, Sequence>::operator<(const Stack& rhv)
{
    return static_cast<Sequence>(*this) < static_cast<Sequence>(rhv);
}

template <typename T, typename Sequence>
Stack<T, Sequence>::Stack()
    : Sequence()
{
}

template <typename T, typename Sequence>
Stack<T, Sequence>::Stack(const Stack& rhv)
    :Sequence(rhv)
{
}

template <typename T, typename Sequence>
const Stack<T, Sequence>&
Stack<T, Sequence>::operator=(const Stack& rhv)
{
    this->Sequence::operator=(rhv);
    return *this;    
} 

template <typename T, typename Sequence>
void
Stack<T, Sequence>::push(const T& d)
{
    Sequence::push_back(d);
}

template <typename T, typename Sequence>
void
Stack<T, Sequence>::pop()
{
    Sequence::pop_back();
}

template <typename T, typename Sequence>
T&
Stack<T, Sequence>::top()
{
    return Sequence::back();
}

template <typename T, typename Sequence>
const T&
Stack<T, Sequence>::top() const
{
    return Sequence::back();
}

template <typename T, typename Sequence>
bool
Stack<T, Sequence>::empty() const
{
    return Sequence::empty();
}

template <typename T, typename Sequence>
typename Stack<T, Sequence>::size_type
Stack<T, Sequence>::size() const
{
    return Sequence::size();
}

