#ifndef __STACK_HPP__
#define __STACK_HPP__

#include <vector>

template <typename T, typename Sequence = std::vector<T> >
class Stack : private Sequence
{
public:
    typedef std::size_t size_type;
    Stack();
    Stack(const Stack& rhv);
    const Stack& operator=(const Stack& rhv);
    void push(const T& d);
    void pop();
    T& top();
    const T& top() const;
    size_type size() const;
    bool empty() const;
    bool operator==(const Stack& rhv);
    bool operator<(const Stack& rhv);
};

#include "templates/Stack.cpp"

#endif ///__STACK_HPP__

