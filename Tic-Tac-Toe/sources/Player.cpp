#include "headers/Player.hpp"

Player::Player()
{
    simbul_ = '0';
}

Player::Player(const char simbul)
{
    setSimbul(simbul);
}

void
Player::setSimbul(const char simbul)
{
    simbul_ = simbul;
}

char
Player::getSimbul() const
{
    return simbul_;
}

