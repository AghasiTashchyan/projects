#include "headers/Board.hpp"
#include <iostream>

Board::Board()
{
    setBoard();
}

void
Board::setBoard()
{
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 3; ++j) {
        board_[i][j] = '.';
        }
    }
}

void
Board::printBoard()
{
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 3; ++j) {
            std::cout << board_[i][j] << " ";
        }
        std::cout << std::endl;
    }

}

void
Board::selectChoisInBoard(const Player& player, const int answer)
{
    const int row = (answer - 1) / 3;
    const int column = (answer- 1) % 3;
    if (board_[row][column]  == '.') {
        board_[row][column] = player.getSimbul();
    }
}

bool
Board::isWin()
{
    for (int x = 0; x < 3; ++x) {
        if ((board_[x][0] == board_[x][1] && board_[x][0] == board_[x][2]) && board_[x][0] != '.') {
            return true;
        }
    }
    for (int y = 0; y < 3; ++y) {
        if ((board_[0][y] == board_[1][y] && board_[0][y] == board_[2][y]) && board_[0][y] != '.' ) {
            return true;
        }
    }
    if ((board_[0][0] == board_[1][1] && board_[1][1] == board_[2][2]) && board_[1][1] != '.') {
        return true;
    }
    
    if ((board_[0][2] == board_[1][1] && board_[2][0] == board_[1][1]) && board_[1][1] != '.') {
        return true;
    }
    return false;
}

bool
Board::checkingBoard()
{
    for (int i = 0; i < 3; ++i) {
        for (int y = 0; y < 3; ++i) {
            if ('.' == board_[i][y]) {
                return true;
            }
        }
    }
    return false;
}

bool
Board::checkingChoice(const int choice)
{
    const int string = (choice - 1) / 3;
    const int block = (choice - 1) % 3;
    if (board_[string][block] == '.') {
        return true;
    }
    return false;
}
