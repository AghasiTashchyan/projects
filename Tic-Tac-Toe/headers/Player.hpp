#ifndef __PLAYER_HPP__
#define __PLAYER_HPP__

class Player
{
public:
    Player();
    Player(const char simbul);
    void setSimbul(const char simbul);
    char getSimbul() const;
private:
    char simbul_;
};

#endif /// __PLAYER_HPP__
