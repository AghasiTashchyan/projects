#ifndef __TICTACTOE_HPP__
#define __TICTACTOE_HPP__

#include "Board.hpp"
#include "Player.hpp"

class TicTacToe
{
public:
    TicTacToe(Board& board, Player& player1, Player& player2);
    int run();
    void printManue() const; 
    int getValue();
private:  
    Board& board_;
    Player& player1_;
    Player& player2_;
};

#endif /// __TICTACTOE_HPP__
