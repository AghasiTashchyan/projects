#include "headers/Vector.hpp"
#include <iostream>
#include <gtest/gtest.h>
/*
TEST(VectorIteratorTest, Iterator) 
{
    Vector<int> v;
    v.push_back(1);
    v.push_back(2);
    v.push_back(3);
    Vector<int>::iterator it = v.begin();
    v.insert(it, 6);
    std::stringstream out;
    out << v;
   EXPECT_EQ(out.str(), "6 1 2 3 "); 
}

TEST(VectorIteratorTest, Iterators) 
{
    Vector<int> v;
    v.push_back(1);
    v.push_back(2);
    v.push_back(3);
    Vector<int>::iterator first = v.begin();
    Vector<int>::iterator last = v.begin() + 2;
    Vector<int> b(first, last);
    Vector<int>::iterator it = b.begin();
    EXPECT_EQ(*it, 1);
}

TEST(VectorIteratorTest, IteratorPlusAdding) 
{
    Vector<int> v;
    v.push_back(1);
    v.push_back(2);
    v.push_back(3);
    Vector<int>::iterator it = v.begin();
    it += 1;
    EXPECT_EQ(*it, 2);
}

TEST(VectorIteratorTest, IteratorPlusing) 
{
    Vector<int> v;
    v.push_back(1);
    v.push_back(2);
    v.push_back(3);
    Vector<int>::iterator it = v.begin();
    EXPECT_EQ(*(it + 1), 2);
}

TEST(Vector, Resize)
{
    std::stringstream in("1 2 3 4");
    Vector<int> v; 
    in >> v;
    std::stringstream out;
    out << v;
    EXPECT_EQ(out.str(), "1 2 3 4 ");
}

TEST(Vector, OperatorSmallest)
{
    std::stringstream in("1 2 3 3");
    Vector<int> v; 
    in >> v;
    std::stringstream in2("1 2 3 4");
    Vector<int> v2; 
    in2 >> v2;
    const bool result = (v < v2);
    EXPECT_TRUE(result);
}

TEST(Vector, OperatorSmallestOrEqual)
{
    std::stringstream in("1 2 3 3");
    Vector<int> v; 
    in >> v;
    std::stringstream in2("1 2 3 4");
    Vector<int> v2; 
    in2 >> v2;
    const bool result = (v <= v2);
    EXPECT_TRUE(result);
}

TEST(Vector, OperatorLargestOrEqual)
{
    std::stringstream in("1 2 3 5");
    Vector<int> v; 
    in >> v;
    std::stringstream in2("1 2 3 4");
    Vector<int> v2; 
    in2 >> v2;
    const bool result = (v >= v2);
    EXPECT_TRUE(result);
}

TEST(Vector, OperatorLargest)
{
    std::stringstream in("1 2 3 5");
    Vector<int> v; 
    in >> v;
    std::stringstream in2("1 2 3 4");
    Vector<int> v2; 
    in2 >> v2;
    const bool result = (v > v2);
    EXPECT_TRUE(result);
}

TEST(VectorIteratorTest, Clear) 
{
    Vector<int> v;
    v.push_back(1);
    v.push_back(2);
    v.push_back(3);
    v.clear();
    EXPECT_EQ(v.size(), 0);
}

TEST(VectorConstructorTest, IteratorConstructor)
{
    int arr[] = {1, 2, 3, 4, 5};
    Vector<int> vec(arr, arr + 5);
}

TEST(VectorIteratorTest, Insert) 
{
    Vector<int> v;
    v.push_back(1);
    v.push_back(2);
    v.push_back(3);
    Vector<int>::iterator it = v.begin() + 2;
    v.insert(it, 6);
    std::stringstream out;
    out << v;
    EXPECT_EQ(out.str(), "1 2 6 3 ");
}

TEST(VectorInsertTest, InsertRange)
{
    Vector<int> vec;
    vec.push_back(0);
    vec.push_back(1);
    vec.push_back(5);
    vec.push_back(6);
    vec.push_back(7);
    vec.push_back(8);
    vec.push_back(9);
    int arr[3] = {2, 3, 4};
    Vector<int>::iterator pos = vec.begin();
    vec.insert(pos + 2 , arr, arr + 3);
    std::stringstream out;
    out << vec;
    EXPECT_EQ(out.str(), "0 1 2 3 4 5 6 7 8 9 ");
}

TEST(VectorInsertTest, InsertOtherRange)
{
    Vector<int> vec;
    vec.push_back(6);
    vec.push_back(7);
    vec.push_back(8);
    vec.push_back(9);
    int arr[3] = {2, 3, 4};
    Vector<int>::iterator pos = vec.begin();
    vec.insert(pos + 2 , arr, arr + 3);
    std::stringstream out;
    out << vec;
    EXPECT_EQ(out.str(), "6 7 2 3 4 8 9 ");
}

TEST(VectorInsertTest, InsertPosConutValue)
{
    Vector<int> vec;
    vec.push_back(6);
    vec.push_back(9);
    Vector<int>::iterator pos = vec.begin();
    vec.insert(pos, 2, 8);
    std::stringstream out;
    out << vec;
    EXPECT_EQ(out.str(), "8 8 6 9 ");
}

TEST(VectorInsertTest, erase) 
{
    Vector<int> vec;
    vec.push_back(1);
    vec.push_back(3);
    vec.push_back(5);
    vec.push_back(5);
    Vector<int>::iterator pos = vec.begin() + 1;
    vec.erase(pos);
    std::stringstream out;
    out << vec;
    EXPECT_EQ(out.str(), "1 5 5 ");
}

TEST(VectorInt, eraseRange) 
{
    Vector<int> vec;
    vec.push_back(1);
    vec.push_back(3);
    vec.push_back(8);
    vec.push_back(5);
    vec.push_back(6);
    vec.push_back(9);
    vec.push_back(10);
    vec.push_back(11);
    Vector<int>::iterator pos = vec.begin();
    Vector<int>::iterator pos2 = vec.begin() + 5;
    vec.erase(pos, pos2);
    std::stringstream out;
    out << vec;
    EXPECT_EQ(out.str(), "9 10 11 ");
}

TEST(VectorInt, constructor ) 
{
    Vector<int> vec;
    std::stringstream out;
    vec.push_back(1);
    vec.push_back(2);
    vec.push_back(3);
    vec.push_back(4);

    Vector<int> vec2;
    vec2.push_back(7);
    vec2 = vec;
    out << vec2;
    EXPECT_EQ(out.str(), "1 2 3 4 ");
}

TEST(VectorInt, SizeConstructor) 
{
    const int SIZE = 5;
    int value = 7;
    Vector<int> vec(SIZE, value);
    EXPECT_EQ(vec.size(), 5);
}

struct A {
    int x;
    A(){ std::cout << "A::A()" << std::endl;}
    A(const A&) {std::cout << "A::A(const A&)" << std::endl;}
    const A& operator=(const A&) { std::cout << "A::operator=(const A&)" << std::endl; return *this; }
    ~A() { std::cout << "A::~A()" << std::endl;}
};

TEST(VectorA, Reserve) 
{
    Vector<A> vec;
    vec.resize(3);
    EXPECT_EQ(vec.size(), 3);
}

TEST(VectorA, erase)
{
    Vector<A> vec;
    vec.resize(3);
    vec.clear();
    EXPECT_TRUE(vec.empty());
}

TEST(VectorA, eraseOneElement)
{
    Vector<A> vec;
    vec.resize(1);
    vec.erase(vec.begin());
    EXPECT_TRUE(vec.empty());
}

TEST(VectorA, Resize0)
{
    Vector<A> a(2);
    a.resize(0);
} 

TEST(VectorA, CopyConstructorOneElement)
{
    Vector<A> a(1);
    A z1;
    a.push_back(z1);
    Vector<A> b = a;
} 

TEST(VectorInt, CopyConstructor)
{
    Vector<int> a;
    a.push_back(8);
    Vector<int> b(a);
    std::stringstream out;
    out << b;
    a.push_back(10);
    EXPECT_EQ(out.str(), "8 ");
}

TEST(VectorA, CopyConstructorTwoElement)
{
    Vector<A> a(2, A());
    Vector<A> b(2, A());
    Vector<A> c = a;
}

TEST(VectroInt, insert_position_size)
{
    Vector<int> a;
    a.push_back(1);
    a.push_back(2);
    a.push_back(3);
    a.push_back(4);
    Vector<int>::iterator i = a.begin() + 1;
    a.insert(i, 2, 7);
    std::stringstream out;
    out << a;
    EXPECT_EQ(out.str(), "1 7 7 2 3 4 ");
}

TEST(VectorTest, SwapTest)
{
    Vector<int> v1(2,1);
    Vector<int> v2(2,2);
    Vector<int> v1_copy = v1;
    Vector<int> v2_copy = v2;
    v1.swap(v2);
    ASSERT_EQ(v1, v2_copy);
    ASSERT_EQ(v2, v1_copy);
}

TEST(VectorInt, revers_iterator)
{
    Vector<int> v;
    v.push_back(1);
    v.push_back(2);
    v.push_back(3);
    v.push_back(4);
    for (Vector<int>::reverse_iterator it = v.rbegin(); it != v.rend(); ++it) {
        std::cout << *it << std::endl;
    } 
}
*/
TEST(VectorInt, insertTest)
{
    Vector<int> v;

    v.push_back(1);
    v.push_back(2);
    v.push_back(3);
    v.push_back(4);
    v.push_back(5);

    Vector<int>::iterator it = v.end() - 1;

    v.insert(it, 10);
    for (Vector<int>::iterator it = v.begin(); it != v.end(); ++it) {
        std::cout << *it << " ";
    }
    std::cout << "\n";
    EXPECT_EQ(*it, 10);
    EXPECT_EQ(v[5], 5);
}



int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

