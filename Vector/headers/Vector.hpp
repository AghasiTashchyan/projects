#ifndef __T_VECTOR_HPP__
#define __T_VECTOR_HPP__

#include <iostream>

template <typename T> class Vector;
template <typename T> std::istream& operator>>(std::istream& in, Vector<T>& v);
template <typename T> std::ostream& operator<<(std::ostream& out, const Vector<T>& v);

template <typename T>
class Vector
{
public:
    typedef T value_type;
    typedef value_type& reference; 
    typedef const value_type& const_reference;
    typedef value_type* pointer; 
    typedef std::ptrdiff_t difference_type;
    typedef std::size_t size_type;

    class const_iterator {
        friend Vector<T>;
    public:
        const_iterator();
        const_iterator(const const_iterator& rhv);
        ~const_iterator();
        const const_iterator& operator=(const const_iterator& rhv);
        const value_type& operator*() const;
        const value_type* operator->() const;
        bool operator==(const const_iterator& rhv) const;
        bool operator!=(const const_iterator& rhv) const;
        bool operator<(const const_iterator& rhv) const;
        bool operator>(const const_iterator& rhv) const;
        bool operator>=(const const_iterator& rhv) const;
        bool operator<=(const const_iterator& rhv) const;
        const_iterator operator++();
        const_iterator operator--();
        const_iterator operator++(int);
        const_iterator operator--(int);
        const_iterator operator+=(const int value);
        const_iterator operator-=(const int value);
        difference_type operator+(const const_iterator& rhv) const;
        difference_type operator-(const const_iterator& rhv) const;
        const_iterator operator+(const int value) const;
        const_iterator operator-(const int value) const;
    protected:
        const_iterator(const pointer ptr);
        pointer getPtr() const;
    private: 
        pointer ptr_;
    };

    class iterator : public const_iterator { 
        friend Vector<T>;
    public:
        iterator(pointer ptr);
        iterator();
        reference operator*();
        value_type* operator->();
        reference operator[](const size_type size); 
        iterator operator++();
        iterator operator--();
        iterator operator++(int);
        iterator operator--(int);
        iterator operator+=(const int value);
        iterator operator-=(const int value);
        iterator operator+(const int value) const;
        iterator operator-(const int value) const;
        difference_type operator+(const iterator& rhv) const;
        difference_type operator-(const iterator& rhv) const;
    };

    class const_reverse_iterator {
        friend Vector<T>;
    public:
        const_reverse_iterator();
        const_reverse_iterator(const const_reverse_iterator& rhv);
        ~const_reverse_iterator();
        const const_reverse_iterator& operator=(const const_reverse_iterator& rhv);
        const value_type& operator*() const;
        const value_type* operator->() const;
        const_reverse_iterator operator++();
        const_reverse_iterator operator--();
        const_reverse_iterator operator++(int);
        const_reverse_iterator operator--(int);
        const_reverse_iterator operator+=(const int value);
        const_reverse_iterator operator-=(const int value);
        const_reverse_iterator operator+(const int value);
        const_reverse_iterator operator-(const int value);
        difference_type operator+(const const_reverse_iterator& rhv) const;
        difference_type operator-(const const_reverse_iterator& rhv) const;
        bool operator==(const const_reverse_iterator& rhv) const;
        bool operator!=(const const_reverse_iterator& rhv) const;
        bool operator<(const const_reverse_iterator& rhv) const;
        bool operator>(const const_reverse_iterator& rhv) const;
        bool operator>=(const const_reverse_iterator& rhv) const;
        bool operator<=(const const_reverse_iterator& rhv) const;
    protected:
        const_reverse_iterator(const pointer value);
        pointer getRptr() const;
    private:
        pointer ptr_;
    };

    class reverse_iterator: public const_reverse_iterator {
        friend Vector<T>;
    public:
        reverse_iterator(const pointer value);
        reference operator*();
        reference operator->();
        reverse_iterator operator++();
        reverse_iterator operator--();
        reverse_iterator operator++(int);
        reverse_iterator operator--(int);
        reverse_iterator operator+=(const int value);
        reverse_iterator operator-=(const int value);
        reverse_iterator operator+(const int value);
        reverse_iterator operator-(const int value);
        reference operator[](const size_type size);
        difference_type operator+(const reverse_iterator& rhv) const;
        difference_type operator-(const reverse_iterator& rhv) const;
    };

    const_iterator begin() const;
    iterator begin();
    const_iterator end() const;
    iterator end();
    iterator insert(iterator pos, const T& x);
    const_reverse_iterator rbegin() const;
    reverse_iterator rbegin();
    const_reverse_iterator rend() const;
    reverse_iterator rend();
    iterator erase(iterator pos);
    iterator erase(iterator first, iterator last); 
    Vector();
    Vector(const int size, const T& t);
    Vector(const size_t size, const T& t);
    Vector(const size_t size);
    Vector(const Vector& rhv);
    template <class InputIterator>
    Vector(InputIterator first, InputIterator last);
    ~Vector();
    const Vector& operator=(const Vector& rhv);
    void swap(Vector<T>& rhv);
    size_type size() const;
    size_type max_size() const;
    const T& get(const size_t index) const;
    void set(const size_t, const T& value);
    const T& at(const size_t index) const;
    T& at(const size_t index);
    const T& operator[](const size_type index) const;
    reference operator[](const size_type index);
    bool empty() const;
    bool operator==(const Vector<T>& rhv) const; 
    bool operator!=(const Vector<T>& rhv) const;
    bool operator<(const Vector<T>& rhv) const;
    bool operator<=(const Vector<T>& rhv) const;
    bool operator>(const Vector<T>& rhv) const;
    bool operator>=(const Vector<T>& rhv) const; 
    template <class InputIterator>
    void insert(iterator pos, InputIterator first, InputIterator last);
    void insert(iterator pos, const int n, const T& x);
    void resize(const size_type n);
    void resize(const size_type n, const T& init);
    void reserve(const size_type n);
    void push_back(const T& element);
    void pop_back();
    void clear();
    size_type capacity() const;
    const_reference back() const;
    reference back();
    reference front();
    const_reference front() const;
private:
    T* begin_;
    T* end_;
    T* bufferEnd_;
};

#include <templates/Vector.cpp>

#endif /// __T_VECTOR_HPP__
