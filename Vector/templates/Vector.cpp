#include "headers/Vector.hpp"

#include <cassert>
#include <cstddef>
#include <limits>
#include <cstring>
#include <cmath>

const double RESERVE_COEFF = 2;

template <typename T>
std::istream&
operator>>(std::istream& in, Vector<T>& v)
{
    int temp;
    while (in >> temp) {
        v.push_back(temp);
    }
    return in;
}

template <typename T>
std::ostream&
operator<<(std::ostream& out, const Vector<T>& v)
{
    for (size_t i = 0; i < v.size(); ++i) {
        out << v[i] << ' ';
    }
    return out;
}

template <typename T>
Vector<T>::Vector()
    : begin_(NULL)
    , end_(NULL)
    , bufferEnd_(NULL)
{
}

template <typename T>
Vector<T>::Vector(const int size, const T& t) 
    : begin_(NULL)
    , end_(NULL)
    , bufferEnd_(NULL)
{
    assert(size >= 0);
    resize(size, t);
}

template <typename T>
Vector<T>::Vector(const size_t size, const T& t) 
    : begin_(NULL)
    , end_(NULL)
    , bufferEnd_(NULL)
{
    resize(size, t);
}

template <typename T>
template <typename InputIterator>
Vector<T>::Vector(InputIterator first, InputIterator last)
    : begin_(NULL)
    , end_(NULL)
    , bufferEnd_(NULL)
{
    assert(first != last);
    while (first != last) {
        this->push_back(*first);
        ++first;
    }
}

template <typename T>
Vector<T>::Vector(const size_t size)
    : begin_(NULL)
    , end_(NULL)
    , bufferEnd_(NULL)
{
    resize(size); 
}

template <typename T>
Vector<T>::Vector(const Vector<T>& rhv)
    : begin_(NULL)
    , end_(NULL)
    , bufferEnd_(NULL)
{
    const int size = rhv.size();
    reserve(size);
    for (int i = 0; i < size; ++i) {
        new (end_++) T(rhv.begin_[i]);
    }
}

template <typename T>
Vector<T>::~Vector()
{
    if (begin_ != NULL) {
        clear();
        ::operator delete [](begin_);
        begin_ = NULL;
        end_ = NULL;
        bufferEnd_ = NULL;
    }
}

template <typename T>
void
Vector<T>::clear()
{
    erase(begin_, end_);
}

template <typename T>
const Vector<T>&
Vector<T>::operator=(const Vector<T>& rhv)
{
    if (this == &rhv) {
        return *this;
    }
    const size_t newSize = rhv.size();
    if(NULL != begin_) {
        clear();
    }
    reserve(newSize);
    for (size_t i = 0; i < newSize; ++i) {
        this-> push_back(rhv.begin_[i]);
    }
    return *this;
}

template <typename T>
void
Vector<T>::swap(Vector<T>& rhv)
{
    std::swap(begin_, rhv.begin_);
    std::swap(end_, rhv.end_);
    std::swap(bufferEnd_, rhv.bufferEnd_);
}

template <typename T>
const T&
Vector<T>::get(const size_t index) const
{
    assert(index < size());
    return begin_[index];
}

template <typename T>
void
Vector<T>::set(const size_t index, const T& value)
{
    assert(index < size());
    begin_[index] = value;
}

template <typename T>
const T&
Vector<T>::at(const size_t index) const
{
    assert(index < size());
    return begin_[index];
}

template <typename T>
T&
Vector<T>::at(const size_t index)
{
    assert(index < size());
    return begin_[index];
}

template <typename T>
const typename Vector<T>::value_type& 
Vector<T>::operator[](const typename Vector<T>::size_type index) const
{
    assert(index < size());
    return begin_[index];
}

template <typename T>
typename Vector<T>::reference 
Vector<T>::operator[](const typename Vector<T>::size_type index)
{
    assert(index < size());
    return begin_[index];
}

template <typename T>
typename Vector<T>::size_type
Vector<T>::max_size() const
{
    return std::numeric_limits<Vector<T>::size_type>::max() / 4;
}

template <typename T>
bool 
Vector<T>::operator==(const Vector<T>& rhv) const
{
    if (size() == rhv.size()) {
        for (size_t i = 0; i < size(); ++i) {
            if (this-> begin_[i] != rhv.begin_[i]) {
                return false;
            } 
        }
        return true;
    }
    return false;
}

template <typename T>
bool 
Vector<T>::operator!=(const Vector<T>& rhv) const
{
    return !(*this == rhv);
}

template <typename T>
bool 
Vector<T>::operator<(const Vector<T>& rhv) const
{ 
    if (this->size() == rhv.size()) {
        for (size_t i = 0; i < size(); ++i) {
            if (this-> begin_[i] > rhv.begin_[i]) {
                return false;
            }
        }
        return true;
    }
    return (this-> size() < rhv.size());
}

template <typename T>
bool
Vector<T>::operator>(const Vector<T>& rhv) const
{
    return !(*this < rhv);
}

template <typename T>
bool 
Vector<T>::operator<=(const Vector<T>& rhv) const
{
    return !(*this > rhv);
}

template <typename T>
bool 
Vector<T>::operator>=(const Vector<T>& rhv) const
{
    return !(*this < rhv);
}

template <typename T>
typename Vector<T>::size_type
Vector<T>::capacity() const
{
    return bufferEnd_ - begin_; 
}

template <typename T>
typename Vector<T>::size_type
Vector<T>::size() const
{
    return end_ - begin_;
}

template <typename T>
typename Vector<T>::reference
Vector<T>::front()
{
    assert(begin != NULL);
    return *begin_;
}

template <typename T>
typename Vector<T>::const_reference
Vector<T>::front() const
{
    assert(begin_ != NULL);
    return *begin_;
}

template <typename T>
typename Vector<T>::const_reference
Vector<T>::back() const
{
    return *(end_ - 1);
}

template <typename T>
typename Vector<T>::reference
Vector<T>::back()
{
    return *(end_ - 1);
}

template <typename T>
void
Vector<T>::resize(const size_type n)
{ 
    assert(n < max_size());
    if (n > capacity()) {
        reserve(n);
    }
    for (size_t i = size(); i < n; ++i) {
        new (begin_ + i) T();
    }
    for(size_t i = n; i < size(); ++i) {
        begin_[i].~T();
    } 
    end_ = begin_ + n;
}

template <typename T>
void
Vector<T>::resize(const size_type n, const T& init)
{
    assert(n < max_size());
    if (n > capacity()) {
        reserve(n);
    }
    for (size_t i = size(); i < n; ++i) {
        new (begin_ + i) T(init);
    }
    for(size_t i = n; i < size(); ++i) {
        begin_[i].~T();
    }  
    end_ = begin_ + n;
}

template <typename T>
void
Vector<T>::reserve(const size_type n)
{
    if (n > capacity()) {
        T* temp = reinterpret_cast<T*>(::operator new[](n * sizeof(T)));
        if (begin_ != NULL) {
            ::memcpy(reinterpret_cast<void*>(temp), reinterpret_cast<void*>(begin_), size() * sizeof(T));
            ::operator delete [](begin_);
        }
        end_ = temp + size();
        bufferEnd_ = temp + n;
        begin_ = temp;
    }
}

template <typename T>
bool
Vector<T>::empty() const
{
    return (begin_ == end_);
}

template <typename T>
void
Vector<T>::push_back(const T& element)
{
    const int currentCapacity = capacity();
    const int currentSize = size();
    if (currentSize == currentCapacity) {
        const int newCapacity = static_cast<int>(RESERVE_COEFF * currentCapacity);  
        reserve(currentCapacity >= newCapacity ? newCapacity + 1 : newCapacity);
    }
    new (begin_ + currentSize) T(element);
    ++end_;
}

template <typename T>
void
Vector<T>::pop_back()
{
    if (!empty()) {
        end_->~T();
        --end_;
    }
}

template <typename T> 
Vector<T>::const_iterator::const_iterator()
    : ptr_(NULL)
{
}

template <typename T> 
Vector<T>::const_iterator::const_iterator(const typename Vector<T>::const_iterator& rhv)
    :ptr_(rhv.ptr_)
{
}

template <typename T>
Vector<T>::const_iterator::const_iterator(const typename Vector<T>::pointer ptr)
    :ptr_(ptr)
{
}

template <typename T> 
Vector<T>::const_iterator::~const_iterator()
{
    ptr_ = NULL;
}

template<typename T>
const
typename Vector<T>::const_iterator& 
Vector<T>::const_iterator::operator=(const typename Vector<T>::const_iterator& rhv)
{
    this-> ptr_ = rhv.ptr_;
    return *this;
}

template <typename T>
const
typename Vector<T>::value_type& 
Vector<T>::const_iterator::operator*() const
{
    return *ptr_;
}

template <typename T>
const
typename Vector<T>::value_type* 
Vector<T>::const_iterator::operator->() const
{
    return ptr_;
}

template <typename T>
typename Vector<T>::pointer
Vector<T>::const_iterator::getPtr() const
{
    return ptr_;
}

template <typename T>
typename Vector<T>::const_iterator
Vector<T>::const_iterator::operator+(const int value) const
{
    return ptr_ + value;
}

template <typename T>
typename Vector<T>::const_iterator
Vector<T>::const_iterator::operator-(const int value) const
{
    return ptr_ - value;
}

template <typename T>
typename Vector<T>::difference_type
Vector<T>::const_iterator::operator+(const const_iterator& rhv) const
{
    return this->ptr_ + rhv.ptr_;
}

template <typename T>
typename Vector<T>::difference_type
Vector<T>::const_iterator::operator-(const const_iterator& rhv) const
{
    return this->ptr_ - rhv.ptr_;
}

template <typename T>
typename Vector<T>::const_iterator
Vector<T>::const_iterator::operator++()
{
    ++ptr_;
    return *this;
}

template <typename T>
typename Vector<T>::const_iterator
Vector<T>::const_iterator::operator--()
{
    --ptr_;
    return *this;
}

template <typename T>
typename Vector<T>::const_iterator
Vector<T>::const_iterator::operator++(int)
{
    const_iterator temp = *this;
    ++ptr_;
    return temp;
}

template <typename T>
typename Vector<T>::const_iterator
Vector<T>::const_iterator::operator--(int)
{
    const_iterator temp = *this;
    --ptr_;
    return temp;
}

template <typename T>
typename Vector<T>::const_iterator
Vector<T>::const_iterator::operator+=(const int value)
{
    ptr_ += value;
    return *this;
}

template <typename T>
typename Vector<T>::const_iterator
Vector<T>::const_iterator::operator-=(const int value)
{
    ptr_ -= value;
    return *this;
}

template <typename T>
bool
Vector<T>::const_iterator::operator==(const typename Vector<T>::const_iterator& rhv) const
{
    return ptr_ == rhv.ptr_;
}

template <typename T>
bool
Vector<T>::const_iterator::operator!=(const typename Vector<T>::const_iterator& rhv) const
{
    return !(ptr_ == rhv.ptr_);
}

template <typename T>
bool
Vector<T>::const_iterator::operator<(const typename Vector<T>::const_iterator& rhv) const
{
    return ptr_ < rhv.ptr_;
}

template <typename T>
bool
Vector<T>::const_iterator::operator>(const typename Vector<T>::const_iterator& rhv) const
{
    return !(ptr_ < rhv.ptr_);
}

template <typename T>
bool
Vector<T>::const_iterator::operator<=(const typename Vector<T>::const_iterator& rhv) const
{
    return !(ptr_ > rhv.ptr_);
}

template <typename T>
bool
Vector<T>::const_iterator::operator>=(const typename Vector<T>::const_iterator& rhv) const
{
    return !(ptr_ < rhv.ptr_);
}

template <typename T>
Vector<T>::iterator::iterator()
    : const_iterator(NULL)
{
}

template <typename T>
Vector<T>::iterator::iterator(const typename Vector<T>::pointer ptr) 
    : const_iterator(ptr)
{
}

template <typename T> 
typename Vector<T>::reference 
Vector<T>::iterator::operator*()
{
    return *(Vector<T>::const_iterator::getPtr());
}

template <typename T>
typename Vector<T>::value_type* 
Vector<T>::iterator::operator->()
{
    return this->ptr_;
}

template <typename T> 
typename Vector<T>::reference 
Vector<T>::iterator::operator[](const size_t s)
{
    assert(this-> ptr_ + s < size());
    return *(this-> ptr_ + s);
}

template <typename T>
typename Vector<T>::difference_type
Vector<T>::iterator::operator+(const iterator& rhv) const
{
    return this->getPtr() + rhv.ptr_;
}

template <typename T>
typename Vector<T>::difference_type
Vector<T>::iterator::operator-(const iterator& rhv) const
{
    return this->getPtr() - rhv.ptr_;
}

template <typename T>
typename Vector<T>::iterator
Vector<T>::iterator::operator+(const int value) const
{
    return this->getPtr() + value;
}

template <typename T>
typename Vector<T>::iterator
Vector<T>::iterator::operator-(const int value) const
{
    return this->getPtr() - value;
}

template <typename T>
typename Vector<T>::iterator
Vector<T>::iterator::operator++()
{
    this-> ptr_ += 1;
    return *this;
}

template <typename T>
typename Vector<T>::iterator
Vector<T>::iterator::operator--()
{
    this-> ptr_ -= 1;
    return *this;
}

template <typename T>
typename Vector<T>::iterator
Vector<T>::iterator::operator++(int)
{
    iterator temp = *this;
    this-> ptr_ += 1;
    return temp;
}

template <typename T>
typename Vector<T>::iterator
Vector<T>::iterator::operator--(int)
{
    iterator temp = *this;
    this-> ptr_ -= 1;
    return temp;
}

template <typename T>
typename Vector<T>::iterator
Vector<T>::iterator::operator+=(const int value)
{
    this-> ptr_ += value;
    return *this;
}

template <typename T>
typename Vector<T>::iterator
Vector<T>::iterator::operator-=(const int value)
{
    this-> ptr_ -= value;
    return *this;
}

template <typename T>
Vector<T>::const_reverse_iterator::const_reverse_iterator() 
    : ptr_(NULL)
{

}

template <typename T>
Vector<T>::const_reverse_iterator::const_reverse_iterator(const typename Vector<T>:: pointer value) 
    : ptr_(value)
{
}

template <typename T>
Vector<T>::const_reverse_iterator::const_reverse_iterator(const typename Vector<T>::const_reverse_iterator& rhv) 
    : ptr_(rhv.ptr_)
{
}

template <typename T>
Vector<T>::const_reverse_iterator::~const_reverse_iterator() 
{
    ptr_ = NULL;
}

template <typename T>
const
typename Vector<T>::const_reverse_iterator&
Vector<T>::const_reverse_iterator::operator=(const typename Vector<T>::const_reverse_iterator& rhv)
{
    ptr_ = rhv.ptr_;
    return *this;
}

template <typename T>
const typename Vector<T>::value_type&
Vector<T>::const_reverse_iterator::operator*() const
{
    return *(this-> ptr_);
}

template <typename T>
const typename Vector<T>::value_type*
Vector<T>::const_reverse_iterator::operator->() const
{
    return (this-> ptr_);
}

template <typename T>
typename Vector<T>::pointer
Vector<T>::const_reverse_iterator::getRptr() const
{
    return ptr_;
}

template <typename T>
typename Vector<T>::const_reverse_iterator
Vector<T>::const_reverse_iterator::operator+(const int value)
{
    const_reverse_iterator temp = ptr_ + value;
    return temp;
}

template <typename T>
typename Vector<T>::const_reverse_iterator
Vector<T>::const_reverse_iterator::operator-(const int value)
{
    const_reverse_iterator temp = ptr_ - value;
    return temp;
}

template <typename T>
typename Vector<T>::const_reverse_iterator
Vector<T>::const_reverse_iterator::operator++()
{
    ++ptr_;
    return *this;
}

template <typename T>
typename Vector<T>::const_reverse_iterator
Vector<T>::const_reverse_iterator::operator--()
{
    --ptr_;
    return *this;
}

template <typename T>
typename Vector<T>::const_reverse_iterator
Vector<T>::const_reverse_iterator::operator++(int)
{
    const_reverse_iterator temp = *this;
    ++ptr_;
    return temp;
}

template <typename T>
typename Vector<T>::const_reverse_iterator
Vector<T>::const_reverse_iterator::operator--(int)
{
    const_reverse_iterator temp = *this;
    --ptr_;
    return temp;
}

template <typename T>
typename Vector<T>::const_reverse_iterator
Vector<T>::const_reverse_iterator::operator+=(const int value)
{
    ptr_ += value;
    return *this;
}

template <typename T>
typename Vector<T>::const_reverse_iterator
Vector<T>::const_reverse_iterator::operator-=(const int value)
{
    ptr_ -= value;
    return *this;
}

template <typename T>
typename Vector<T>::difference_type
Vector<T>::const_reverse_iterator::operator+(const const_reverse_iterator& rhv) const
{
    return this->ptr_ - rhv.ptr_;
}

template <typename T>
typename Vector<T>::difference_type
Vector<T>::const_reverse_iterator::operator-(const const_reverse_iterator& rhv) const
{
    return this->ptr_ + rhv.ptr_;
}

template <typename T>
Vector<T>::reverse_iterator::reverse_iterator(typename Vector<T>::pointer value)
    : const_reverse_iterator(value)
{
}

template <typename T>
bool
Vector<T>::const_reverse_iterator::operator==(const typename Vector<T>::const_reverse_iterator& rhv) const
{
    return ptr_ == rhv.ptr_;
}

template <typename T>
bool
Vector<T>::const_reverse_iterator::operator!=(const typename Vector<T>::const_reverse_iterator& rhv) const
{
    return !(ptr_ == rhv.ptr_);
}

template <typename T>
bool
Vector<T>::const_reverse_iterator::operator<(const typename Vector<T>::const_reverse_iterator& rhv) const
{
    return ptr_ < rhv.ptr_;
}

template <typename T>
bool
Vector<T>::const_reverse_iterator::operator>(const typename Vector<T>::const_reverse_iterator& rhv) const
{
    return !(ptr_ < rhv.ptr_);
}

template <typename T>
bool
Vector<T>::const_reverse_iterator::operator<=(const typename Vector<T>::const_reverse_iterator& rhv) const
{
    return !(ptr_ > rhv.ptr_);
}

template <typename T>
bool
Vector<T>::const_reverse_iterator::operator>=(const typename Vector<T>::const_reverse_iterator& rhv) const
{
    return !(ptr_ < rhv.ptr_);
}
template <typename T>
typename Vector<T>::reference
Vector<T>::reverse_iterator::operator*()
{
    return *(this-> getRptr());
}

template <typename T>
typename Vector<T>::reference
Vector<T>::reverse_iterator::operator->()
{
    return *this;
}

template <typename T>
typename Vector<T>::reference
Vector<T>::reverse_iterator::operator[](const typename Vector<T>::size_type s)
{
    assert(0 > this-> ptr_ - s);
    return *(this-> ptr_ - s);
}

template <typename T>
typename Vector<T>::reverse_iterator
Vector<T>::reverse_iterator::operator+(const int value)
{
    reverse_iterator temp = this-> ptr_ - value;
    return temp;
}

template <typename T>
typename Vector<T>::reverse_iterator
Vector<T>::reverse_iterator::operator-(const int value)
{
    reverse_iterator temp = this-> ptr_ + value;
    return temp;
}

template <typename T>
typename Vector<T>::reverse_iterator
Vector<T>::reverse_iterator::operator++()
{
    --(this->ptr_);
    return *this;
}

template <typename T>
typename Vector<T>::reverse_iterator
Vector<T>::reverse_iterator::operator--()
{
    ++(this->ptr_);
    return *this;
}

template <typename T>
typename Vector<T>::reverse_iterator
Vector<T>::reverse_iterator::operator++(int)
{
    reverse_iterator temp = *this;
    (this-> ptr_)--;
    return temp;
}

template <typename T>
typename Vector<T>::reverse_iterator
Vector<T>::reverse_iterator::operator--(int)
{
    reverse_iterator temp = *this;
    ++(this-> ptr_);
    return temp;
}

template <typename T>
typename Vector<T>::reverse_iterator
Vector<T>::reverse_iterator::operator+=(const int value)
{
    (this-> ptr_) -= value;
    return *this;
}

template <typename T>
typename Vector<T>::reverse_iterator
Vector<T>::reverse_iterator::operator-=(const int value)
{
    this-> ptr_ += value;
    return *this;
}

template <typename T>
typename Vector<T>::difference_type
Vector<T>::reverse_iterator::operator+(const reverse_iterator& rhv) const
{
    return this->getPtr() - rhv.ptr_;
}

template <typename T>
typename Vector<T>::difference_type
Vector<T>::reverse_iterator::operator-(const reverse_iterator& rhv) const
{
    return this->getPtr() + rhv.ptr_;
}

template<typename T>
typename Vector<T>::const_iterator
Vector<T>::begin() const
{
    return const_iterator(begin_);
}

template<typename T>
typename Vector<T>::iterator 
Vector<T>::begin() 
{
   return iterator(begin_); 
}

template  <typename T>
typename Vector<T>::const_iterator
Vector<T>::end() const 
{
    return const_iterator(end_);
}

template <typename T>
typename Vector<T>::iterator
Vector<T>::end()
{
    return iterator(end_);
}

template <typename T>
typename Vector<T>::const_reverse_iterator
Vector<T>::rbegin() const
{
    return const_reverse_iterator(end_ - 1);   
}

template <typename T>
typename Vector<T>::reverse_iterator
Vector<T>::rbegin()
{
    return reverse_iterator(end_ - 1);
}

template <typename T>
typename Vector<T>::const_reverse_iterator
Vector<T>::rend() const
{
    return const_reverse_iterator(begin_ - 1);
}

template <typename T>
typename Vector<T>::reverse_iterator
Vector<T>::rend()
{
    return reverse_iterator(begin_ - 1);
}

template <typename T>
typename Vector<T>::iterator
Vector<T>::insert(iterator pos, const T& x)
{
    assert(pos >= begin_ && pos < end_);
    const size_t index = pos - begin();
    const size_t newSize = size();
    reserve(newSize);
    pos = begin_ + index;
    for (iterator i = end(); i >= pos; --i) {
        memcpy(reinterpret_cast<void*>((i).getPtr()), reinterpret_cast<void*>((i - 1).getPtr()), sizeof(T));
    }
    new (pos.getPtr()) T(x);
    ++end_;
    return pos;
}

template <typename T>
template <typename InputIterator>
void
Vector<T>::insert(iterator pos, InputIterator first, InputIterator last)
{
    assert(pos >= begin_ && pos < end_);
    const size_t n = last - first;
    const size_t newSize = size() + n;
    const size_t index = pos - begin();
    reserve(newSize);
    pos = begin_ + index;
    for (iterator move = end() - 1; move > pos; --move) { 
        memcpy(reinterpret_cast<void*>((move + n).getPtr()), reinterpret_cast<void*>((move).getPtr()), sizeof(T));
    }
    while (first != last) {
        new ((pos++).getPtr())  T(*first++);
    }
    end_ += n;
}

template <typename T>
void
Vector<T>::insert(iterator pos, const int n, const T& x)
{
    assert(pos >= begin_ && pos < end_);
    const size_t index = pos - begin_;
    const size_t newSize = n + size();
    reserve(newSize);
    pos = begin_ + index;
    for (iterator move = end() - 1; move > pos; --move) { 
        memcpy(reinterpret_cast<void*>((move + n).getPtr()), reinterpret_cast<void*>((move).getPtr()), sizeof(T));
    }
    for (int step = 0; step < n; ++step) {
        new ((pos++).getPtr()) T(x);
    }
    end_ += n;
}

template <typename T>
typename Vector<T>::iterator
Vector<T>::erase(iterator pos)
{ 
    if (!empty()) {
        iterator move = pos;
        move-> ~T();
        ++move;
        while (move != end_) { 
            new((pos++).getPtr())  T(*move++);
        }
        --end_;
        return pos;
    }
    return end_;
}

template <typename T>
typename Vector<T>::iterator
Vector<T>::erase(iterator first, iterator last)
{
    iterator distance = first;
    const size_t s = last - first;
    while (distance != last) {
        distance-> ~T();
        ++distance;
    }
    while (last != end_) {
        new ((first++).getPtr()) T (*last++);
    }
    end_ -= s;
    return last;
}

